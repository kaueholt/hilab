# API Seriços de Circuitos do projeto LINCE

Fornece os serviços necessários para a gestão dos circuitos elaborados pela empresa LINCE, bem como o controle dos resultados de exames feitos nesses circuitos. Essa API será utilizada pelos tablets que estarão em campo e pelos terceiros que enviarão os resultados dos exames.

## Utilização

Execute o seguinte comando na raiz do projeto para instalar as dependências:

```
npm install
```

Para executar a aplicação em ambiente de Produção utilize o seguinte comando:

```
npm run prod
```

Para executar a aplicação em ambiente de Desenvolvimento, utilize o seguinte comando:

```
npm run dev
```

## Endpoints

Utilizamos o Postman para documentar as rotas e o conjunto de collections está disponnibilizado neste [link](https://app.getpostman.com/join-team?invite_code=563b0d2092be7cec048370923555f501)

Abaixo são apresentadas as rotas existentes na versão atual da API:

```
├── http://{{url}}:{{port}}
│   ├── /authentication
│   │   ├── /sign-up
│   │   ├── /login
│   │   ├── /forgot
│   │   ├── /ask-recover-password
│   │   └── /confirm-password
│   ├── /users
│   │   ├── /index
│   │   ├── /create
│   │   └── /:id
```

## Nomenclatura de Arquivos

O padrão para os nomes de arquivos é o lower CamelCase, onde a primeira palavra é iniciada com a letra minúscula e as demais com letra maiúscula e, para as palavras subsequentes o mesmo se aplica, sem espaços ou caracteres especiais entre elas.

Veja os exemplos abaixo:

```
userRoutes.js
userController.js
server.js
```

## Arquitetura

O sistema da API é dividido em duas principais camadas, sendo a primeira `routes` representada pela arquitetura de `Routers` e `Controllers`, e a segunda a camada `application`, onde são executados os `Services` e `Repository` (operações com o banco). A comunicação dentre essas duas camada se dará através do padrão `facade`, onde são disparados comandos que podem consumir um ou mais `Services`.

Também vale ressaltar que o padrão `Dependency Injection` (Injeção de dependências) está sedo implementado através do script `diFactory.js`, localizado na pasta `src`.

## Estrutura de Pastas

A estrutura de diretórios deste projeto é organizada da seguinte forma:

```
├── cert
├── src
│   ├── application
│   │   ├── repository
│   │   ├── enums
│   │   ├── managers
│   │   ├── migrate
│   │   ├── models
│   │   ├── seeds
│   │   ├── services
│   │   └── util
│   ├── config
│   │   └── plugins
│   ├── routes
│   │   ├── controllers
```

## Cert

Nessa pasta são guardados os certificados que serão utilizados na conexão [SSL/TLS](https://pt.wikipedia.org/wiki/Transport_Layer_Security).

## Src

A pasta `Src` contém todos os recursos do sistema. Os arquivos fora dessa pasta são considerados como recursos externos ou configurações externas.

### Application

**Pasta Enums**

Contém os `Enumerators` utilizados pela camada `application`.

Os scripts devem respeitar o seguinte padrão de nomes: `prefixoEnum.js`, onde `prefixo` será substituído pelo nome que será atribuído ao script.

**Pasta Managers**

Contém os scripts encarregados de manipular uma arquitetura ou recurso geral em específico. Pode-se tomar como exemplo os scripts: `facadeManager.js` como o design pattern, `replyManager.js` encarregado de gerenciar o retorno de erros ao cliente e `securityManager.js`, o qual possui o objetivo de fornecer serviços para criptografar e decriptar mensagens.

Os scripts devem respeitar o seguinte padrão de nomes: `prefixoManager.js`, onde `prefixo` será substituído pelo nome que será atribuído ao script.

**Pasta Services**

Nessa pasta são guardados os scripts de serviços específicos, os quais são consumidos pela camada `Routes` através do design pattern `Facade`. Esses scripts utilizam uma camada intermediária, chamada `Repository`, para extrair dados do banco de dados e montar o payload que será retornado à camada `Route`.

Os scripts devem respeitar o seguinte padrão de nomes: `prefixoService.js`, onde `prefixo` será substituído pelo nome que será atribuído ao script.

**Pasta Repository**

Essa pasta contém os scripts que se comunicam e operam diretamente com o banco de dados `PostgreeSQL`. Seus métodos de classes receberão os parâmetros para as operações do banco e retornarão a collection da consulta para a camada `Service`.

Os scripts devem respeitar o seguinte padrão de nomes: `prefixoRepository.js`, onde `prefixo` será substituído pelo nome que será atribuído ao script.

**Pasta Models**

Contém os scripts com as classes Modelos para criar instâncias de entidades do banco de dados.

**Pasta Migrate**

Contém os arquivos de controle de versão de tabelas do banco de dados, gerados pelo migrate.

**Pasta Utils**

Essa pasta contém scripts diversos, com mecanismos simples que possam ser reaproveitados. Como por exemplo `parser.js`, script que converte arquivos `csv`, `xml` ou alguma outra extensão. 

### Config

Contém scripts com dados e configurações gerais de toda a API. Aqui tambéms são guardados os plugins e os dados carregados dos arquivos de `environments` como variáveis estáticas.

**Pasta Plugins**

Subpasta que separa os plugins dos arquivos de configurações.

### Routes

Camada `Routes` onde são guardados os scripts de configuração de rotas e controllers.

**Pasta Routes**

Contém os scripts das rotas, onde cada script poderá conter mais de uma rota configurada com seus respectivos `Controllers`.

**Pasta Controllers**

Contém os scripts `Controllers` que invocam métodos de autenticação e serviços através da camada `Facade`.

**Bugfix**
