'use strict';

const Joi = require('@hapi/joi')
var UserAgent = require('./application/utils/userAgent')

/**
 * Classe encarregada de coletar as rotas de outros scripts e configurar
 * rotas gerais
 */
module.exports = class Routes {
    constructor(authenticationRoutes, userRoutes, companyRoutes, workerRoutes, equipmentRoutes,
        circuitRoutes, examRoutes, examClassRoutes, formRoutes, linceRoutes) {
        this.authenticationRoutes = authenticationRoutes
        this.userRoutes = userRoutes
        this.companyRoutes = companyRoutes
        this.workerRoutes = workerRoutes
        this.equipmentRoutes = equipmentRoutes
        this.circuitRoutes = circuitRoutes
        this.examRoutes = examRoutes
        this.examClassRoutes = examClassRoutes
        this.formRoutes = formRoutes
        this.linceRoutes = linceRoutes
    }

    /**
     * Configura rotas gerais
     */
    getMainRoute() {
        this.Main = {
            path: '/',
            method: ['GET'],
            options: {
                auth: false,
                tags: ['api'],
                description: "Rota padrão para a integração contínua",
                notes: "rota para verificar se o servidor está operante",
                validate: {
                    headers: Joi.object({
                        agent: Joi.string().default('browser'),
                        'app-version': Joi.number().default(0)
                    }).unknown()
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string().required()
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return { message: "Server is running!" }
            }
        }

        return [this.Main]
    }

    /**
     * Coleta as rotas dos outros scripts e incluir na lista as 
     * que aqui foram configuradas
     */
    getRoutes() {
        return [
            this.authenticationRoutes.getRoutes(),
            this.userRoutes.getRoutes(),
            this.companyRoutes.getRoutes(),
            this.circuitRoutes.getRoutes(),
            this.workerRoutes.getRoutes(),
            this.equipmentRoutes.getRoutes(),
            this.examRoutes.getRoutes(),
            this.examClassRoutes.getRoutes(),
            this.formRoutes.getRoutes(),
            this.linceRoutes.getRoutes(),
            this.getMainRoute()
        ];
    }
};
