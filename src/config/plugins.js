'use strict';

const HapiAuthJwt = require('hapi-auth-jwt2').plugin;
const Swagger = require('./plugins/swagger');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');

/**
 * Script responsável por construir um array com plugins para
 * injetar no servidor HAPI
 */
const plugins = () => {
    const listaPlugins = [HapiAuthJwt]
    listaPlugins.push(Inert)
    listaPlugins.push(Vision)
    listaPlugins.push(Swagger)
    return listaPlugins
};

module.exports = plugins()
