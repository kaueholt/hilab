'use strict';

/**
 * Configuração do plugin hapi-swager para implementar
 * a documentação da API com Swagger.
 * Para acessar a documentação basta acessar <url>/documentation
 */
module.exports = {
    plugin: require('hapi-swagger'),
    options: {
        info: {
            title: 'LINCE - Documento de API do Sistema de Circuitos',
            version: '1.0.0'
        }
    }
};