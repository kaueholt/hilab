'use strict'

const dotenv = require('dotenv')
dotenv.config()

/**
 * Classe encarregada de comportar os dados globais, onde alguns podem ser coletados do arquivo de configuração de ambiente para 
 * disponibilizar para os scripts da API.
 */
module.exports = class Config {

    constructor() {
        this.Data = {
            environment: process.env.NODE_ENV,
            server: {
                host: process.env.HOST,
                port: process.env.PORT,
                key: process.env.API_KEY,
                requestTimeout: process.env.REQUEST_TIMEOUT,
                uploadPath: 'uploads',
                mail: {
                    user: 'circuitosaudelongevidade@gmail.com',
                    password: 'lincelince'
                },
                db: {
                    username: process.env.DB_USER,
                    password: process.env.DB_PASSWORD,
                    database: process.env.DB_NAME,
                    host: process.env.DB_HOST,
                    port: process.env.DB_PORT,
                    dialect: 'postgres',
                    operatorsAliases: 0,
                    define: {
                        timestamp: 1,
                        underscored: 1
                    }
                },
                ssl: {
                    lince: {
                        useSSL: (process.env.USESSL || 'no') === 'yes',
                        sslPort: process.env.SSLPORT || '',
                        key: process.env.CERTKEY || '',
                        cert: process.env.CERT || '',
                        ca: process.env.CERTCA || ''
                    }
                }
            },
            security: {
                tokenKey: 'd41d8cd98f00b204e9800998ecf8427e',
                tokenMaxAge: '10y',
                tokenAlgorithm: 'HS256', //'',
                cipherAlgorithm: 'aes-256-cbc',//'aes128',
                ecdhAlgorithm: 'secp384r1',
                hashAlgorithm: 'sha256',
                tokenEncryptionEntropy: 'd41d8cd98f00b204e9800998ecf8427e',
                tokenIv: 'j54xyunkzne59sxg',
            },
            crypto: {
                hash: 'sha1',
                salt: '7dkpprw6k2m',
                iv: 'j54xyunkzne59sxg',
                iterations: 13,
                keySize: 256
            }
        }
    }
}
