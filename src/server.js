'use strict'

const plugins = require('./config/plugins');
const _ = require('lodash');
const Log = require('./application/utils/log')

/**
 * Classe encarregada de configrar e executar o servidor @hapi
 */
module.exports = class Server {
    constructor(hapi, securityManager, config, routes) {
        this.config = config;
        this.securityManager = securityManager;
        this.server = new hapi.Server({
            host: this.config.Data.server.host,
            port: this.config.Data.server.port,
            routes: {
                cors: {
                    credentials: true
                },
                security: false
            }
        })

        this.routes = routes;
    }

    /**
     * Inicializa configura e inicializa o servidor
     */
    async init() {
        await this.startPlugins()
        await this.start()
    }

    /**
     * Registra as rotas e os plugins configurados em ./config/plugins no servidor que 
     * será iniciado.
     */
    async startPlugins() {

        await this.server.register(plugins);

        await this.server.auth.strategy('token', 'jwt', {
            key: this.config.Data.security.tokenKey,
            validate: (request, decodedData) => {
                try {
                    let token = decodedData.auth.token
                    const decryptedData = this.securityManager.decryptToken(token);
                    return { isValid: true }
                }
                catch (error) {
                    return { isValid: false }
                }
            },
            verifyOptions: {
                maxAge: this.config.Data.security.tokenMaxAge,
                algorithms: [
                    this.config.Data.security.tokenAlgorithm
                ]
            }
        });

        await _.each(this.routes.getRoutes(), (route) => {
            this.server.route(route);
        });
    }

    /**
     * Inicia o servidor
     */
    async start() {
        try {
            await this.server.start();
        }
        catch (error) {
            Log.writeLog('./log.txt', __filename.slice(__dirname.length + 1), error)
            process.exit(1);
        }
        
        if (this.config.Data.environment !== 'production') {
            console.log(`Running port: 
                ----PORT: ${this.config.Data.server.port} 
                ----HOST: ${this.config.Data.server.host}
                -----KEY: ${this.config.Data.server.key}`)
        }
    };
};