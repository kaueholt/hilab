'use strict'

/**
 * Classe utilizada para encriptar e decriptar dados como tokens e hash de senhas
 * @version 1.0.0
 */
module.exports = class SecurityManager {
    /**
     * Construtor da classe
     * @param {Config} config - instância de @see Config com as configurações gerais da API
     * @param {Crypto} crypto - biblioteca Cripto
     * @param {jwt} jwt - biblioteca JsonWebToken
     * @param {ReplyManager} replyManager - instância de @see ReplyManager  para tratar erros
     */
    constructor(config, crypto, jwt, replyManager) {
        this.config = config
        this.crypto = crypto
        this.jwt = jwt
        this.replyManager = replyManager
    }

    /**
     * Método responsável por gerar um token de acesso a API com a biblioteca JsonWebToken (JWT)
     * @param {String} encryptedTokenData - dados da sessão criptografados pelo método dataEncrypt
     */
    generateToken(encryptedTokenData) {
        const options = { algorithm: this.config.Data.security.tokenAlgorithm, expiresIn: '1h' }
        const token = this.jwt.sign(
            { encryptedData: encryptedTokenData },
            this.config.Data.security.tokenKey, options
        )
        return token
    }

    /**
     * Método responsável por criptografar objetos de sessão utilizando a biblioteca crypto
     * @param {String} data - dados da sessão
     * @returns {String} dados criptografados
     */
    sessionDataEncrypt(data) {
        try{
            const jsonData = JSON.stringify(data)

            const cipher = this.crypto.createCipheriv(this.config.Data.security.cipherAlgorithm,
                this.config.Data.security.tokenKey,
                this.config.Data.security.tokenIv)

            let encrypted = cipher.update(jsonData, 'utf8', 'hex')
            encrypted += cipher.final('hex')
            return encrypted
        }catch(e){
            throw e
        }
    }

    /**
     * Decodifica objetos de sessão utilizando a LIB crypto
     * @param {String} decryptedTokenData - dados criptografados
     * @returns {Object} - dados da sessão
     */
    sessionDataDecrypt(decryptedTokenData) {
        try {
            let decipher = this.crypto.createDecipheriv(
                this.config.Data.security.cipherAlgorithm,
                this.config.Data.security.tokenKey,
                this.config.Data.security.tokenIv
            )
    
            let decrypted = decipher.update(decryptedTokenData, 'hex', 'utf8')
            decrypted += decipher.final('utf8')
            
            let decryptedSessionData = JSON.parse(decrypted)

            return decrypted
        }
        catch (e) {
            throw e
        }
    }

    /**
     * Responsável por criptografar senhas
     * @param {String} text - senha 
     * @param {String} key - chave de segurança para a criptografia
     */
    encryptPassword(text, key) {
        if (!text || !key) {
            throw new Error(`A função encrypt precisa de dois argumentos, ${text} e ${key} precisam ser fornecidas`)
        }

        const derivedKey = this.crypto.pbkdf2Sync(key,
            this.config.Data.crypto.salt,
            this.config.Data.crypto.iterations,
            this.config.Data.crypto.keySize / 8,
            this.config.Data.crypto.hash)

        const cipher = this.crypto.createCipheriv(
            this.config.Data.security.cipherAlgorithm, 
            derivedKey, 
            this.config.Data.crypto.iv)

        let result = cipher.update(text, 'utf8', 'hex')
        result += cipher.final('hex')
        return result
    }

    /**
     * Responsável por descriptografar senhas
     * @param {String} text - senha criptografada
     * @param {String} key - chave de segurança para descriptografar
     */
    decryptPassword(text, key) {
        if (!text || !key) {
            throw new Error(`A função encrypt precisa de dois argumentos, ${text} e ${key} precisam ser fornecidas`)
        }

        const derivedKey = this.crypto.pbkdf2Sync(key,
            this.config.Data.crypto.salt,
            this.config.Data.crypto.iterations,
            this.config.Data.crypto.keySize / 8,
            this.config.Data.crypto.hash)

        const decipher = this.crypto.createDecipheriv(
            this.config.Data.security.cipherAlgorithm, 
            derivedKey, 
            this.config.Data.crypto.iv)

        let result = decipher.update(text, 'hex', 'utf8')
        result += decipher.final('utf8')
        return result
    }

    /**
    * Método responsável por descriptografar o token de acesso a API com a biblioteca JsonWebToken (JWT) 
    * @param {string} token - token gerado pelo JWT
    * @returns {string} dados descriptografados
    */
    decryptToken(token) {
        let decoded = this.jwt.verify(token, this.config.Data.security.tokenKey);
        return decoded.encryptedData;
    }
}
