'use strict'

const ActionEnums = require('../enums/actionEnum')

/**
 * Classe responsável por intermediar e possibilitar o consumo dos serviços pela camada de 
 * Controllers
 */
module.exports = class Facade {

    /**
    * Construtor da classe, onde as instâncias de serviços são associadas ao facade
    * @param {ReplyManager} replyManager - encarregado de gerenciar respostas com exceptions
    * @param {AuthenticationService} authenticationService - encarregado de operar os serviços de autenticação
    * @param {@class} - demais parâmetros são serviços utilizados pela camada controller
    */
    constructor(replyManager, authenticationService, userService, workerService,
        companyService, addressService, equipmentService, circuitsService, examService, formService, linceProfileService) {
        this.authenticationService = authenticationService
        this.userService = userService
        this.workerService = workerService
        this.companyService = companyService
        this.replyManager = replyManager
        this.addressService = addressService
        this.equipmentService = equipmentService
        this.circuitsService = circuitsService
        this.examService = examService
        this.linceProfileService = linceProfileService
        this.formService = formService
        this.ActionEnums = ActionEnums
    }

    /**
     * Autentica um usuário com o token recebido
     * @param {Object} request - objeto da requisição
     * @returns {Object} - usuário logado
     */
    async _auth(request) {
        return await this.authenticationService.authenticate(request)
            .then(result => result)
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.unauthorized, error))
    }

    /**
     * Método assíncrono que executa os serviços aqui associados
     * @param {Function} action - ação que será executada pelos serviços
     * @param {Object} data - dados da requisição
     * @param {Object} data.headers[] - headers recebidos pela requisição
     * @param {Object} data.payload - objeto com os valores enviados no payload da requisição
     * @param {Object} data.params - objeto com os valores enviados nos parâmetros da requisição
     * @param {Object} data.search - objeto com os valores enviados nos parâmetros da requisição
     * @param {Object} data.query - objeto com os valores enviados nos parâmetros da requisição
     * @param {Object} data.auth - (opcional) credenciais de autenticação
     * @param {Boolean} auth - autenticação necessária
     * @returns {Object} - retorno construído pelo serviço que foi executado
     */
    async action(action, data, auth = false) {

        if (auth) {
            data.loggedUser = await this._auth(data)
                .then(result => result)
        }

        switch (action) {
            case ActionEnums.login:
                return this.authenticationService.login(data)
            case ActionEnums.logout:
                return this.authenticationService.logout(data)
            case ActionEnums.signUp:
                return this.authenticationService.signUp(data)
            case ActionEnums.forgotPassword:
                return this.authenticationService.forgotPassword(data)
            case ActionEnums.resetPassword:
                return this.authenticationService.resetPassword(data)
            case ActionEnums.insertUser:
                return this.userService.insert(data)
            case ActionEnums.getUser:
                return this.userService.info(data)
            case ActionEnums.deleteUser:
                return this.userService.delete(data)
            case ActionEnums.getUsers:
                return this.userService.index(data)
            case ActionEnums.updateUser:
                return this.userService.update(data)
            case ActionEnums.insertWorker:
                return this.workerService.insert(data)
            case ActionEnums.getWorker:
                return this.workerService.info(data)
            case ActionEnums.deleteWorker:
                return this.workerService.delete(data)
            case ActionEnums.getWorkers:
                return this.workerService.index(data)
            case ActionEnums.updateWorker:
                return this.workerService.update(data)
            case ActionEnums.insertCompany:
                return this.companyService.insert(data)
            case ActionEnums.getCompany:
                return this.companyService.info(data)
            case ActionEnums.deleteCompany:
                return this.companyService.delete(data)
            case ActionEnums.getCompanies:
                return this.companyService.index(data)
            case ActionEnums.updateCompany:
                return this.companyService.update(data)
            case ActionEnums.insertModel:
                return this.equipmentService.insertModel(data)
            case ActionEnums.getModel:
                return this.equipmentService.infoModel(data)
            case ActionEnums.deleteModel:
                return this.equipmentService.deleteModel(data)
            case ActionEnums.getModels:
                return this.equipmentService.indexModels(data)
            case ActionEnums.updateModel:
                return this.equipmentService.updateModel(data)
            case ActionEnums.insertPatrimony:
                return this.equipmentService.insertPatrimony(data)
            case ActionEnums.getPatrimony:
                return this.equipmentService.infoPatrimony(data)
            case ActionEnums.deletePatrimony:
                return this.equipmentService.deletePatrimony(data)
            case ActionEnums.getPatrimonies:
                return this.equipmentService.indexPatrimonies(data)
            case ActionEnums.updatePatrimony:
                return this.equipmentService.updatePatrimony(data)
            case ActionEnums.insertAddress:
                return this.addressService.insert(data)
            case ActionEnums.getAddress:
                return this.addressService.info(data)
            case ActionEnums.deleteAddress:
                return this.addressService.delete(data)
            case ActionEnums.getAddresses:
                return this.addressService.index(data)
            case ActionEnums.updateAddress:
                return this.addressService.update(data)
            case ActionEnums.getCircuits:
                return this.circuitsService.index(data)
            case ActionEnums.getCircuit:
                return this.circuitsService.info(data)
            case ActionEnums.insertCircuit:
                return this.circuitsService.insert(data)
            case ActionEnums.updateCircuit:
                return this.circuitsService.update(data)
            case ActionEnums.deleteCircuit:
                return this.circuitsService.delete(data)
            case ActionEnums.deleteScheduling:
                return this.circuitsService.deleteScheduling(data)
            case ActionEnums.insertExam:
                return this.examService.insert(data)
            case ActionEnums.getExam:
                return this.examService.info(data)
            case ActionEnums.getExams:
                return this.examService.index(data)
            case ActionEnums.deleteExam:
                return this.examService.delete(data)
            case ActionEnums.insertExamClass:
                return this.examService.insertExamClass(data)
            case ActionEnums.infoExamClass:
                return this.examService.infoExamClass(data)
            case ActionEnums.getExamClass:
                return this.examService.indexExamClass(data)
            case ActionEnums.updateExamClass:
                return this.examService.updateExamClass(data)
            case ActionEnums.deleteExamClass:
                return this.examService.deleteExamClass(data)
            case ActionEnums.insertForm:
                return this.formService.insert(data)
            case ActionEnums.getForms:
                return this.formService.index(data)
            case ActionEnums.getForm:
                return this.formService.info(data)
            case ActionEnums.updateForm:
                return this.formService.update(data)
            case ActionEnums.deleteForm:
                return this.formService.delete(data)
            case ActionEnums.insertTopic:
                return this.formService.insertTopic(data)
            case ActionEnums.updateTopic:
                return this.formService.updateTopic(data)
            case ActionEnums.getTopics:
                return this.formService.indexTopics(data)
            case ActionEnums.getTopic:
                return this.formService.infoTopic(data)
            case ActionEnums.deleteTopic:
                return this.formService.deleteTopic(data)
            case ActionEnums.insertQuestion:
                return this.formService.insertQuestion(data)
            case ActionEnums.updateQuestion:
                return this.formService.updateQuestion(data)
            case ActionEnums.deleteQuestion:
                return this.formService.deleteQuestion(data)
            case ActionEnums.getQuestions:
                return this.formService.indexQuestions(data)
            case ActionEnums.getQuestion:
                return this.formService.infoQuestions(data)
            case ActionEnums.insertQuestionField:
                return this.formService.insertQuestionField(data)
            case ActionEnums.updateQuestionField:
                return this.formService.updateQuestionField(data)
            case ActionEnums.deleteQuestionField:
                return this.formService.deleteQuestionField(data)
            case ActionEnums.getQuestionFields:
                return this.formService.indexQuestionFields(data)
            case ActionEnums.getQuestionField:
                return this.formService.infoQuestionFields(data)
            case ActionEnums.getLinceProfiles:
                return this.formService.getLinceProfiles(data)
            case ActionEnums.getLinceProfile:
                return this.formService.getLinceProfile(data)
            case ActionEnums.insertLinceProfile:
                return this.formService.insertLinceProfile(data)
            case ActionEnums.deleteLinceProfile:
                return this.formService.deleteLinceProfile(data)
            case ActionEnums.updateLinceProfile:
                return this.formService.updateLinceProfile(data)
            default:
                this.replyManager.handleError(this.replyManager.statusEnum.notFound)
        }
    }
}
