'use strict'

const boom = require('boom')
const Log = require('../utils/log')

/**
 * Classe utilizada para padronizar retornos de erros ocorridos na API.
 */
module.exports = class ReplyManager {

    /**
     * Construtor da classe
     * @param {Config} config - classe config com as configurações gerais da API.
     * @param {StatusEnum} statusEnum - enumerador com os tipos de erros
     */
    constructor(config, statusEnum) {
        this.config = config
        this.statusEnum = statusEnum
    }

    /**
     * Constrói um response com a mensagem e os dados de payload.
     * @param {String} message - mensagem que retornará ao cliente com o status 200
     * @returns {Object} - objeto com os dados para o payload do response
     */
    handleSuccess(message, data) {
        return { message, data }
    }

    /**
     * Constrói o retorno de um erro com a biblioteca Boom
     * @param {int} errorStatus - tipo de erro contido no enumerador @see StatusEnum
     * @param {Error} error - erro com stack e mensagem
     */
    handleError(errorStatus, error = null) {

        let message = 'Estamos passando por um momento de instabilidade, por favor, tente novamente mais tarde.'

        if (error != null)
            message = error.message || error.name

        switch (errorStatus) {
            default:
            case this.statusEnum.badGateway:
            case 417:
                Log.writeLog('./log.txt', __filename.slice(__dirname.length + 1), error)
                throw boom.badGateway(message)
            case this.statusEnum.badRequest:
                Log.writeLog('./log.txt', __filename.slice(__dirname.length + 1), error)
                throw boom.badRequest(message)
            case this.statusEnum.unprocessableEntity:
            case this.statusEnum.conflict:
                throw boom.badRequest('Entrada de dados inválida')
            case this.statusEnum.unauthorized:
            case this.statusEnum.forbidden:
            case 412:
                throw boom.unauthorized('Acesso não autorizado')
            case this.statusEnum.paymentNecessary:
                throw boom.paymentRequired('Pagamento necessário')
            case this.statusEnum.notFound:
                throw boom.notFound('Recurso não encontrado')
            case this.statusEnum.resourceGone:
                throw boom.resourceGone('Versão desatualizada')
            case this.statusEnum.tooManyRequests:
                throw boom.tooManyRequests('Quantidade de requisições excedida')
        }
    }
}