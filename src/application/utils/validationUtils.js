/**
 * Função utilizada para fazer operações utilitárias de validação de Strings, que podem ser
 * utilziadas em várias ocasiões.
 */
const validationUtils = () => {

    /**
     * Verifica se o CPF informado é válido
     * @param {String} CPF - cpf sem pontuações
     */
    const isCPF = CPF => {
        if (CPF === null) {
            return false
        }
        CPF = CPF.replace(/\D/g, '')
        if (CPF.length !== 11) {
            return false
        }
        let Soma
        let Resto
        Soma = 0
        if (CPF === '00000000000' ||
            CPF === '11111111111' ||
            CPF === '22222222222' ||
            CPF === '33333333333' ||
            CPF === '44444444444' ||
            CPF === '55555555555' ||
            CPF === '66666666666' ||
            CPF === '77777777777' ||
            CPF === '88888888888' ||
            CPF === '99999999999') {
            return false
        }
        for (let i = 1; i <= 9; ++i) {
            Soma = Soma + parseInt(CPF.substring(i - 1, i)) * (11 - i)
        }
        Resto = (Soma * 10) % 11
        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0
        }
        if (Resto !== parseInt(CPF.substring(9, 10))) {
            return false
        }
        Soma = 0
        for (let i = 1; i <= 10; ++i) {
            Soma = Soma + parseInt(CPF.substring(i - 1, i)) * (12 - i)
        }
        Resto = (Soma * 10) % 11
        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0
        }
        if (Resto !== parseInt(CPF.substring(10, 11))) {
            return false
        }
        return true
    }

    /**
     * Verifica se o PIS/NIT informado é válido
     * @param {String} PIS - PIS/NIT sem pontuações
     */
    const isPIS = PIS => {
        PIS = PIS.toString()
        let weight = [3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
        var sum = 0, digito = 0

        if (PIS.length != 11 || PIS.match(/\D/))
            return false

        PIS = PIS.split("")

        for (var i = 0; i < 10; i++)
            sum += weight[i] * PIS[i]

        digito = 11 - (sum % 11)

        digito = digito < 10 ? digito : 0

        return PIS[10] == digito
    }

    /**
     * Verifica se o CNPJ informado é válido
     * @param {String} CNPJ - CNPJ sem pontuações
     */
    const isCNPJ = CNPJ => {

        CNPJ = CNPJ.replace(/[^\d]+/g, '');

        if (CNPJ == '') return false;

        if (CNPJ.length !== 14)
            return false;


        if (CNPJ == "00000000000000" ||
            CNPJ == "11111111111111" ||
            CNPJ == "22222222222222" ||
            CNPJ == "33333333333333" ||
            CNPJ == "44444444444444" ||
            CNPJ == "55555555555555" ||
            CNPJ == "66666666666666" ||
            CNPJ == "77777777777777" ||
            CNPJ == "88888888888888" ||
            CNPJ == "99999999999999")
            return false;


        let tamanho = CNPJ.length - 2
        let numeros = CNPJ.substring(0, tamanho);
        let digitos = CNPJ.substring(tamanho);
        let soma = 0;
        let pos = tamanho - 7;
        for (let i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0)) return false;
        tamanho = tamanho + 1;
        numeros = CNPJ.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (let i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;

        return true;

    }

    return {
        isCPF,
        isPIS,
        isCNPJ
    }

}

module.exports = validationUtils()