const fs = require('fs')
const Log = require('./log')

/**
 * Formata os dados adquiridos no header 'user-agent' para objetos
 */
const UserAgent = () => {

    /**
     * Constrói um objeto com as informações do sistema operacional do client-side
     * @param {String} userAgentHeader - informações contidas no header 'user-agent'
     * @returns {Object} os - objeto contendo os dados do sistema operacional
     * @returns {Object} os.name - Nome do sistema operacional
     * @returns {Object} os.version - Versão do sistema operacional
     * @returns {Object} os.device - Dispositivo do cliente (x64, motog8,..)
     */
    const getOperationSystem = (userAgentHeader) => {
        try {
            let dataWithParenthesis = userAgentHeader.match(/\([^)]*?\)/g)
            let osData = dataWithParenthesis[0].replace(/[()]/g, '').split(/;/)

            let os = {
                name: osData[0].trim() || '',
                version: osData[1].trim() || '',
                device: osData[2] || ''
            }

            return os
        } catch (error) {
            Log.writeLog('./log.txt', __filename.slice(__dirname.length + 1), error)
            throw error
        }
    }

    /**
     * Constrói um objeto com as informações do navegador do client-side
     * @param {String} userAgentHeader - informações contidas no header 'user-agent'
     * @returns {Object} browser - objeto contendo os dados do navegador
     * @returns {Object} browser.name - Nome do navegador
     * @returns {Object} browser.version - Versão do navegador
     * @returns {Object} browser.engine - Motor/Engine do navegador
     * @returns {Object} browser.engine_version - Versão do Motor/Engine do navegador
     */
    const getBrowser = (userAgentHeader) => {
        try {
            let engineRegex = /(AppleWebKit\/(.*)|Gecko\/(.*))+/g
            let engineMatch = engineRegex.exec(userAgentHeader)
            let engineNameAndVersionOnly = engineMatch[1].substr(0, engineMatch[1].indexOf(" ")).split('/')

            let nameVersionRegex = /(MSIE|Trident|(?!Gecko.+)Firefox|(?!AppleWebKit.+Chrome.+)Safari(?!.+Edge)|(?!AppleWebKit.+)Chrome(?!.+Edge)|(?!AppleWebKit.+Chrome.+Safari.+)Edge|AppleWebKit(?!.+Chrome|.+Safari)|Gecko(?!.+Firefox))(?: |\/)([\d\.apre]+)/g
            let nameVersion = userAgentHeader.match(nameVersionRegex)
            let nameVersionSplited = nameVersion[0].split('/')

            let browser = {
                name: nameVersionSplited[0] || '',
                version: nameVersionSplited[1] || '',
                engine: engineNameAndVersionOnly[0] || '',
                engine_version: engineNameAndVersionOnly[1] || ''
            }

            return browser
        } catch (error) {
            Log.writeLog('./log.txt', __filename.slice(__dirname.length + 1), error)
            throw error
        }
    }

    return { getOperationSystem, getBrowser }
}

module.exports = UserAgent()