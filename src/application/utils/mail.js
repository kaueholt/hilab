const nodemailer = require('nodemailer')

/**
 * Classe criada para configurar e disparar e-mails
 */
module.exports = class Mail {

    /**
     * Construtor da classe
     * @param {Config} config - instância de @see Confg
     */
    constructor(config) {
        this.config = config
    }

    /**
     * Configura o cabeçalho, rodapé, destinatários e outas configurações para o envio
     * @param {Array} receivers - destinatários
     * @param {string} subject - dados da requisição
     * @param {string} body - corpo do e-mail. Obs: o seu valor é inserido dentro de <body></body>
     */
    setMailConfig(receivers, subject, body) {

        this.transporter = nodemailer.createTransport({
            service: 'Gmail',
            host: 'smtp.gmail.com',
            port: 465,
            secure: false,
            auth: {
                user: this.config.Data.server.mail.user,
                pass: this.config.Data.server.mail.password
            }
        })

        this.mailOptions = {
            from: 'Circuito Saúde Longevidade <' + this.config.Data.server.mail.user + '>',
            to: receivers,
            subject,
            html: '<html><body>' + body + '</body></html>'
        }
    }

    /**
     * Envia aos destinatários o e-mail já configurado nesta classe
     */
    async sendMail() {
        return await this.transporter.sendMail(
            this.mailOptions, (error, result) => {
                if (error)
                    return error
                return result
            }
        )
    }
}