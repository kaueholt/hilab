const fs = require('fs')

/**
 * Classe criada para fazer upload de arquivos
 */
module.exports = class Uploader {

    /**
     * Construtor da classe
     * @param {Config} config - instância de @see Confg
     */
    constructor(config) {
        this.config = config
    }
    
    /**
     * Configura o arquivo recebido para gravar no diretório de uploads
     * @param {Object} file - dados do arquivo enviado via form-data
     * @param {String} name - nome que será utilizado para gravar o arquivo
     * @param {Array} expectedExtensions - extensões válidas
     */
    setFileToUpload(file, name, expectedExtensions = []){
        this.file = file
        this.name = name
        this.data = file._data
        this.ext = this.file.hapi.filename.split(".")
        this.ext = this.ext[this.ext.length - 1]
        return expectedExtensions.includes(this.ext.toLowerCase())
    }

    /**
     * Adquire o nome e extensão do arquivo concatenados, de acordo 
     * com o que já foi condigurado nesta classe
     */
    getFileFormated(){
        return this.name + "." + this.ext
    }

    /**
     * Exclui um arquivo do diretório de uploads
     * @param {String} filename - nome do arquivo
     */
    async deleteFile(filename){
        return await fs.unlinkSync(this.config.Data.server.uploadPath + "/" + filename); 
    }

    /**
     * Escreve o arquivo configurado nesta classe no diretório de uploads
     */
    async uploadFile() {
        if (!fs.existsSync(this.config.Data.server.uploadPath)) fs.mkdirSync(this.config.Data.server.uploadPath)
        let fileNameWithExt = this.name + "." + this.ext;
        await fs.writeFileSync(this.config.Data.server.uploadPath + "/" + fileNameWithExt, this.data)
    }
}