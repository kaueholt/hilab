'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

/**
 * Classe responsável por operar as interações com a tabela de @see LinceExamProfiles na base de dados
 */
module.exports = class LinceExamProfileRepository {

    /**
     * Construtor da classe
     * @param 
     */
    constructor() { }

    /**
     * Adquire uma lista de @see LinceExamProfiles no banco de dados
     * @param {Array} fields - array com os campos desejados para cada registro
     * @param {Object} options - objeto com parametros de busca
     * @returns {Array} - array de registros do tipo @see LinceExamProfiles
     */
    async index(fields = ['id', 'questao', 'perfil_id', 'sesi_tipoexame_id'],
        where={}, limit=1000, offset=0) {

        let data = {
            raw: true,
            attributes: fields,
            limit: limit,
            where,
            offset: offset,
        }

        return await models.LinceExamProfiles.findAll(data)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire os dados de um registro de @see LinceExamProfiles no banco de dados
     * @param {Object} options - objeto com os dados para efetuar a busca @see LinceExamProfiles
     * @returns {Object} - um registro de @see LinceExamProfiles
     */
    async info(fields, options) {
        let data = {
            id: null,
            search: options.search,
            attributes: fields
        }

        let reg = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
        let isUuid = reg.exec(options.search)

        isUuid ? data.id = data.search : null

        return await models.LinceExamProfiles.findOne({
            raw: true,
            plain: true,
            where: {
                [Op.or]: [
                    { id: { [Op.eq]: data.id } }
                ]
            },
        })
            .then(result => {

                if (!result)
                    throw new Error('Perfil-Exame não encontrado')
                else
                    return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere os dados fornecidos para um novo registro de @see LinceExamProfiles no banco de dados
     * @param {Object} options - objeto com os dados de um registro @see LinceExamProfiles
     * @returns {Object} - registro @see LinceExamProfiles que foi inserido
     */
    async insert(options) {
        let { payload } = options

        return await models.LinceExamProfiles.create({
            id: uuid(),
            questao: payload.questao,
            perfil_id: payload.perfil_id,
            sesi_tipoexame_id: payload.sesi_tipoexame_id
        })
            .then(result => {
                result = result.get({ plain: true })
                return result
            })
            .catch(error => {
                if (error.fields.sesi_tipoexame_id !== '')
                    throw new Error(`Perfil-Exame já cadastrado!`)
                throw error
            })
    }

    /**
     * Atualiza os dados de um registro @see LinceExamProfiles no banco de dados
     * @param {Object} options - objeto com os dados a atualizar do registro de @see LinceExamProfiles
     * @returns {Object} - objeto com os dados do registro de @see LinceExamProfiles atualizados
     */
    async update(options) {
        let { payload } = options

        return await models.LinceExamProfiles.update(
            payload,
            {
                where: { id: payload.id },
                returning: true,
                raw: true,
            })
            .then(result => {

                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar perfil-exame')
                return result[1][0]
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Exclui um registro de @see LinceExamProfiles no banco de dados
     * @param {Object} options - objeto com os dados para remoção @see LinceExamProfiles
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {

        return await models.LinceExamProfiles.destroy({
            plain: true, returning: true,
            where: { id: options.id }
        })
            .then(result => {

                if (result !== 1)
                    throw new Error('Erro ao deletar perfil-exame')

                return result
            })
            .catch(error => {
                throw error
            })
    }
}