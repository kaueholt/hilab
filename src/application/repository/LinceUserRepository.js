'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

/**
 * Classe responsável por operar as interações com a tabela de @see LinceUsers na base de dados
 */
module.exports = class LinceUserRepository {

    /**
     * Construtor da classe
     * @param 
     */
    constructor() { }

    /**
     * Adquire uma lista de @see LinceUsers no banco de dados
     * @param {Array} fields - array com os campos desejados para cada registro
     * @param {Object} options - objeto com parametros de busca
     * @returns {Array} - array de registros do tipo @see LinceUsers
     */
    async index(fields, options) {
        let data = {
            raw: true,
            attributes: fields,
            limit: 10,
            offset: 0,
        }

        return await models.LinceUsers.findAll(data)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire os dados de um registro de @see LinceUsers no banco de dados
     * @param {Object} options - objeto com os dados para efetuar a busca @see LinceUsers
     * @returns {Object} - um registro de @see LinceUsers
     */
    async info(fields, options) {
        let data = {
            id: null,
            cpf: null,
            search: options.search,
            attributes: fields
        }

        let reg = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
        let isUuid = reg.exec(options.search)

        isUuid ? data.id = data.search : data.cpf = data.search

        return await models.LinceUsers.findOne({
            raw: true,
            plain: true,
            where: {
                [Op.or]: [
                    { cpf: { [Op.eq]: data.cpf } },
                    { id: { [Op.eq]: data.id } }
                ]
            },
        })
            .then(result => {

                if (!result)
                    throw new Error('Usuário não encontrado')
                else
                    return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere os dados fornecidos para um novo registro de @see LinceUsers no banco de dados
     * @param {Object} options - objeto com os dados de um registro @see LinceUsers
     * @returns {Object} - registro @see LinceUsers que foi inserido
     */
    async insert(options) {
        let { payload } = options

        return await models.LinceUsers.create({
            id: uuid(),
            cpf: payload.cpf,
            status: payload.status,
            sesi_usuario_id: payload.sesi_usuario_id
        })
            .then(result => {
                result = result.get({ plain: true })
                return result
            })
            .catch(error => {
                if (error.fields.cpf !== '')
                    throw new Error(`Usuário já cadastrado!`)

                throw error
            })
    }

    /**
     * Atualiza os dados de um registro @see LinceUsers no banco de dados
     * @param {Object} options - objeto com os dados a atualizar do registro de @see LinceUsers
     * @returns {Object} - objeto com os dados do registro de @see LinceUsers atualizados
     */
    async update(options) {
        let { payload } = options

        return await models.LinceUsers.update(
            payload,
            {
                where: { cpf: options.search },
                returning: true,
                raw: true,
            })
            .then(result => {

                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar usuário')
                return result[1][0]
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Exclui um registro de @see LinceUsers no banco de dados
     * @param {Object} options - objeto com os dados para remoção @see LinceUsers
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {

        return await models.LinceUsers.destroy({
            plain: true, returning: true,
            where: { cpf: options.search }
        })
            .then(result => {

                if (result !== 1)
                    throw new Error('Erro ao deletar modelo')

                return result
            })
            .catch(error => {
                throw error
            })
    }
}