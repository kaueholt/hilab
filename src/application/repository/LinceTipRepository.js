'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

/**
 * Classe responsável por operar as interações com a tabela de @see LinceTips na base de dados
 */
module.exports = class LinceTipRepository {

    /**
     * Construtor da classe
     * @param 
     */
    constructor() { }

    /**
     * Adquire uma lista de @see LinceTips no banco de dados
     * @param {Array} fields - array com os campos desejados para cada registro
     * @param {Object} where - objeto com parametros de busca
     * @param {Object} limit - inteiro - número máximo de registros no retorno
     * @param {Object} offset - inteiro - offset - paginação
     * @returns {Array} - array de registros do tipo @see LinceTips
     */
    async index(fields = ['id', 'titulo', 'descricao', 'link','status', 'perfil_id'],
            where={}, limit=1000, offset=0) {
        let data = {
            raw: true,
            attributes: fields,
            limit: limit,
            where,
            offset: offset,
        }

        return await models.LinceTips.findAll(data)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire os dados de um registro de @see LinceTips no banco de dados
     * @param {Object} options - objeto com os dados para efetuar a busca @see LinceTips
     * @returns {Object} - um registro de @see LinceTips
     */
    async info(fields, options) {
        let data = {
            id: null,
            search: options.search,
            attributes: fields
        }

        let reg = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
        let isUuid = reg.exec(options.search)

        isUuid ? data.id = data.search : null

        return await models.LinceTips.findOne({
            raw: true,
            plain: true,
            where: {
                [Op.or]: [
                    { id: { [Op.eq]: data.id } }
                ]
            },
        })
            .then(result => {

                if (!result)
                    throw new Error('Dica não encontrada')
                else
                    return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere os dados fornecidos para um novo registro de @see LinceTips no banco de dados
     * @param {Object} options - objeto com os dados de um registro @see LinceTips
     * @returns {Object} - registro @see LinceTips que foi inserido
     */
    async insert(options) {
        let { payload } = options

        return await models.LinceTips.create({
            id: uuid(),
            titulo: payload.titulo,
            descricao: payload.descricao,
            link: payload.link,
            status: payload.status,
            perfil_id: payload.perfil_id
        })
            .then(result => {
                result = result.get({ plain: true })
                return result
            })
            .catch(error => {
                if (error.fields.perfil_id !== '')
                    throw new Error(`Dica já cadastrada!`)
                throw error
            })
    }

    /**
     * Atualiza os dados de um registro @see LinceTips no banco de dados
     * @param {Object} options - objeto com os dados a atualizar do registro de @see LinceTips
     * @returns {Object} - objeto com os dados do registro de @see LinceTips atualizados
     */
    async update(payload) {
        return await models.LinceTips.update(
            payload,
            {
                where: { id: payload.id },
                returning: true,
                raw: true,
            })
            .then(result => {
                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar dica')
                return result[1][0]
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Exclui um registro de @see LinceTips no banco de dados
     * @param {Object} options - objeto com os dados para remoção @see LinceTips
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {

        return await models.LinceTips.destroy({
            plain: true, returning: true,
            where: { id: options.id }
        })
            .then(result => {
                if (result !== 1)
                    throw new Error('Erro ao deletar dica')
                return result
            })
            .catch(error => {
                throw error
            })
    }
}