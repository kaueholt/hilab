'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

/**
 * Classe responsável por operar as interações com a tabela de @see LinceUserProfiles na base de dados
 */
module.exports = class CompanyRepository {
    /**
     * Construtor da classe
     * @param 
     */
    constructor() { }

    /**
    * Adquire uma lista de registros de @see LinceUserProfiles no banco de dados
    * @param {Array} fields - array com os campos desejados para cada registro
    * @param {Object} options - objeto com parametros de busca
    * @param {Boolean} usuario - boolean para informar se deseja capturar os dados do usuário
    * @param {Boolean} perfil - boolean para informar se deseja capturar os dados do perfil
    * @returns {Array} - lista de registros de @see LinceUserProfiles
    */
    async index(fields, options, usuario = false, perfil = false) {

        let data = {
            raw: true,
            attributes: fields,
            limit: 10,
            offset: 0,
            query: options.query,
            nest: true,
            include: (usuario === true && {
                model: models.LinceUsers,
                as: 'usuarioLince',
                attributes: ['id', 'cpf', 'status'],
            }) || (perfil === true && {
                model: models.LinceProfiles,
                as: 'perfil',
                attributes: ['id', 'descricao'],
            }),
        }

        return await models.LinceUserProfiles.findAll(data)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Consulta um registro de @see LinceUserProfiles na base de dados
    * @param {Array} fields - array com os campos desejados para cada registro
    * @param {Object} options - objeto com parametros de busca
    * @returns {Object} - registro de @see LinceUserProfiles
    */
    async info(fields, options) {

        let data = {
            id: null,
            status: null,
            query: options.query,
            search: options.search,
            attributes: fields
        }

        let reg = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
        let isUuid = reg.exec(data.search)

        isUuid ? data.id = data.search : null

        return await models.LinceUserProfiles.findOne({
            raw: true,
            plain: true,
            nest: true,
            include: data.query.usuario === true && {
                model: models.LinceUsers, attributes: {
                    exclude: ['created_at', 'updated_at']
                }
            },
            where: {
                [Op.or]: [
                    { status: { [Op.eq]: data.status } },
                    { id: { [Op.eq]: data.id } }
                ]
            },
        })
            .then(result => {

                if (!result)
                    throw new Error('Usuário-Perfil não encontrado')
                else
                    return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Insere um registro de @see LinceUserProfiles na base de dados
    * @param {Object} options - dados para um novo registro de @see LinceUserProfiles
    * @returns {Object} - dados do registro de @see LinceUserProfiles que foi inserido
    */
    async insert(options) {
        let { id, status, origemCadastro, perfil_id, lince_usuario_id }  = options.payload

        return await models.LinceUserProfiles.create({
            id: uuid(), id, status, origemCadastro,
            perfil_id, lince_usuario_id
        })
            .then(result => result.get({ plain: true }))
            .catch(error => {
                if (error.fields.status !== '')
                    throw new Error(`Usuário-Perfil já cadastrado!`)
                throw error
            })
    }

    /**
    * Atualiza o registro de @see LinceUserProfiles na base de dados
    * @param {Object} options - dados para atualização de um registro de @see LinceUserProfiles
    * @returns {Object} - registro de @see LinceUserProfiles com os dados atualizados
    */
    async update(options) {
        let { payload } = options
        return await models.LinceUserProfiles.update(
            payload,
            {
                where: { status: options.search },
                returning: true,
                raw: true,
            })
            .then(result => {
                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar usuário - perfil')

                return result[1][0]
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Remove o registro de @see LinceUserProfiles na base de dados
    * @param {Object} options - objeto de @see LinceUserProfiles com informações do usuário - perfil
    * @returns {int} - quantidade de registros removidos
    */
    async delete(options) {

        return await models.LinceUserProfiles.destroy({
            plain: true, returning: true,
            where: { status: options.search }
        })
            .then(result => {

                if (result !== 1)
                    throw new Error('Erro ao deletar usuário - perfil')

                return result
            })
            .catch(error => {
                throw error
            })
    }
}