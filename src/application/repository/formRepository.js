'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

/**
 * Classe responsável por operar as interações com a tabela de @see Formularies
 * na base de dados
 */
module.exports = class FormRepository {

    /**
     * Construtor da classe
     */
    constructor() { }

    /**
    * Adquire uma lista de registros de @see Formularies no banco de dados
    * @param {Array} attrs - array com os campos desejados para cada registro
    * @param {Object} options.query - opções de pesquisa
    * @param {Object} options.headers - opções header da requisição
    * @returns {Array} - lista de registros de @see Formularies
    */
    async index(options, attrs) {

        let where = {}

        let searchByEquipModel = {
            [Op.and]: { equipment_model_id: options.query.equipment_model_id }
        }

        let searchByExamClass = {
            [Op.and]: { exam_class_id: options.query.exam_class_id }
        }

        let searchByName = {
            [Op.like]: { name: options.query.name }
        }

        let searchByStatus = {
            [Op.and]: { status: options.query.status }
        }

        if (options.query.equipment_model_id !== '')
            where = Object.assign({}, where, searchByEquipModel)

        if (options.query.exam_class_id !== '')
            where = Object.assign({}, where, searchByExamClass)

        if (options.query.name !== '')
            where = Object.assign({}, where, searchByName)

        if (options.query.status !== '')
            where = Object.assign({}, where, searchByStatus)

        let searchOptions = {
            where,
            raw: true,
            nest: true,
            attributes: attrs,
            include: [{
                model: models.ExamClass,
                as: 'exam_class',
                attributes: { exclude: ['created_at', 'updated_at'] }
            },
            {
                model: models.EquipmentModels,
                as: 'equipment_model',
                attributes: { exclude: ['created_at', 'updated_at'] }
            }],
        }

        return await models.Formularies.findAll(searchOptions)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Consulta um registro de @see Formularies no banco de dados
    * @param {Array} attrs - array com os campos desejados do registro
    * @param {Object} options.query - opções de pesquisa
    * @param {Object} options.headers - opções header da requisição
    * @returns {Object} - objecto com o registro encontrado de @see Formularies
    */
    async info(options, attrs) {

        let search = {
            where: { id: options.query.id },
            raw: true,
            nest: true,
            attributes: attrs,
            include: [
                {
                    model: models.ExamClass,
                    as: 'exam_class',
                    attributes: { exclude: ['created_at', 'updated_at'] }
                },
                {
                    model: models.EquipmentModels,
                    as: 'equipment_model',
                    attributes: { exclude: ['created_at', 'updated_at'] }
                }
            ]
        }
        return await models.Formularies.findOne(search)
            .then(async result => {
                if (!result)
                    throw new Error('Formulário não encontrado!')

                if (options.query.topics) {
                    options.query.formulary_id = options.query.id
                    result.topics = []
                    const topicsData = await this.indexTopics(options, [
                        'description', 'id', 'sequence', 'formulary_id',
                        'status'
                    ])

                    if (topicsData.length > 0)
                        topicsData.forEach(topic => result.topics.push(topic))
                }

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Insere um registro de @see Formularies no banco de dados
    * @param {Object} options.headers - opções header da requisição
    * @param {Object} options.payload - dados payload para inserção
    * @returns {Object} - objeto inserido no banco de dados de @see Formularies
    */
    async insert(options) {
        return await models.Formularies.create({
            id: uuid(),
            exam_class_id: options.payload.exam_class_id,
            equipment_model_id: options.payload.equipment_model_id,
            status: options.payload.status || 1,
            name: options.payload.name,
            description: options.payload.description
        })
            .then(result => result.get({ plain: true }))
            .catch(error => {
                if (error.fields.name !== '')
                    throw new Error('Nome de formulário já existe!')

                if (error.fields.description !== '')
                    throw new Error('Descrição de formulário já existe!')

                throw error
            });
    }

    /**
    * Atualiza um registro de @see Formularies no banco de dados
    * @param {Object} options.headers - opções header da requisição
    * @param {Object} options.payload - dados payload para inserção
    * @returns {Object} - objeto atualizado no banco de dados de @see Formularies
    */
    async update(options) {
        let { payload } = options

        return await models.Formularies.update(
            payload,
            {
                where: { id: options.query.id },
                returning: true,
                raw: true,
            })
            .then(result => {
                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar formulário')

                return result[1][0]
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Remove um registro de @see Formularies no banco de dados
    * @param {Object} options.headers - opções header da requisição
    * @param {Object} options.query - dados de busca para deletar registro
    * @returns {Int} - quantidade de registros deletados no banco de dados de @see Formularies
    */
    async delete(options) {
        return await models.Formularies.destroy({ where: { id: options.query.id } })
            .then(result => {
                if (result !== 1)
                    throw new Error('Erro ao deletar formulário')

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Insere um registro de @see TopicForms no banco de dados
    * @param {Object} options.headers - opções header da requisição
    * @param {Object} options.payload - dados payload para inserção
    * @returns {Object} - objeto inserido no banco de dados de @see TopicForms
    */
    async insertTopic(options) {
        return await models.TopicForms.create({
            id: uuid(),
            formulary_id: options.payload.formulary_id,
            sequence: options.payload.sequence,
            description: options.payload.description,
            status: options.payload.status,
        }).then(result => result.get({ plain: true }))
            .catch(error => {
                if (error.fields.description !== '')
                    throw new Error('Nome de tópico já existe!')

                throw error
            });
    }

    async infoTopic(options, attrs) {
        let data = {
            where: { id: options.query.id },
            raw: true,
            nest: true,
            attributes: attrs,
        }
        return await models.TopicForms.findOne(data)
            .then(async result => {
                if (!result)
                    throw new Error('Tópico não encontrado!')

                if (options.query.questions) {
                    result.questions = []
                    const questionsData = await this.indexQuestions(options, [
                        'description', 'id', 'sequence', 'formulary_id',
                        'type', 'topic_form_id', 'complement', 'status'
                    ])

                    if (questionsData.length > 0)
                        questionsData.forEach(question => result.questions.push(question));
                }

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Adquire uma lista de registros de @see TopicForms no banco de dados
    * @param {Array} attrs - array com os campos desejados para cada registro
    * @param {Object} options.query - opções de pesquisa
    * @param {Object} options.headers - opções header da requisição
    * @returns {Array} - lista de registros de @see TopicForms
    */
    async indexTopics(options, attrs) {

        let where = {}

        let searchByFormularyId = {
            [Op.and]: { formulary_id: options.query.formulary_id }
        }

        let searchByDescription = {
            [Op.like]: { description: options.query.description }
        }

        if (options.query.formulary_id && options.query.formulary_id !== '')
            where = Object.assign({}, where, searchByFormularyId)

        if (options.query.description && options.query.description !== '')
            where = Object.assign({}, where, searchByDescription)

        let searchOptions = {
            where,
            raw: true,
            nest: true,
            attributes: attrs,
        }

        return await models.TopicForms.findAll(searchOptions)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
   * Remove um registro de @see TopicForms no banco de dados
   * @param {Object} options.headers - opções header da requisição
   * @param {Object} options.query - dados de busca para deletar registro
   * @returns {Int} - quantidade de registros deletados no banco de dados de @see TopicForms
   */
    async deleteTopic(options) {
        return await models.TopicForms.destroy({ where: { id: options.query.id } })
            .then(result => {
                if (result !== 1)
                    throw new Error('Erro ao deletar tópico')

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Atualiza um registro de @see TopicForms no banco de dados
    * @param {Object} options.headers - opções header da requisição
    * @param {Object} options.payload - dados payload para inserção
    * @returns {Object} - objeto atualizado no banco de dados de @see TopicForms
    */
    async updateTopic(options) {
        let { payload } = options

        return await models.TopicForms.update(
            payload,
            {
                where: { id: options.query.id },
                returning: true,
                raw: true,
            })
            .then(result => {
                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar tópico')
                return result[1][0]
            })
            .catch(error => {
                if (error.fields.description !== '')
                    throw new Error('Nome de tópico já existe!')
                throw error
            })
    }

    /**
    * Insere um registro de @see FormQuestion no banco de dados
    * @param {Object} options.headers - opções header da requisição
    * @param {Object} options.payload - dados payload para inserção
    * @returns {Object} - objeto inserido no banco de dados de @see FormQuestion
    */
    async insertQuestion(options) {
        return await models.Questions.create({
            id: uuid(),
            formulary_id: options.payload.formulary_id,
            topic_form_id: options.payload.topic_form_id,
            sequence: options.payload.sequence,
            complement: options.payload.complement,
            description: options.payload.description,
            type: options.payload.type,
            status: options.payload.status
        })
            .then(result => result.get({ plain: true }))
            .catch(error => {
                if (error.fields.description !== '')
                    throw new Error('Nome de campo já existe!')
                throw error
            });
    }

    /**
    * Consulta um registro de @see FormQuestion no banco de dados
    * @param {Array} attrs - array com os campos desejados do registro
    * @param {Object} options.query - opções de pesquisa
    * @param {Object} options.headers - opções header da requisição
    * @returns {Object} - objecto com o registro encontrado de @see FormQuestion
    */
    async infoQuestion(options, attrs) {
        let data = {
            where: { id: options.query.id },
            raw: true,
            nest: true,
            attributes: attrs,
        }
        return await models.Questions.findOne(data)
            .then(async result => {
                if (!result)
                    throw new Error('Campo não encontrado!')

                if (options.query.question_fields) {
                    options.query.question_id = options.query.id
                    result.question_fields = []
                    const questionsItemsData = await this.indexQuestionFields(options, [
                        'description', 'id', 'sequence', 'formulary_id',
                        'status', 'value', 'question_id', 'complement',
                    ])

                    if (questionsItemsData.length > 0)
                        questionsItemsData.forEach(field => result.question_fields.push(field));
                }
                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Adquire uma lista de registros de @see FormQuestion no banco de dados
    * @param {Array} attrs - array com os campos desejados para cada registro
    * @param {Object} options.query - opções de pesquisa
    * @param {Object} options.headers - opções header da requisição
    * @returns {Array} - lista de registros de @see FormQuestion
    */
    async indexQuestions(attrs, options) {

        let where = {}

        let searchByFormularyId = {
            [Op.and]: { formulary_id: options.query.formulary_id }
        }

        let searchByTopicId = {
            [Op.and]: { topic_form_id: options.query.topic_form_id }
        }

        if (options.query.topic_form_id && options.query.topic_form_id !== '')
            where = Object.assign({}, where, searchByTopicId)

        if (options.query.formulary_id && options.query.formulary_id !== '')
            where = Object.assign({}, where, searchByFormularyId)

        let searchOptions = {
            where,
            raw: true,
            nest: true,
            attributes: attrs,
        }

        return await models.Questions.findAll(searchOptions)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Atualiza um registro de @see FormQuestion no banco de dados
    * @param {Object} options.headers - opções header da requisição
    * @param {Object} options.payload - dados payload para inserção
    * @returns {Object} - objeto atualizado no banco de dados de @see FormQuestion
    */
    async updateQuestion(options) {
        let { payload } = options

        return await models.Questions.update(
            payload,
            {
                where: { id: options.query.id },
                returning: true,
                raw: true,
            })
            .then(result => {
                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar campo')

                return result[1][0]
            })
            .catch(error => {
                if (error.fields.description !== '')
                    throw new Error('Descrição de campo já existe!')
                throw error
            })
    }

    /**
    * Remove um registro de @see FormQuestion no banco de dados
    * @param {Object} options.headers - opções header da requisição
    * @param {Object} options.query - dados de busca para deletar registro
    * @returns {Int} - quantidade de registros deletados no banco de dados de @see FormQuestion
    */
    async deleteQuestion(options) {
        return await models.Questions.destroy({ where: { id: options.query.id } })
            .then(result => {
                if (result !== 1)
                    throw new Error('Erro ao deletar campo')

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Insere um registro de @see FormQuestionItem no banco de dados
    * @param {Object} options.headers - opções header da requisição
    * @param {Object} options.payload - dados payload para inserção
    * @returns {Object} - objeto inserido no banco de dados de @see FormQuestionItem
    */
    async insertQuestionField(options) {
        return await models.QuestionItems.create({
            id: uuid(),
            formulary_id: options.payload.formulary_id,
            topic_form_id: options.payload.topic_form_id,
            question_id: options.payload.question_id,
            sequence: options.payload.sequence,
            complement: options.payload.complement,
            description: options.payload.description,
            value: options.payload.value,
            status: options.payload.status
        })
            .then(result => result.get({ plain: true }))
            .catch(error => {
                if (error.fields.description !== '')
                    throw new Error('Nome de item já existe!')
                throw error
            });
    }

    /**
    * Consulta um registro de @see FormQuestionItem no banco de dados
    * @param {Array} attrs - array com os campos desejados do registro
    * @param {Object} options.query - opções de pesquisa
    * @param {Object} options.headers - opções header da requisição
    * @returns {Object} - objecto com o registro encontrado de @see FormQuestionItem
    */
    async infoQuestionField(options, attrs) {
        let data = {
            where: { id: options.query.id },
            raw: true,
            nest: true,
            attributes: attrs
        }
        return await models.QuestionItems.findOne(data)
            .then(async result => {
                if (!result)
                    throw new Error('Item não encontrado!')
                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Adquire uma lista de registros de @see FormQuestionItem no banco de dados
    * @param {Array} attrs - array com os campos desejados para cada registro
    * @param {Object} options.query - opções de pesquisa
    * @param {Object} options.headers - opções header da requisição
    * @returns {Array} - lista de registros de @see FormQuestionItem
    */
    async indexQuestionFields(attrs, options) {

        let where = {}

        let searchByFormularyId = {
            [Op.and]: { formulary_id: options.formulary_id }
        }

        let searchByTopicId = {
            [Op.and]: { topic_form_id: options.query.topic_form_id }
        }

        let searchByQuestionId = {
            [Op.and]: { question_id: options.query.question_id }
        }

        if (options.equipment_model_id && options.equipment_model_id !== '')
            where = Object.assign({}, where, searchByFormularyId)

        if (options.query.topic_form_id && options.query.topic_form_id !== '')
            where = Object.assign({}, where, searchByTopicId)

        if (options.query.question_id && options.query.question_id !== '')
            where = Object.assign({}, where, searchByQuestionId)

        let searchOptions = {
            where,
            raw: true,
            nest: true,
            attributes: attrs,
        }

        return await models.QuestionItems.findAll(searchOptions)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Atualiza um registro de @see FormQuestionItem no banco de dados
    * @param {Object} options.headers - opções header da requisição
    * @param {Object} options.payload - dados payload para inserção
    * @returns {Object} - objeto atualizado no banco de dados de @see FormQuestionItem
    */
    async updateQuestionField(options) {
        let { payload } = options

        return await models.QuestionItems.update(
            payload,
            {
                where: { id: options.query.id },
                returning: true,
                raw: true,
            })
            .then(result => {
                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar item')

                return result[1][0]
            })
            .catch(error => {
                if (error.fields.description !== '')
                    throw new Error('Descrição de item já existe!')
                throw error
            })
    }

    /**
    * Remove um registro de @see FormQuestionItem no banco de dados
    * @param {Object} options.headers - opções header da requisição
    * @param {Object} options.query - dados de busca para deletar registro
    * @returns {Int} - quantidade de registros deletados no banco de dados de @see FormQuestionItem
    */
    async deleteQuestionField(options) {
        return await models.QuestionItems.destroy({ where: { id: options.query.id } })
            .then(result => {
                if (result !== 1)
                    throw new Error('Erro ao deletar item')
                return result
            })
            .catch(error => {
                throw error
            })
    }
}
