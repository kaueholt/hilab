'use strict'

const models = require('../models');
const uuid = require('uuid/v4')

/**
 * Classe responsável por operar as interações com a tabela de @see Sessions na base de dados
 */
module.exports = class AuthenticationRepository {

    /**
     * Construtor da classe
     * @param
     */
    constructor() { }

    /**
     * Adquire uma lista de registros de @see Sessions no banco de dados
     * @param {Array} fields - array com os registros de @see Sessions
     * @param {Object} where - objeto com parametros de busca
     * @returns {Array} - array com os registros de @see Sessions
     */
    async index(fields, where) {

        let data = {
            raw: true,
            attributes: fields,
            where: where
        }
        return await models.Sessions.findAll(data)
            .then((result) => result)
            .catch((error) => {
                throw error
            })
    }

    /**
     * Adquire os dados de um registro de @see Sessions registrado no banco de dados
     * @param {Object} data.session_id - objeto com o campo id de um registro de @see Sessions
     * @returns {Object} - registro de @see Sessions
     */
    async info(data) {

        return await models.Sessions.findAll({
            raw: true,
            where: { id: data.session_id },
            limit: 1
        })
            .then(result => {
                if (!result)
                    throw new Error('401')

                if (result.length > 0)
                    return result[0];

                throw new Error('401')
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere um novo registro de @see Sessions no banco de dados
     * @param {Object} data - objeto com os dados de @see Sessions
     * @returns {Object} - registro de @see Sessions inserido no banco
     */
    async insert(data) {

        return await models.Sessions.create({
            id: uuid(),
            user_id: data.user_id,
            os_name: data.os_name,
            os_version: data.os_version,
            device: data.device,
            browser: data.browser,
            browser_version: data.browser_version,
            browser_engine: data.browser_engine,
            browser_engine_version: data.browser_engine_version
        })
            .then((result) => result.dataValues)
            .catch((error) => {
                throw error
            })
    }

    /**
     * Exclui um ou um conjunto de registros de @see Sessions no banco de dados
     * @param {Object} data.id - objeto com id de @see User para remover todos os registros de @see Sessions
     * @returns {int} - quantidade de registros removidos
     */
    async delete(data) {

        return await models.Sessions.destroy({
            where: { user_id: data.loggedUser.id },
            raw: true
        })
            .then((result) => result)
            .catch((error) => {
                throw error
            })
    }

}