'use strict'

const models = require('../models');
const uuid = require('uuid/v4')

/**
 * Classe responsável por operar as interações com a tabela de @see Addresses na base de dados
 */
module.exports = class AddressRepository {
  /**
    * Construtor da classe
    * @param
    */
  constructor() { }

  /**
  * Adquire uma lista de registros de @see Addresses no banco de dados
  * @param {Array} fields - array com os campos desejados para cada registro
  * @param {Object} where - objeto com parametros de busca
  * @returns {Array} - lista de registros de @see Addresses
  */
  async index(fields, where) {

    let data = { raw: true, attributes: fields, where }

    return await models.Addresses.findAll(data)
      .then(result => result)
      .catch(error => {
        throw error
      })
  }

  /**
    * Consulta um registro de @see Addresses na base de dados
    * @param {String} company_id - uuid de um registro de @see Company
    * @returns {Object} - dados de um registro de @see Addresses
    */
  async info(company_id) {

    return await models.Addresses.findAll({
      raw: true, where: { company_id }, limit: 1
    })
      .then(result => result)
      .catch(error => {
        throw error
      })
  }

  /**
   * Insere um novo registro de @see Addresses na base de dados
   * @param {Object} options - dados de um novo registro de @see Addresses
   * @returns {Object} - objeto com os dados do registro de @see Addresses que foi inserido
   */
  async insert(options) {
    let { company_id, street, number, complement,
      district, city, state, zipcode } = options.payload.address

    return await models.Addresses.create({
      id: uuid(), company_id, street, number,
      complement, district, city, state, zipcode
    })
      .then(result => result)
      .catch(error => {
        throw error
      })
  }

  /**
   * Atualiza um registro de @see Addresses na base de dados
   * @param {Object} options - dados para atualização de um registro de @see Addresses
   * @returns {Object} - dados de um registro de @see Addresses atualizados
   */
  async update(options) {
    let { payload } = options

    return await models.Addresses.update(
      payload,
      {
        where: { id: options.search },
        returning: true,
        raw: true,
      })
      .then(result => {

        if (result[0] !== 1)
          throw new Error('Erro ao atualizar endereço')

        return result[1][0]
      })
      .catch(error => {
        throw error
      })
  }

  /**
   * Remove um registro de @see Addresses na base de dados
   * @param {String} id - uuid de um registro de @see Addresses
   * @returns {int} - quantidade de registros removidos
   */
  async delete(id) {

    return await models.Addresses.destroy({ where: { id } })
      .then(result => result)
      .catch(error => {
        throw error
      })
  }
}