'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

/**
 * Classe responsável por operar as interações com a tabela de @see Users na base de dados
 */
module.exports = class UserRepository {

    /**
     * Construtor da classe
     * @param 
     */
    constructor() { }

    /**
     * Adquire uma lista de @see Users no banco de dados
     * @param {Array} fields - array com os campos desejados para cada registro
     * @param {Object} options - objeto com parametros de busca
     * @returns {Array} - array de registros do tipo @see Users
     */
    async index(fields, options) {
        let data = {
            raw: true,
            attributes: fields,
            limit: 10,
            offset: 0,
        }

        return await models.Users.findAll(data)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire os dados de um registro de @see Users no banco de dados
     * @param {Object} options - objeto com os dados para efetuar a busca @see Users
     * @returns {Object} - um registro de @see Users
     */
    async info(fields, options) {
        let data = {
            id: null,
            cpf: null,
            search: options.search,
            attributes: fields
        }

        let reg = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
        let isUuid = reg.exec(options.search)

        isUuid ? data.id = data.search : data.cpf = data.search

        return await models.Users.findOne({
            raw: true,
            plain: true,
            where: {
                [Op.or]: [
                    { cpf: { [Op.eq]: data.cpf } },
                    { id: { [Op.eq]: data.id } }
                ]
            },
        })
            .then(result => {

                if (!result)
                    throw new Error('Usuário não encontrado')
                else
                    return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere os dados fornecidos para um novo registro de @see Users no banco de dados
     * @param {Object} options - objeto com os dados de um registro @see Users
     * @returns {Object} - registro @see Users que foi inserido
     */
    async insert(options) {
        let { payload } = options

        return await models.Users.create({
            id: uuid(),
            name: payload.name,
            cpf: payload.cpf,
            password: payload.password,
            token: payload.token,
            email: payload.email,
            date_birth: payload.date_birth,
            avatar: payload.avatar,
            status: payload.status,
            role: payload.role
        })
            .then(result => {
                result = result.get({ plain: true })
                delete result.password && delete result.token
                return result
            })
            .catch(error => {
                if (error.fields.cpf !== '')
                    throw new Error(`Usuário já cadastrado!`)

                throw error
            })
    }

    /**
     * Atualiza os dados de um registro @see Users no banco de dados
     * @param {Object} options - objeto com os dados a atualizar do registro de @see Users
     * @returns {Object} - objeto com os dados do registro de @see Users atualizados
     */
    async update(options) {
        let { payload } = options

        return await models.Users.update(
            payload,
            {
                where: { cpf: options.search },
                returning: true,
                raw: true,
            })
            .then(result => {

                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar usuário')

                delete result[1][0].password && delete result[1][0].token
                return result[1][0]
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Exclui um registro de @see Users no banco de dados
     * @param {Object} options - objeto com os dados para remoção @see Users
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {

        return await models.Users.destroy({
            plain: true, returning: true,
            where: { cpf: options.search }
        })
            .then(result => {

                if (result !== 1)
                    throw new Error('Erro ao deletar modelo')

                return result
            })
            .catch(error => {
                throw error
            })
    }
}