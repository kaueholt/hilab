'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;


/**
 * Classe responsável por operar as interações com as tabelas de @see LinceProfiles e @see LinceTips
 * na base de dados
 */
module.exports = class LinceProfileRepository {

    /**
     * Construtor da classe
     */
    constructor() { }

    /**
     * Adquire uma lista de registros de @see LinceProfiles no banco de dados
     * @param {Array} attributes - array com os campos desejados para cada registro
     * @returns {Array} - lista de registros de @see LinceProfiles
     */
    async index(attributes) {

        let profileOptions = {
            raw: true,
            nest: true,
            attributes: attributes
        }

        return await models.LinceProfiles.findAll(profileOptions)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Consulta um registro de @see LinceProfiles na base de dados
    * @param {String} uuid - UUID de um registro de @see LinceProfiles para efetuar a busca
    * @param {Array} attributes - array com os campos desejados para o registro
    * @returns {Object} - registro de @see LinceProfiles
    */
    async info(id, attributes = ['id', 'descricao']) {
        let profileOptions = {
            where: { id },
            raw: true,
            nest: true,
            attributes: attributes
        }


        return await models.LinceProfiles.findOne(profileOptions)
            .then(async result => {
                if (!result)
                    throw new Error('Perfil não encontrado!')

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere registros de perfís no banco de dados.
     * @param {Object} profile - Dados do perfil @see LinceProfiles
     * @param {String} profile.id - Id do perfil, para apenas adquirir os dados do perfil
     * @param {String} profile.descricao - Descrição do perfil para inserção
     */
    async insertProfile(profile) {
        let profileFound = {}
        if (profile.id) {
            profileFound = await this.info(profile.id, true)
                .then(result => result)
                .catch(error => {
                    throw new Error('Perfil não encontrado')
                })
        } else {
            profileFound = await models.LinceProfiles.create({
                id: uuid(),
                descricao: profile.descricao,
            }).then(insertedProfile => insertedProfile.get({ plain: true })).catch(error => {
                if (error.parent.code == '23503')
                    error = new Error('Perfil não encontrada!')

                throw error
            });
        }

        return profileFound;
    }

    /**
     * Adquire uma lista de registros de @see LinceTips no banco de dados
     * @param {Array} profile_id - UUID do perfil
     * @returns {Array} - lista de registros de @see LinceTips
     */
    async indexTips(profile_id) {
        return await models.LinceTips.findAll(
            {
                raw: true,
                nest: true,
                attributes: [
                    'id', 'titulo', 'descricao', 'link',
                    'status', 'perfil_id'
                ],
                where: { perfil_id: profile_id, status: 1 },
                order: [['perfil_id', 'ASC']],
            }
        )
            .then(tips => {
                return tips
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Atualiza um registro de @see LinceProfiles na base de dados
    * @param {Object} profile - dados para atualização de um registro de @see LinceProfiles
    * @param {String} profile.id - UUID do perfil que será atualizado
    * @param {String} profile.descricao - nome do perfil
    * @returns {Object} - registro de @see LinceProfiles com os dados atualizados
    */
    async update(profile) {
        const { descricao } = profile

        return await models.LinceProfiles.update(
            { descricao },
            {
                where: { id: profile.id }, returning: true, raw: true, plain: true
            })
            .then(result => {
                return result[1]
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Remove um registro de @see LinceProfiles na base de dados
    * @param {String} id - ID do registro @see LinceProfiles para efetuar a busca
    * @returns {int} - quantidade de registros removidos
    */
    async deleteProfile(id) {
        return await models.LinceProfiles.destroy({ where: { id } })
            .then(result => {
                if (result !== 1) {
                    throw new Error('Erro ao deletar Perfil')
                }

				return result
            })
            .catch(error => {
                throw error
            })
    }
}