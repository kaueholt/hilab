'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

/**
 * Classe responsável por operar as interações com a tabela de @see Workers na base de dados
 */
module.exports = class WorkerRepository {

	/**
	 * Construtor da classe
	 * @param 
	 */
	constructor() { }

	/**
		 * Adquire uma lista de @see Workers no banco de dados
		 * @param {Array} fields - array com os campos desejados para cada registro
		 * @param {Object} options - objeto com parametros de busca
		 * @returns {Array} - array de registros do tipo @see Workers
		 */
	async index(fields, options) {

		let data = {
			search: options.search,
			raw: true,
			nest: true,
			query: options.query,
			include: options.query.company === true && {
				model: models.Companies,
				attributes: { exclude: ['created_at', 'updated_at'] },
				where: { id: options.search }
			},
		}

		return await models.Workers.findAll(
			data,
			{ where: { company_id: data.search } })
			.then(result => result)
			.catch(error => {
				throw error
			})
	}

	/**
	 * Adquire os dados de um registro de @see Workers no banco de dados
	 * @param {Array} fields - array com os campos desejados para cada registro
	 * @param {Object} options - objeto com os dados para efetuar a busca @see Workers
	 * @returns {Object} - um registro de @see Workers
	 */
	async info(fields, options) {

		options = {
			id: null,
			cpf: null,
			search: options.search,
			attributes: fields
		}

		let reg = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
		let isUuid = reg.exec(options.search)

		isUuid ? options.id = options.search : options.cpf = options.search

		return await models.Workers.findOne({
			raw: true,
			plain: true,
			where: {
				[Op.or]: [
					{ cpf: { [Op.eq]: options.cpf } },
					{ id: { [Op.eq]: options.id } }
				]
			},
		})
			.then(result => {

				if (!result)
					throw new Error('Colaborador não encontrado')
				else
					return result
			})
			.catch(error => {
				throw error
			})
	}

	/**
	 * Insere os dados fornecidos para um novo registro de @see Workers no banco de dados
	 * @param {Object} options - objeto com os dados de um registro @see Workers
	 * @returns {Object} - registro @see Workers que foi inserido
	 */
	async insert(options) {
		let { payload } = options

		return await models.Workers.create({
			id: uuid(),
			name: payload.name,
			cpf: payload.cpf,
			gender: payload.gender,
			race: payload.race,
			pis_nit: payload.pis_nit,
			email: payload.email,
			phone: payload.phone,
			date_birth: payload.date_birth,
			status: payload.status,
			avatar: payload.avatar,
			company_id: payload.company_id,
			rfid: payload.rfid,
			observation: payload.observation,
			role: payload.role
		})
			.then(result => result.get({ plain: true }))
			.catch(error => {

				if (error.fields.cpf)
					throw new Error(`Colaborador já cadastrado!`)

				if (error.fields.pis_nit)
					throw new Error(`PIS/NIT já cadastrado!`)

				throw error
			})
	}

	/**
	 * Atualiza os dados de um registro @see Workers no banco de dados
	 * @param {Object} options - objeto com os dados a atualizar do registro de @see Workers
	 * @returns {Object} - objeto com os dados do registro de @see Workers atualizados
	 */
	async update(options) {
		let { payload } = options

		return await models.Workers.update(
			payload,
			{
				where: { cpf: options.search },
				returning: true,
				raw: true,
			})
			.then(result => {

				if (result[0] !== 1)
					throw new Error('Erro ao atualizar colaborador')

				delete result[1][0].password && delete result[1][0].token
				return result[1][0]
			})
			.catch(error => {
				throw error
			})
	}

	/**
	 * Exclui um registro de @see Workers no banco de dados
	 * @param {Object} options - objeto com os dados para remoção @see Workers
	 * @returns {int} - quantidade de registros removidos
	 */
	async delete(options) {

		return await models.Workers.destroy({
			plain: true, returning: true,
			where: { cpf: options.search }
		})
			.then(result => {

				if (result !== 1)
					throw new Error('Erro ao deletar colaborador')

				return result
			})
			.catch(error => {
				throw error
			})
	}
}