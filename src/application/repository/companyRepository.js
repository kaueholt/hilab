'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

/**
 * Classe responsável por operar as interações com a tabela de @see Companies na base de dados
 */
module.exports = class CompanyRepository {
    /**
     * Construtor da classe
     * @param 
     */
    constructor() { }

    /**
    * Adquire uma lista de registros de @see Companies no banco de dados
    * @param {Array} fields - array com os campos desejados para cada registro
    * @param {Object} options - objeto com parametros de busca
    * @returns {Array} - lista de registros de @see Companies
    */
    async index(fields, options) {

        let data = {
            raw: true,
            attributes: fields,
            limit: 10,
            offset: 0,
            query: options.query,
            nest: true,
            include: options.query.addresses === true && {
                model: models.Addresses, attributes: {
                    exclude: ['created_at', 'updated_at', 'CompanyId']
                }
            },
        }

        return await models.Companies.findAll(data)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Consulta um registro de @see Companies na base de dados
    * @param {Array} fields - array com os campos desejados para cada registro
    * @param {Object} options - objeto com parametros de busca
    * @returns {Object} - registro de @see Companies
    */
    async info(fields, options) {

        let data = {
            id: null,
            cnpj: null,
            query: options.query,
            search: options.search,
            attributes: fields
        }

        let reg = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
        let isUuid = reg.exec(data.search)

        isUuid ? data.id = data.search : data.cnpj = data.search

        return await models.Companies.findOne({
            raw: true,
            plain: true,
            nest: true,
            include: data.query.addresses === true && {
                model: models.Addresses, attributes: {
                    exclude: ['created_at', 'updated_at', 'CompanyId']
                }
            },
            where: {
                [Op.or]: [
                    { cnpj: { [Op.eq]: data.cnpj } },
                    { id: { [Op.eq]: data.id } }
                ]
            },
        })
            .then(result => {

                if (!result)
                    throw new Error('Empresa não encontrada')
                else
                    return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Insere um registro de @see Companies na base de dados
    * @param {Object} options - dados para um novo registro de @see Companies
    * @returns {Object} - dados do registro de @see Companies que foi inserido
    */
    async insert(options) {
        let { name, cnpj, email, phone, foundation_date, status } = options.payload

        return await models.Companies.create({
            id: uuid(), name, cnpj, email,
            phone, foundation_date, status
        })
            .then(result => result.get({ plain: true }))
            .catch(error => {

                if (error.fields.cnpj !== '')
                    throw new Error(`Empresa já cadastrada!`)

                throw error
            })
    }

    /**
    * Atualiza o registro de @see Companies na base de dados
    * @param {Object} options - dados para atualização de um registro de @see Companies
    * @returns {Object} - registro de @see Companies com os dados atualizados
    */
    async update(options) {
        let { payload } = options

        return await models.Companies.update(
            payload,
            {
                where: { cnpj: options.search },
                returning: true,
                raw: true,
            })
            .then(result => {
                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar empresa')

                return result[1][0]
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Remove o registro de @see Companies na base de dados
    * @param {Object} options - objeto de @see Companies com informações da empresa
    * @returns {int} - quantidade de registros removidos
    */
    async delete(options) {

        return await models.Companies.destroy({
            plain: true, returning: true,
            where: { cnpj: options.search }
        })
            .then(result => {

                if (result !== 1)
                    throw new Error('Erro ao deletar empresa')

                return result
            })
            .catch(error => {
                throw error
            })
    }
}