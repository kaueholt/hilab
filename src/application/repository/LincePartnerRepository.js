'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

/**
 * Classe responsável por operar as interações com a tabela de @see LincePartnerRepository na base de dados
 */
module.exports = class LincePartnerRepository {

    /**
     * Construtor da classe
     * @param 
     */
    constructor() { }

    /**
     * Adquire uma lista de @see LincePartnerRepository no banco de dados
     * @param {Array} fields - array com os campos desejados para cada registro
     * @param {Object} options - objeto com parametros de busca
     * @returns {Array} - array de registros do tipo @see LincePartnerRepository
     */
    async index(fields = ['id', 'nome', 'CNPJ', 'parceiro_id'],
        where={}, limit=1000, offset=0) {

        let data = {
            raw: true,
            attributes: fields,
            limit: limit,
            where,
            offset: offset,
        }

        return await models.LincePartnerRepository.findAll(data)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire os dados de um registro de @see LincePartnerRepository no banco de dados
     * @param {Object} options - objeto com os dados para efetuar a busca @see LincePartnerRepository
     * @returns {Object} - um registro de @see LincePartnerRepository
     */
    async info(fields, options) {
        let data = {
            id: null,
            CNPJ: null,
            search: options.search,
            attributes: fields
        }

        let reg = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
        let isUuid = reg.exec(options.search)

        isUuid ? data.id = data.search : data.CNPJ = data.search

        return await models.LincePartnerRepository.findOne({
            raw: true,
            plain: true,
            where: {
                [Op.or]: [
                    { id: { [Op.eq]: data.id } },
                    { CNPJ: { [Op.eq]: data.CNPJ } }
                ]
            },
        })
            .then(result => {

                if (!result)
                    throw new Error('Parceiro não encontrado')
                else
                    return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere os dados fornecidos para um novo registro de @see LincePartnerRepository no banco de dados
     * @param {Object} options - objeto com os dados de um registro @see LincePartnerRepository
     * @returns {Object} - registro @see LincePartnerRepository que foi inserido
     */
    async insert(options) {
        let { payload } = options

        return await models.LincePartnerRepository.create({
            id: uuid(),
            nome: payload.nome,
            CNPJ: payload.CNPJ,
            parceiro_id: payload.parceiro_id
        })
            .then(result => {
                result = result.get({ plain: true })
                return result
            })
            .catch(error => {
                if (error.fields.parceiro_id !== '')
                    throw new Error(`Parceiro já cadastrado!`)
                throw error
            })
    }

    /**
     * Atualiza os dados de um registro @see LincePartnerRepository no banco de dados
     * @param {Object} options - objeto com os dados a atualizar do registro de @see LincePartnerRepository
     * @returns {Object} - objeto com os dados do registro de @see LincePartnerRepository atualizados
     */
    async update(options) {
        let { payload } = options

        return await models.LincePartnerRepository.update(
            payload,
            {
                where: { id: payload.id },
                returning: true,
                raw: true,
            })
            .then(result => {

                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar parceiro')
                return result[1][0]
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Exclui um registro de @see LincePartnerRepository no banco de dados
     * @param {Object} options - objeto com os dados para remoção @see LincePartnerRepository
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {

        return await models.LincePartnerRepository.destroy({
            plain: true, returning: true,
            where: { id: options.id }
        })
            .then(result => {

                if (result !== 1)
                    throw new Error('Erro ao deletar parceiro')

                return result
            })
            .catch(error => {
                throw error
            })
    }
}