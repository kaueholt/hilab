'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

/**
 * Classe responsável por operar as interações com as tabelas
 * de @see EquipmentModels e @see EquipmentPatrimonies na base de dados
 */
module.exports = class EquipmentRepository {
	/**
	 * Construtor da classe
	 * @param 
	 */
	constructor() { }

	/**
 * Adquire uma lista de registros de @see EquipmentModels no banco de dados
 * @param {Array} fields - array com os campos desejados para cada registro
 * @param {Object} options - objeto com parametros de busca
 * @returns {Array} - lista de registros de @see EquipmentModels
 */
	async indexModels(fields, options) {

		let data = {
			attributes: fields,
			limit: 10,
			offset: 0,
			query: options.query,
			nest: true,
			include: options.query.patrimonies === true && [{
				model: models.EquipmentPatrimonies, 
				as: 'equipment_patrimonies',
				attributes: {
					exclude: ['created_at', 'updated_at', 'EquipmentModelId', 'equipmentModelId']
				}
			}]
		}

		return await models.EquipmentModels.findAll(data)
			.then(result => {
				let res = result.map(function(sensor){ return sensor.get({ raw: true })})
				return JSON.parse(JSON.stringify(res))
			})
			.catch(error => {
				throw error
			})
	}

	/**
	* Consulta um registro de @see EquipmentModels na base de dados
	* @param {Array} fields - array com os campos desejados para cada registro
	* @param {Object} options - dados de @see EquipmentModels para efetuar a busca
	* @returns {Object} - registro de @see EquipmentModels
	*/
	async infoModel(fields, options) {
		let data = {
			query: options.query,
			search: options.search,
			raw: true,
			attributes: fields,
			plain: true,
			nest: true,
			where: { id: options.search },
			include: options.query.patrimonies === true && {
				model: models.EquipmentPatrimonies,
				attributes: { exclude: ['created_at', 'updated_at'] },
				where: { equipment_model_id: options.search }
			},
		}

		return await models.EquipmentModels.findOne(data)
			.then(result => {

				if (!result)
					throw new Error('Modelo de equipamento não encontrado')

				else
					return result
			})
			.catch(error => {
				throw error
			})
	}

	/**
	* Insere um registro de @see EquipmentModel na base de dados
	* @param {Object} options - dados para um novo registro de @see EquipmentModel
	* @returns {Object} - dados do registro de @see EquipmentModel que foi inserido
	*/
	async insertModel(options) {
		let { name, maker, description } = options.payload

		return await models.EquipmentModels.create({
			id: uuid(), name, maker, description
		})
			.then(result => result.get({ plain: true }))
			.catch(error => {

				if (error.parent.code === '22001')
					throw new Error(`Limite de caracteres excedido!`)

				if (error.fields.name !== '')
					throw new Error(`Nome de modelo já cadastrado!`)

				throw error
			})
	}

	/**
	* Atualiza o registro de @see EquipmentModels na base de dados
	* @param {Object} options - dados para atualização de um registro de @see EquipmentModels
	* @returns {Object} - registro de @see EquipmentModels com os dados atualizados
	*/
	async updateModel(options) {
		let { payload } = options

		return await models.EquipmentModels.update(
			payload,
			{
				where: { id: options.search },
				returning: true,
				raw: true,
			})
			.then(result => {

				if (result[0] !== 1)
					throw new Error('Erro ao atualizar modelo')

				return result[1][0]
			})
			.catch(error => {

				if (error.parent.code === '22001')
					throw new Error(`Limite de caracteres excedido!`)

				if (error.fields.name !== '')
					throw new Error(`Nome de modelo já cadastrado!`)

				throw error
			})
	}

	/**
	* Remove o registro de @see EquipmentModels na base de dados
	* @param {Object} options - dados de @see EquipmentModels para efetuar a remoção
	* @returns {int} - quantidade de registros removidos
	*/
	async deleteModel(options) {

		return await models.EquipmentModels.destroy({
			plain: true, returning: true,
			where: { id: options.id }
		})
			.then(result => {

				if (result !== 1)
					throw new Error('Erro ao deletar modelo')

				return result
			})
			.catch(error => {
				throw error
			})
	}

	/**
	 * Adquire uma lista de registros de @see EquipmentPatrimonies no banco de dados
	 * @param {Array} fields - array com os campos desejados para cada registro
	 * @param {Object} options - dados de @see EquipmentPatrimonies para efetuar a busca
	 * @returns {Array} - lista de registros de @see EquipmentPatrimonies
	 */
	async indexPatrimonies(fields, options) {

		let data = {
			search: options.search,
			raw: true,
			nest: true,
			query: options.query,
			attributes: { exclude: ['equipmentModelId', 'EquipmentModelId'] },
			include: options.query.model === true && {
				model: models.EquipmentModels,
				as: 'equipment_model',
				attributes: { exclude: ['created_at', 'updated_at'] },
			},
		}

		return await models.EquipmentPatrimonies.findAll(data)
			.then(result => {
				return result
			})
			.catch(error => {
				throw error
			})
	}

	/**
	* Consulta um registro de @see EquipmentPatrimonies na base de dados
	* @param {Array} fields - array com os campos desejados para cada registro
	* @param {Object} options - dados de @see EquipmentPatrimonies para efetuar a busca
	* @returns {Object} - registro de @see EquipmentPatrimonies
	*/
	async infoPatrimony(fields, options) {

		let data = {
			query: options.query,
			search: options.search,
			raw: true,
			attributes: fields,
			plain: true,
			nest: true,
			where: { id: options.search },
			include: options.query.model === true && {
				model: models.EquipmentModels,
				as: 'equipment_model',
				attributes: { exclude: ['created_at', 'updated_at'] }
			},
		}

		return await models.EquipmentPatrimonies.findOne(data)
			.then(result => {

				if (result === null)
					throw new Error('Patrimônio não encontrado!')

				return result
			})
			.catch(error => {
				throw error
			})
	}

	/**
	* Insere um registro de @see EquipmentPatrimonies na base de dados
	* @param {Object} options - dados para um novo registro de @see EquipmentPatrimonies
	* @returns {Object} - dados do registro de @see EquipmentPatrimonies que foi inserido
	*/
	async insertPatrimony(options) {
		let { mac, code, status, token, equipment_model_id } = options.payload

		return await models.EquipmentPatrimonies.create(
			{ id: uuid(), mac, code, status, token, equipment_model_id },
			{ plain: true, raw: true }
		)
			.then(result => {

				delete result.dataValues.EquipmentModelId &&
					delete result.dataValues.token
				return result.get({ plain: true })
			})
			.catch(error => {

				if (error.fields.code === code)
					throw new Error(`Código de patrimônio já cadastrado!`)

				if (error.fields.mac === mac)
					throw new Error(`Patrimônio com código MAC já cadastrado!`)

				throw error
			})
	}

	/**
	* Atualiza o registro de @see EquipmentPatrimonies na base de dados
	* @param {Object} options - dados para atualização de um registro de @see EquipmentPatrimonies
	* @returns {Object} - registro de @see EquipmentPatrimonies com os dados atualizados
	*/
	async updatePatrimony(options) {
		let { payload } = options

		return await models.EquipmentPatrimonies.update(
			payload,
			{
				where: { id: options.search },
				returning: true,
				raw: true,
			})
			.then(result => {

				if (result[0] !== 1)
					throw new Error('Erro ao atualizar patrimônio')

				return result[1][0]
			})
			.catch(error => {

				if (error.fields.code === payload.code)
					throw new Error(`Código de patrimônio já cadastrado!`)

				if (error.fields.mac === payload.mac)
					throw new Error(`Patrimônio com código MAC já cadastrado!`)

				throw error
			})
	}

	/**
	* Remove o registro de @see EquipmentPatrimonies na base de dados
	* @param {Object} options - dados de @see EquipmentPatrimonies para efetuar a remoção
	* @returns {int} - quantidade de registros removidos
	*/
	async deletePatrimony(options) {

		return await models.EquipmentPatrimonies.destroy({
			plain: true, returning: true,
			where: { id: options.id }
		})
			.then(result => {

				if (result !== 1)
					throw new Error('Erro ao deletar patrimônio')

				return result
			})
			.catch(error => {
				throw error
			})
	}
}