'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;


/**
 * Classe responsável por operar as interações com as tabelas de @see Circuits e @see Schedulings
 * na base de dados
 */
module.exports = class CircuitsRepository {

    /**
     * Construtor da classe
     */
    constructor() { }

    /**
     * Adquire uma lista de registros de @see Circuits no banco de dados
     * @param {Array} attrs - array com os campos desejados para cada registro
     * @param {String} options.company_id - timestamp da data de início para a filtragem
     * @param {String} options.date_start - timestamp da data de início para a filtragem
     * @param {String} options.date_finish - timestamp da data de término para a filtragem
     * @param {Boolean} expandCompany - boolean que indica se os dados da empresa devem vir juntos
     * @returns {Array} - lista de registros de @see Circuits
     */
    async index(attrs, options, expandCompany = false) {

        let where = {
            start: {
                [Op.between]: [options.date_start, options.date_finish]
            }
        }

        let searchById = {
            [Op.and]: { company_id: options.company_id }
        }

        if (options.company_id !== '')
            where = Object.assign({}, where, searchById)


        let circuitsOptions = {
            where,
            raw: true,
            nest: true,
            attributes: attrs,
            include: expandCompany === true && {
                model: models.Companies,
                as: 'company',
                attributes: ['id', 'name', 'cnpj'],
            }
        }

        return await models.Circuits.findAll(circuitsOptions)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Consulta um registro de @see Circuits na base de dados
    * @param {String} uuid - UUID de um registro de @see Circuits para efetuar a busca
    * @param {Array} attrs - array com os campos desejados para o registro
    * @param {Boolean} company - boolean para informar se deseja capturar os dados da empresa
    * @returns {Object} - registro de @see Circuits
    */
    async info(id, attr, company = false) {
        let circuitsOptions = {
            where: { id },
            raw: true,
            nest: true,
            attributes: attr,
            include: company === true && {
                model: models.Companies,
                as: 'company',
                attributes: ['id', 'name', 'cnpj'],
            },
        }


        return await models.Circuits.findOne(circuitsOptions)
            .then(async result => {
                if (!result)
                    throw new Error('Circuito não encontrado!')

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere registros de circuitos no banco de dados.
     * @param {Object} circuit - Dados do circuito @see Circuits
     * @param {String} circuit.id - Id do circuito, para apenas adquirir os dados do circuito
     * @param {String} circuit.name - Nome do circuito para inserção
     * @param {String} circuit.start - Data e hora do início do circuito
     * @param {String} circuit.finish - Data e hora do fim do circuito
     * @param {String} circuit.company_id - UUID da empresa o circuito
     */
    async insertCircuit(circuit) {
        let circuitFound = {}
        if (circuit.id) {
            circuitFound = await this.info(circuit.id, true)
                .then(result => result)
                .catch(error => {
                    throw new Error('Circuito não encontrado')
                })
        } else {
            circuitFound = await models.Circuits.create({
                id: uuid(),
                name: circuit.name,
                start: circuit.start,
                finish: circuit.finish,
                company_id: circuit.company_id
            }).then(insertedCircuit => insertedCircuit.get({ plain: true })).catch(error => {
                if (error.parent.code == '23503')
                    error = new Error('Empresa não encontrada!')

                throw error
            });
        }

        return circuitFound;
    }


    /**
     * Insere um conjunto de registros de agendamentos de equipametnos no banco de dados
     * @param {String} circuit_id - UUID do objeto do tipo @see Circuits
     * @param {Array} schedulingArray - array de objetos do tipo @see Schedulings
     */
    async insertSchedulings(circuit_id, schedulingArray) {
        let schedulings = []

        let promise = schedulingArray.map(async scheduling => {
            scheduling.circuit_id = circuit_id

            let inserted = await models.Schedulings.create({
                id: uuid(),
                start: scheduling.start,
                finish: scheduling.finish,
                equipment_id: scheduling.equipment_id,
                circuit_id: scheduling.circuit_id
            }).then(result => {
                result = result.get({ plain: true })
                delete result.CircuitId
                return result
            }).catch(error => {
                throw error
            })

            schedulings.push(inserted);
        })

        await Promise.all(promise)
        return schedulings
    }


    /**
     * Adquire uma lista de registros de @see Scheduling no banco de dados
     * @param {Array} circuit_id - UUID do circuito
     * @returns {Array} - lista de registros de @see Scheduling
     */
    async indexSchedulings(circuit_id) {
        return await models.Schedulings.findAll(
            {
                raw: true,
                nest: true,
                attributes: [
                    'id', 'start', 'finish'
                ],
                where: { circuit_id: circuit_id },
                order: [['start', 'ASC']],
                include: {
                    model: models.EquipmentPatrimonies,
                    as: 'equipment_patrimony',
                    attributes: ['id', 'mac', 'code', 'status'],
                    nest: true,
                    include: {
                        model: models.EquipmentModels,
                        as: 'equipment_model',
                        attributes: ['name', 'maker']
                    },
                }
            }
        )
            .then(schedulings => {
                let result = schedulings.map(function (item) {
                    item.equipment_id = item.equipment_patrimony.id
                    item.mac = item.equipment_patrimony.mac
                    item.code = item.equipment_patrimony.code
                    item.status = item.equipment_patrimony.status
                    item.model = item.equipment_patrimony.equipment_model.name
                    item.maker = item.equipment_patrimony.equipment_model.maker

                    delete item.equipment_patrimony

                    return item
                })

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Atualiza um registro de @see Circuits na base de dados
    * @param {Object} circuit - dados para atualização de um registro de @see Circuits
    * @param {String} circuit.id - UUID do circuito que será atualizado
    * @param {String} circuit.name - nome do circuito
    * @param {String} circuit.company_id - UUID da empresa do circuito
    * @returns {Object} - registro de @see Circuits com os dados atualizados
    */
    async update(circuit) {
        const { company_id, name } = circuit

        return await models.Circuits.update(
            { company_id, name },
            {
                where: { id: circuit.id }, returning: true, raw: true, plain: true
            })
            .then(result => {
                return result[1]
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Remove um registro de @see Circuits na base de dados
    * @param {String} id - ID do registro @see Circuits para efetuar a busca
    * @returns {int} - quantidade de registros removidos
    */
    async deleteCircuit(id) {
        return await models.Circuits.destroy({ where: { id } })
            .then(result => {
                if (result !== 1) {
                    throw new Error('Erro ao deletar Circuito')
                }

				return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Remove um registro de @see Schedulings na base de dados
    * @param {String} id - ID do registro @see Schedulings para efetuar a busca
    * @returns {int} - quantidade de registros removidos
    */
    async deleteScheduling(id) {
        return await models.Schedulings.destroy({ where: { id } })
            .then(result => result)
            .catch(error => {
                throw Error("Agendamento não encontrado")
            })
    }
}