'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

/**
 * Classe responsável por operar as interações com a tabela de @see Exams e @see ExamClass
 * na base de dados
 */
module.exports = class ExamsRepository {

    /**
     * Construtor da classe
     */
    constructor() { }

    /**
     * Adquire uma lista de registros de @see Exams no banco de dados
     * @param {Array} attrs - array com os campos desejados para cada registro
     * @param {String} options.circuit_id - id do circuito
     * @param {String} options.worker_id - id do trabalhador
     * @param {String} options.date_start - timestamp da data de início para a filtragem
     * @param {String} options.date_finish - timestamp da data de término para a filtragem
     * @param {Boolean} expandWorker - boolean para informar se deseja obter os dados dos colaboradores
     * @returns {Array} - lista de registros de @see Circuits
     */
    async index(attrs, options, expandWorker = false) {

        let where = {}

        let date_filter = {
            start: {
                [Op.between]: [options.date_start, options.date_finish]
            }
        }

        let searchByCircuit = {
            [Op.and]: { circuit_id: options.circuit_id }
        }

        let searchByWorker = {
            [Op.and]: { worker_id: options.worker_id }
        }

        if (options.circuit_id !== '')
            where = Object.assign({}, where, searchByCircuit)

        if (options.worker_id !== '')
            where = Object.assign({}, where, searchByWorker)

        if (options.date_start !== '' && options.date_finish !== '')
            where = Object.assign({}, where, date_filter)


        let searchOptions = {
            where,
            raw: true,
            nest: true,
            attributes: attrs,
            include: [{
                model: models.Circuits,
                as: 'circuit',
                attributes: ['id', 'name', 'start', 'finish'],
            }],
        }

        if (expandWorker === true) {
            searchOptions.include.push(
                {
                    model: models.Workers,
                    as: 'worker',
                    attributes: ['id', 'name', 'cpf', 'email', 'gender', 'phone', 'pis_nit', 'date_birth'],
                }
            )
        }

        return await models.Exams.findAll(searchOptions)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Consulta um registro de @see Exams na base de dados
    * @param {String} uuid - UUID de um registro de @see Exams para efetuar a busca
    * @param {Array} attrs - array com os campos desejados para o registro
    * @param {Boolean} expandWorker - boolean para informar se deseja obter os dados do colaborador
    * @returns {Object} - registro de @see Exams
    */
    async info(uuid, attrs, expandWorker = false) {
        let options = {
            where: { id: uuid },
            raw: true,
            nest: true,
            attributes: attrs,
            include: [
                {
                    model: models.Circuits,
                    as: 'circuit',
                    attributes: ['id', 'name', 'start', 'finish'],
                },
            ]
        }

        if (expandWorker === true) {
            options.include.push(
                {
                    model: models.Workers,
                    as: 'worker',
                    attributes: ['id', 'name', 'cpf', 'email', 'gender', 'phone', 'pis_nit', 'date_birth'],
                }
            )
        }


        return await models.Exams.findOne(options)
            .then(async result => {
                if (!result)
                    throw new Error('Exame não encontrado!')

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere registros de @see Exams no banco de dados.
     * @param {Object} register - Dados do novo registro
     * @param {String} register.start - Data e hora do início
     * @param {String} register.finish - Data e hora do fim
     * @param {String} register.circuit_id - UUID do circuito
     * @param {String} register.worker_id - UUID do trabalhador
     */
    async insert(register) {
        return await models.Exams.create({
            id: uuid(),
            start: register.start,
            finish: register.finish,
            circuit_id: register.circuit_id,
            worker_id: register.worker_id
        }).then(result => result.get({ plain: true })).catch(error => {
            if (error.parent.code == '23503')
                error = new Error('Empresa ou Trabalhador não encontrado!')

            throw error
        });
    }

    /**
    * Remove um registro de @see Exams na base de dados
    * @param {String} uuid - UUID do registro @see Exams para efetuar a busca
    * @returns {int} - quantidade de registros removidos
    */
    async delete(uuid) {
        return await models.Exams.destroy({ where: { id: uuid } })
            .then(result => {
                if (result !== 1)
                    throw new Error('Erro ao deletar exame')

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire uma lista de registros de @see ExamClass do banco de dados
     * @param {Array} attrs - array com os campos desejados para cada registro
     * @returns {Array} - lista de registros
     */
    async indexExamClass(attrs) {

        let where = {}

        let searchOptions = {
            where,
            raw: true,
            nest: true,
            attributes: attrs
        }

        return await models.ExamClass.findAll(searchOptions)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Consulta um registro de @see ExamClass na base de dados
    * @param {String} options - UUID de um registro de @see ExamClass para efetuar a busca
    * @param {Array} attrs - array com os campos desejados para o registro
    * @returns {Object} - registro de @see ExamClass
    */
    async infoExamClass(options, attrs) {
        let data = {
            where: { id: options.query.id },
            raw: true,
            nest: true,
            attributes: attrs,
        }
        return await models.ExamClass.findOne(data)
            .then(async result => {
                if (!result)
                    throw new Error('Tipo de exame não encontrado!')
                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere registros de @see ExamClass no banco de dados.
     * @param {Object} register - Dados do novo registro
     * @param {String} register.name - Nome da classe de exame
     */
    async insertExamClass(register) {
        return await models.ExamClass.create({
            id: uuid(),
            name: register.name
        })
            .then(result => result.get({ plain: true }))
            .catch(error => {
                if (error.fields.description !== '')
                    throw new Error('Nome de tipo de exame já existe!')
                throw error
            });
    }

    /**
    * Atualiza o registro de @see ExamClass na base de dados
    * @param {String} data.id - UUID do registro que será atualizado
    * @param {String} data.name - Nome da classe de exame
    */
    async updateExamClass(data) {

        return await models.ExamClass.update(
            { name: data.name },
            {
                where: { id: data.id },
                returning: true,
                raw: true,
            })
            .then(result => {
                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar classe de exame')

                return result[1][0]
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Remove um registro de @see ExamClass na base de dados
    * @param {String} uuid - UUID do registro @see ExamClass para efetuar a busca
    * @returns {int} - quantidade de registros removidos
    */
    async deleteExamClass(uuid) {
        return await models.ExamClass.destroy({ where: { id: uuid } })
            .then(result => {
                if (result !== 1)
                    throw new Error('Erro ao deletar classe de exame')

                return result
            })
            .catch(error => {
                throw error
            })
    }
}
