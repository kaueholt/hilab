'use strict'

const models = require('../models');
const uuid = require('uuid/v4')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

/**
 * Classe responsável por operar as interações com a tabela de @see LinceAnswerTypes na base de dados
 */
module.exports = class LinceAnswerTypeRepository {

    /**
     * Construtor da classe
     * @param 
     */
    constructor() { }

    /**
     * Adquire uma lista de @see LinceAnswerTypes no banco de dados
     * @param {Array} fields - array com os campos desejados para cada registro
     * @param {Object} options - objeto com parametros de busca
     * @returns {Array} - array de registros do tipo @see LinceAnswerTypes
     */
    async index(fields = ['id', 'tipo'],
        where={}, limit=1000, offset=0) {

        let data = {
            raw: true,
            attributes: fields,
            limit: limit,
            where,
            offset: offset,
        }

        return await models.LinceAnswerTypes.findAll(data)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire os dados de um registro de @see LinceAnswerTypes no banco de dados
     * @param {Object} options - objeto com os dados para efetuar a busca @see LinceAnswerTypes
     * @returns {Object} - um registro de @see LinceAnswerTypes
     */
    async info(fields, options) {
        let data = {
            id: null,
            search: options.search,
            attributes: fields
        }

        let reg = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
        let isUuid = reg.exec(options.search)

        isUuid ? data.id = data.search : null

        return await models.LinceAnswerTypes.findOne({
            raw: true,
            plain: true,
            where: {
                [Op.or]: [
                    { id: { [Op.eq]: data.id } }
                ]
            },
        })
            .then(result => {

                if (!result)
                    throw new Error('Tipo de resposta não encontrado')
                else
                    return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere os dados fornecidos para um novo registro de @see LinceAnswerTypes no banco de dados
     * @param {Object} options - objeto com os dados de um registro @see LinceAnswerTypes
     * @returns {Object} - registro @see LinceAnswerTypes que foi inserido
     */
    async insert(options) {
        let { payload } = options

        return await models.LinceAnswerTypes.create({
            id: uuid(),
            tipo: payload.tipo,
        })
            .then(result => {
                result = result.get({ plain: true })
                return result
            })
            .catch(error => {
                if (error.fields.parceiro_id !== '')
                    throw new Error(`Tipo de resposta já cadastrado!`)
                throw error
            })
    }

    /**
     * Atualiza os dados de um registro @see LinceAnswerTypes no banco de dados
     * @param {Object} options - objeto com os dados a atualizar do registro de @see LinceAnswerTypes
     * @returns {Object} - objeto com os dados do registro de @see LinceAnswerTypes atualizados
     */
    async update(options) {
        let { payload } = options

        return await models.LinceAnswerTypes.update(
            payload,
            {
                where: { id: payload.id },
                returning: true,
                raw: true,
            })
            .then(result => {

                if (result[0] !== 1)
                    throw new Error('Erro ao atualizar tipo de resposta')
                return result[1][0]
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Exclui um registro de @see LinceAnswerTypes no banco de dados
     * @param {Object} options - objeto com os dados para remoção @see LinceAnswerTypes
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {

        return await models.LinceAnswerTypes.destroy({
            plain: true, returning: true,
            where: { id: options.id }
        })
            .then(result => {
                if (result !== 1)
                    throw new Error('Erro ao deletar tipo de resposta')
                return result
            })
            .catch(error => {
                throw error
            })
    }
}