'use strict'

/**
 * Classe responsável por operar todos os serviços relacionados
 * à colaboradores
 */
module.exports = class WorkerService {

    /**
     * Construtor da classe
     * @param {WorkerRepository} workerRepository
     * @param {Uploader} uploader 
     */
    constructor(workerRepository, uploader) {
        this.workerRepository = workerRepository
        this.uploader = uploader
    }

    /**
      * Adquire uma lista de colaboradores no banco de dados
      * @param {Object} options - Objeto com dados da requisição
      * @returns {Array} - lista de colaboradores encontrados no banco
      */
    async index(options) {
        let fields = [
            'id', 'company_id', 'name', 'cpf',
            'email', 'gender', 'race', 'pis_nit',
            'date_birth', 'phone', 'avatar',
            'status', 'role', 'rfid', 'observation'
        ]

        return await this.workerRepository.index(fields, options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Consulta os dados de um colaborador
     * @param {Object} options - Objeto com dados da requisição
     * @param {String} search - CPF ou ID do colaborador para a consulta, informado nos parâmetros
     * @returns {Object} - dados do colaborador encontrado
     */
    async info(options) {
        let fields = [
            'id', 'cpf', 'email', 'gender',
            'race', 'status', 'avatar', 'phone',
            'pis_nit', 'company_id', 'rfid',
            'observation', 'role', 'date_birth'
        ]

        return await this.workerRepository.info(fields, options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere o registro de um novo colaborador no banco de dados
     * @param {Object} options - objeto com os dados do colaborador
     * @param {String} options.name - nome do colaborador
     * @param {String} options.cpf - CPF do colaborador
     * @param {String} options.email - email do colaborador
     * @param {String} options.gender - gênero do colaborador
     * @param {String} options.race - raça/cor do colaborador
     * @param {String} options.pis_nit - pis/nit do colaborador
     * @param {String} options.date_birth - data de nascimento yyyy-mm-dd
     * @param {String} options.phone - telefone do colaborador
     * @param {String} options.company_id - id da empresa que colaborador atua
     * @param {Int} options.status - status do colaborador
     * @returns {Object} - dados do colaborador que foi inserido no banco
     */
    async insert(options) {

        const { payload } = options

        let fileObjetToUpload = payload.avatar || ''

        let grabCpf = payload.cpf.substr(payload.cpf.length - 3)

        let arrExtensions = ['png', 'jpg', 'jpeg']
        if (fileObjetToUpload != '' && fileObjetToUpload.hapi.filename != '') {
            payload.avatar = new Date().getTime() + "_" + grabCpf

            if (!this.uploader.setFileToUpload(fileObjetToUpload, payload.avatar, arrExtensions))
                throw new Error("Extensão inválida. Tipos de arquivos permitidos: " + arrExtensions)

            payload.avatar = this.uploader.getFileFormated()
        } else {
            payload.avatar = 'thumb.png'
        }

        return await this.workerRepository.insert(options)
            .then(async result => {
                if (fileObjetToUpload != '' && fileObjetToUpload.hapi.filename != '')
                    await this.uploader.uploadFile()

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Atualiza o registro de um colaborador no banco de dados
     * @param {Object} options - objeto com os dados do colaborador
     * @param {String} options.name - nome do colaborador
     * @param {String} options.cpf - CPF do colaborador
     * @param {String} options.email - email do colaborador
     * @param {String} options.gender - gênero do colaborador
     * @param {String} options.race - raça/cor do colaborador
     * @param {String} options.pis_nit - pis/nit do colaborador
     * @param {String} options.date_birth - data de nascimento yyyy-mm-dd
     * @param {String} options.phone - telefone do colaborador
     * @param {String} options.company_id - id da empresa que colaborador atua
     * @param {Int} options.status - status do colaborador
     * @returns {Object} - dados do colaborador que foi inserido no banco
     */
    async update(options) {

        let workerResult = await this.info(options)
            .then(async result => {

                let workerResult = {
                    name: result.name,
                    gender: result.gender,
                    race: result.race,
                    pis_nit: result.pis_nit,
                    email: result.email,
                    phone: result.phone,
                    date_birth: result.date_birth,
                    status: result.status,
                    avatar: result.avatar,
                    company_id: result.company_id,
                    rfid: result.rfid,
                    observation: result.observation,
                    role: result.role,
                }
                return workerResult
            })
            .catch(error => {
                throw error
            })

        let fileObjetToUpload = options.payload.avatar || ''

        let grabCpf = options.search.substr(options.search.length - 3)

        let arrExtensions = ['png', 'jpg', 'jpeg']
        if (fileObjetToUpload != '' && fileObjetToUpload.hapi.filename != '') {
            options.payload.avatar = new Date().getTime() + "_" + grabCpf

            if (!this.uploader.setFileToUpload(fileObjetToUpload, options.payload.avatar, arrExtensions))
                throw new Error("Extensão inválida. Tipos de arquivos permitidos: " + arrExtensions)

            options.payload.avatar = this.uploader.getFileFormated()
        } else {
            delete options.payload.avatar
        }

        options.payload = {
            name: options.payload.name ? options.payload.name : workerResult.name,
            gender: options.payload.gender ? options.payload.gender : workerResult.gender,
            race: options.payload.race ? options.payload.race : workerResult.race,
            pis_nit: workerResult.pis_nit,
            email: options.payload.email ? options.payload.email : workerResult.email,
            phone: options.payload.phone ? options.payload.phone.replace(/[^\w\s]/gi, '') : workerResult.phone,
            date_birth: options.payload.date_birth ? options.payload.date_birth : workerResult.date_birth,
            status: options.payload.status ? options.payload.status : workerResult.status,
            avatar: options.payload.avatar ? options.payload.avatar : workerResult.avatar,
            company_id: options.payload.company_id ? options.payload.company_id : workerResult.company_id,
            rfid: options.payload.rfid ? options.payload.rfid : workerResult.rfid,
            observation: options.payload.observation ? options.payload.observation : workerResult.observation,
            role: options.payload.role ? options.payload.role : workerResult.role,
        }

        let workerUpdated = await this.workerRepository.update(options)
            .then(async result => result)
            .catch(error => {
                throw error
            })

        if (fileObjetToUpload != '' && fileObjetToUpload.hapi.filename != '') {
            if (workerResult.avatar != 'thumb.png')
                await this.uploader.deleteFile(workerResult.avatar)
            await this.uploader.uploadFile()
        }

        return workerUpdated
    }

    /**
     * Remove um registro de colaborador do banco de dados
     * @param {Object} options - objeto com dados da requisição
     * @param {String} options.cpf - CPF do colaborador que será removido
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {
        let data = { search: options.search }

        return this.info(options)
            .then(async result => {
                return await this.workerRepository.delete(data)
                    .then(async resultDelete => {

                        result.avatar !== 'thumb.png' && await this.uploader.deleteFile(result.avatar)
                        return resultDelete
                    })
                    .catch(error => {
                        throw error
                    })
            })
            .catch(error => {
                throw error
            })
    }

}
