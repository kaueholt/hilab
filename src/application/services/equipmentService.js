'use strict'

/**
 * Classe responsável por operar todos os serviços relacionados
 * aos modelos de equipamentos e patrimônios
 */
module.exports = class EquipmentService {

	/**
	 * Construtor da classe
	 * @param {EquipmentRepository} equipmentRepository 
	 */
	constructor(equipmentRepository) {
		this.equipmentRepository = equipmentRepository
	}

	/**
	  * Adquire uma lista de modelos de equipamentos
	  * @param {Object} options - parâmetros para filtrar a busca
	  * @returns {Array} - array de registros de modelos de equipamentos
	  */
	async indexModels(options) {
		let fields = ['id', 'name', 'maker', 'description']

		return await this.equipmentRepository.indexModels(fields, options)
			.then(result => result)
			.catch(error => {
				throw error
			})
	}

	/**
	* Insere um modelo de equipamento na base de dados
	* @param {Object} options - objeto com os dados do modelo do equipamento
	* @param {String} options.name - nome do modelo do equipamento
	* @param {String} options.maker - nome do fabricante do modelo do equipamento
	* @param {String} options.description - descrição do modelo do equipamento
	* @returns {Object} - dados do modelo de equipamento que foi inderido no banco
	*/
	async insertModel(options) {

		return await this.equipmentRepository.insertModel(options)
			.then(result => result)
			.catch(error => {
				throw error
			})
	}

	/**
	* Consulta os dados de um modelo de equipamento na base de dados
	* @param {String} id - ID do modelo do equipamento para efetuar a busca
	* @returns {Object} - dados do modelo do equipamento
	*/
	async infoModel(options) {
		let fields = ['id', 'name', 'maker', 'description']

		return await this.equipmentRepository.infoModel(fields, options)
			.then(result => result)
			.catch(error => {
				throw error
			})
	}

	/**
	* Remove o registro de um modelo de equipamento na base de dados
	* @param {Object} options - objeto com os dados do modelo do equipamento
	* @param {String} options.id - ID modelo
	* @returns {int} - quantidade de registros removidos
	*/
	async deleteModel(options) {
		let where = { id: options.params.id }

		return await this.equipmentRepository.deleteModel(where)
			.then(result => result)
			.catch(error => {
				throw error
			})
	}

	/**
	* Atualiza o registro de um modelo de equipamento na base de dados
	* @param {Object} options - objeto com os dados do modelo de equipamento
	* @param {String} options.id - ID do modelo
	* @param {String} options.name - nome do modelo do equipamento
	* @param {String} options.maker - fabricante do modelo do equipamento
	* @param {String} options.description - descrição do modelo do equipamento
	* @returns {Object} - modelo do equipamento com os dados atualizados
	*/
	async updateModel(options) {
		options.query = { patrimonies: false }

		let resultModel = await this.infoModel(options)
			.then(async result => result)
			.catch(error => {
				throw error
			})

		const { payload } = options

		options.payload = {
			name: payload.name ? payload.name : resultModel.name,
			maker: payload.maker ? payload.maker : resultModel.maker,
			description: payload.description == '' || payload.description != '' ?
				payload.description : resultModel.description,
		}

		return await this.equipmentRepository.updateModel(options)
			.then(async result => result)
			.catch(error => {
				throw error
			})
	}

	/**
	* Adquire uma lista de patrimônios de equipamentos
	* @param {Object} options - parâmetros para filtrar a busca
	* @returns {Array} - array de registros de patrimônios
	*/
	async indexPatrimonies(options) {
		let fields = ['id', 'mac', 'status', 'code', 'equipment_model_id']

		return await this.equipmentRepository.indexPatrimonies(fields, options)
			.then(result => result)
			.catch(error => {
				throw error
			})
	}

	/**
	* Consulta os dados de um patrimônio de equipamento na base de dados
	* @param {String} id - ID do patrimônio do equipamento para efetuar a busca
	* @returns {Object} - dados do patrimônio do equipamento
	*/
	async infoPatrimony(options) {
		let fields = ['id', 'status', 'code', 'mac', 'equipment_model_id']

		return await this.equipmentRepository.infoPatrimony(fields, options)
			.then(result => result)
			.catch(error => {
				throw error
			})
	}

	/**
	* Insere um patrimônio de equipamento na base de dados
	* @param {Object} options - objeto com os dados do patrimônio do equipamento
	* @param {String} options.equipment_model_id - ID do modelo de equipamento referenciado do patrimônio
	* @param {String} options.mac - MAC do patrimônio do equipamento
	* @param {String} options.status - status do patrimônio do equipamento
	* @returns {Object} - dados do modelo de equipamento que foi inderido no banco
	*/
	async insertPatrimony(options) {

		return await this.equipmentRepository.insertPatrimony(options)
			.then(result => result)
			.catch(error => {
				throw error
			})
	}

	/**
	* Remove o registro de um patrimônio de equipamento na base de dados
	* @param {Object} options - objeto com os dados do patrimônio do equipamento
	* @param {String} options.id - ID patrimônio
	* @returns {int} - quantidade de registros removidos
	*/
	async deletePatrimony(options) {
		let where = { id: options.params.id }

		return await this.equipmentRepository.deletePatrimony(where)
			.then(result => result)
			.catch(error => {
				throw error
			})
	}

	/**
	* Atuqliza um patrimônio de equipamento na base de dados
	* @param {Object} options - objeto com os dados do patrimônio do equipamento
	* @param {String} options.equipment_model_id - ID do modelo de equipamento referenciado do patrimônio
	* @param {String} options.mac - MAC do patrimônio do equipamento
	* @param {String} options.status - status do patrimônio do equipamento
	* @returns {Object} - dados do modelo de equipamento que foi atualizado no banco
	*/
	async updatePatrimony(options) {
		options.query = { models: false }

		let resultPatrimony = await this.infoPatrimony(options)
			.then(async result => result)
			.catch(error => {
				throw error
			})

		const { payload } = options

		options.payload = {
			equipment_model_id: payload.equipment_model_id ?
				payload.equipment_model_id : resultPatrimony.equipment_model_id,
			mac: payload.mac ? payload.mac.replace(/[^\w\s]/gi, '') : resultPatrimony.mac,
			code: payload.code == '' || payload.code != '' ?
				payload.code.replace(/[^\w\s]/gi, '') : resultPatrimony.code,
			status: payload.status == 0 || payload.status == 1 ? payload.status : resultPatrimony.status,
		}

		return await this.equipmentRepository.updatePatrimony(options)
			.then(async result => result)
			.catch(error => {
				throw error
			})
	}

}
