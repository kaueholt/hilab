'use strict'

/**
 * Classe responsável por operar todos os serviços relacionados
 * aos formulários
 */
module.exports = class FormService {

  /**
   * Construtor da classe
   * @param {FormRepository} formRepository
   */
  constructor(formRepository) {
    this.formRepository = formRepository
  }

  /**
   * Insere o registro do tipo formularies no banco de dados
   * @param {Object} options.headers - objeto com os headers recebidos na requisição
   * @param {Object} options.payload - objeto com o payload recebido na requisição
   * @returns {Object} - dados do registro que foi inserido no banco
   */
  async insert(options) {
    return await this.formRepository.insert(options)
      .then(async insertedResult => insertedResult)
      .catch(error => {
        throw error
      })
  }

  /**
  * Consulta os dados de um formulário
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params de busca
  * @returns {Object} - dados do exame encontrado
  */
  async info(options) {
    let attrs = [
      'id', 'exam_class_id', 'equipment_model_id', 'status',
      'name', 'description', 'created_at', 'updated_at'
    ]
    return await this.formRepository.info(options, attrs)
      .then(resultInfo => resultInfo)
      .catch(error => {
        throw error
      })
  }

  /**
  * Deleta um registro do tipo formularies do banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params de busca
  * @returns {Int} - quantidade de registros excluídos
  */
  async delete(options) {
    return await this.formRepository.delete(options)
      .then(deletedResult => deletedResult)
      .catch(error => {
        throw error
      })
  }

  /**
  * Atualiza o registro de um formulário no banco de dados
  * @param {Object} data.headers - objeto com os headers recebidos na requisição
  * @param {Object} data.query - objeto com params de busca recebidos na requisição
  * @param {Object} data.payload - objeto com o payload recebido na requisição
  * @returns {Object} - dados do registro que foi atualizado no banco
  */
  async update(options) {
    return await this.info(options)
      .then(async formResultInfo => {
        options.payload = {
          id: formResultInfo.id,
          exam_class_id: options.payload.exam_class_id || formResultInfo.exam_class_id,
          equipment_model_id:
            options.payload.equipment_model_id == null
              || options.payload.equipment_model_id !== ''
              ? options.payload.equipment_model_id
              : formResultInfo.equipment_model_id,
          status: options.payload.status == 0 || options.payload.status == 1 ? options.payload.status : formResultInfo.status,
          name: options.payload.name || formResultInfo.name,
          description: options.payload.description == '' || options.payload.description != ''
            ? options.payload.description
            : questionResultInfo.description
        }
        return await this.formRepository.update(options)
          .then(updatedResult => updatedResult)
          .catch(error => {
            throw error
          })
      }).catch(error => {
        throw error
      })
  }

  /**
   * Adquire uma lista de formulários no banco de dados
   * @param {Object} options - Objeto com dados da requisição
   * @param {Object} options.headers - Objeto com os headers
   * @param {Object} options.query - Objeto com os params de busca
   * @returns {Array} - lista de registros encontrados no banco
   */
  async index(options) {
    let attrs = [
      'id', 'name', 'status', 'description', 'exam_class_id',
      'equipment_model_id', 'created_at', 'updated_at'
    ]

    return await this.formRepository.index(options, attrs)
      .then(resultForms => resultForms)
      .catch(error => {
        throw error
      })
  }

  /**
  * Insere um registro do tipo topic_forms no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.payload - objeto com o payload recebido na requisição
  * @returns {Array} - lista de registros encontrados no banco
  */
  async insertTopic(options) {
    return await this.formRepository.insertTopic(options)
      .then(insertedResult => insertedResult)
      .catch(error => {
        throw error
      })
  }

  /**
  * Consulta os dados de um tópico
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params
  * @returns {Object} - dados do exame encontrado
  */
  async infoTopic(options) {
    let attrs = [
      'id', 'formulary_id', 'status', 'sequence', 'description',
      'created_at', 'updated_at'
    ]

    return await this.formRepository.infoTopic(options, attrs)
      .then(result => result)
      .catch(error => {
        throw error
      })
  }

  /**
  * Atualiza um registro do tipo topic_forms no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params
  * @returns {Object} - dados do exame atualizado
  */
  async updateTopic(options) {
    return await this.infoTopic(options)
      .then(async topicResultInfo => {
        options.payload = {
          id: topicResultInfo.id,
          status: options.payload.status == 0 || options.payload.status == 1
            ? options.payload.status
            : topicResultInfo.status,
          description: options.payload.description || topicResultInfo.description,
          sequence: options.payload.sequence || topicResultInfo.sequence
        }
        return await this.formRepository.updateTopic(options)
          .then(updatedResult => updatedResult)
          .catch(error => {
            throw error
          })
      }).catch(error => {
        throw error
      })
  }

  /**
  * Deleta um registro do tipo topic_forms no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params
  * @returns {Int} - quantidade de registros deletados no banco
  */
  async deleteTopic(options) {
    return await this.formRepository.deleteTopic(options)
      .then(deletedResult => deletedResult)
      .catch(error => {
        throw error
      })
  }

  /**
  * Adquire uma lista de tópicos no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params
  * @returns {Array} - lista de registros encontrados no banco
  */
  async indexTopics(options) {
    let attrs = [
      'id', 'description', 'status', 'sequence', 'status',
      'formulary_id', 'created_at', 'updated_at'
    ]

    return await this.formRepository.indexTopics(options, attrs)
      .then(resultTopics => resultTopics)
      .catch(error => {
        throw error
      })
  }

  /**
  * Insere um registro do tipo questions no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.payload - Objeto com os dados para inserção
  * @returns {Object} - dados da questçao inserida
  */
  async insertQuestion(options) {
    return await this.formRepository.insertQuestion(options)
      .then(insertedQuestion => insertedQuestion)
      .catch(error => {
        throw error
      })
  }

  /**
  * Consulta um registro do tipo questions no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params
  * @returns {Object} - dados do campo encontrado
  */
  async infoQuestion(options) {
    let attrs = [
      'id', 'formulary_id', 'topic_form_id', 'status',
      'description', 'type', 'complement', 'created_at', 'updated_at'
    ]

    return await this.formRepository.infoQuestion(options, attrs)
      .then(resultInfoQuestion => resultInfoQuestion)
      .catch(error => {
        throw error
      })
  }

  /**
  * Adquire uma lista de questões no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params
  * @returns {Array} - lista de registros encontrados no banco
  */
  async indexQuestions(options) {
    let attrs = [
      'id', 'description', 'status', 'sequence', 'type',
      'formulary_id', 'topic_form_id', 'complement',
      'created_at', 'updated_at'
    ]

    return await this.formRepository.indexQuestions(attrs, options)
      .then(resultQuestions => resultQuestions)
      .catch(error => {
        throw error
      })
  }

  /**
  * Atualiza um registro do tipo questions no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params
  * @returns {Object} - dados do campo atualizado
  */
  async updateQuestion(options) {
    return await this.infoQuestion(options)
      .then(async questionResultInfo => {
        options.payload = {
          id: questionResultInfo.id,
          status: options.payload.status == 0 || options.payload.status == 1
            ? options.payload.status
            : questionResultInfo.status,
          description: options.payload.description || questionResultInfo.description,
          sequence: options.payload.sequence || questionResultInfo.sequence,
          type: options.payload.type || questionResultInfo.type,
          complement: options.payload.complement == '' || options.payload.complement != ''
            ? options.payload.complement
            : questionResultInfo.complement
        }
        return await this.formRepository.updateQuestion(options)
          .then(updatedResult => updatedResult)
          .catch(error => {
            throw error
          })
      }).catch(error => {
        throw error
      })
  }

  /**
  * Deleta um registro do tipo questions no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params de busca
  * @returns {Int} - quantidade de registros deletados no banco
  */
  async deleteQuestion(options) {
    return await this.formRepository.deleteQuestion(options)
      .then(deletedResult => deletedResult)
      .catch(error => {
        throw error
      })
  }

  /**
  * Insere um registro do tipo question_items no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.payload - Objeto com payload inserção
  * @returns {Object} - dados do exame inserido
  */
  async insertQuestionField(options) {
    return await this.formRepository.insertQuestionField(options)
      .then(insertedItem => insertedItem)
      .catch(error => {
        throw error
      })
  }

  /**
   * Consulta um registro do tipo question_items no banco de dados
   * @param {Object} options - Objeto com dados da requisição
   * @param {Object} options.headers - Objeto com os headers
   * @param {Object} options.query - Objeto com os params
   * @returns {Object} - dados do item encontrado
   */
  async infoQuestionField(options) {
    let attrs = [
      'id', 'formulary_id', 'topic_form_id', 'question_id', 'status',
      'description', 'value', 'complement', 'created_at', 'updated_at'
    ]

    return await this.formRepository.infoQuestionField(options, attrs)
      .then(result => result)
      .catch(error => {
        throw error
      })
  }

  /**
  * Adquire uma lista de question_items no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params
  * @returns {Array} - lista de registros encontrados no banco
  */
  async indexQuestionFields(options) {
    let attrs = [
      'id', 'description', 'status', 'sequence', 'value',
      'question_id', 'complement', 'formulary_id',
      'topic_form_id', 'created_at', 'updated_at'
    ]

    return await this.formRepository.indexQuestionFields(attrs, options)
      .then(resultItems => resultItems)
      .catch(error => {
        throw error
      })
  }

  /**
  * Atualiza um registro do tipo question_items no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params
  * @returns {Object} - dados do item atualizado
  */
  async updateQuestionField(options) {
    return await this.infoQuestionField(options)
      .then(async questionFieldResultInfo => {
        options.payload = {
          id: questionFieldResultInfo.id,
          status: options.payload.status == 0 || options.payload.status == 1
            ? options.payload.status
            : questionFieldResultInfo.status,
          description: options.payload.description || questionFieldResultInfo.description,
          sequence: options.payload.sequence || questionFieldResultInfo.sequence,
          value: options.payload.value || questionFieldResultInfo.value,
          complement: options.payload.complement == '' || options.payload.complement != ''
            ? options.payload.complement
            : questionResultInfo.complement
        }
        return await this.formRepository.updateQuestionField(options)
          .then(updatedResult => updatedResult)
          .catch(error => {
            throw error
          })
      }).catch(error => {
        throw error
      })
  }

  /**
  * Deleta um registro do tipo question_items no banco de dados
  * @param {Object} options - Objeto com dados da requisição
  * @param {Object} options.headers - Objeto com os headers
  * @param {Object} options.query - Objeto com os params
  * @returns {Array} - lista de registros encontrados no banco
  */
  async deleteQuestionField(options) {
    return await this.formRepository.deleteQuestionField(options)
      .then(deletedResult => deletedResult)
      .catch(error => {
        throw error
      })
  }
}
