'use strict'

/**
 * Classe responsável por operar todos os serviços relacionados
 * à circuitos
 */
module.exports = class CircuitService {

    /**
     * Construtor da classe
     * @param {CircuitRepository} circuitRepository
     */
    constructor(circuitRepository) {
        this.circuitRepository = circuitRepository
    }

    /**
     * Consulta os dados de um circuito
     * @param {Object} options - Objeto com dados da requisição
     * @returns {Object} - dados do circuito encontrado
     */
    async info(options) {
        let uuid = options.query.id
        let attrs = [
            'id', 'name', 'company_id', 'start', 'finish',
            'created_at', 'updated_at'
        ]

        return await this.circuitRepository.info(uuid, attrs, true)
            .then(async result => {
                if (options.query.expandSchedulings)
                    return await this.circuitRepository.indexSchedulings(result.id)
                        .then(schedulings => {
                            result.schedulings = schedulings
                            return result
                        })
                else
                    return result
            })
            .catch(error => {
                throw error
            })
    }


    /**
     * Insere o registro de um novo circuito no banco de dados
     * @param {Object} data.headers - objeto com os headers recebidos na requisição
     * @param {Object} data.payload - objeto com o payload recebido na requisição
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async insert(data) {
        let circuit = {
            name: data.payload.name,
            start: data.payload.start,
            finish: data.payload.finish,
            company_id: data.payload.company_id
        }

        if (data.payload.id)
            circuit.id = data.payload.id

        let schedulings = data.payload.schedulings


        return await this.circuitRepository.insertCircuit(circuit)
            .then(async insertedResult => {

                insertedResult.schedulings = await this.circuitRepository.insertSchedulings(
                    insertedResult.id, schedulings)
                    .then(result => result)
                    .catch(error => {
                        throw error
                    })

                let dataInfo = {
                    headers: data.headers,
                    query: {
                        id: insertedResult.id,
                        expandSchedulings: true
                    }
                }

                return await this.info(dataInfo).then(res => res).catch(error => {
                    throw error
                })
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire uma lista de circuitos no banco de dados
     * @param {Object} options - Objeto com dados da requisição
     * @param {Object} options.headers - Objeto com os headers
     * @param {Object} options.query - Objeto com os params
     * @returns {Array} - lista de registros encontrados no banco
     */
    async index(options) {
        let attrs = [
            'id', 'name', 'company_id', 'start', 'finish',
            'created_at', 'updated_at'
        ]

        let where = {
            company_id: options.query.company_id,
            date_start: options.query.date_start,
            date_finish: options.query.date_finish
        }

        return await this.circuitRepository.index(attrs, where, options.query.expandCompany)
            .then(async circuits => {
                if (options.query.expandSchedulings) {
                    var result = circuits.map(async item => {
                        item.schedulings = await this.circuitRepository.indexSchedulings(item.id)
                        return item
                    })

                    return await Promise.all(result).then(fullObject => fullObject)
                }
                else
                    return circuits
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Atualiza o registro de um circuito no banco de dados
     * @param {Object} data.headers - objeto com os headers recebidos na requisição
     * @param {Object} data.payload - objeto com o payload recebido na requisição
     * @returns {Object} - dados do registro que foi atualizado no banco
     */
    async update(data) {
        let dataInfo = {
            headers: data.headers,
            query: {
                id: data.payload.id,
                expandSchedulings: true
            }
        }

        let updateFields = {
            id: data.payload.id,
            company_id: data.payload.company_id,
            name: data.payload.name
        }

        return await this.info(dataInfo).then(async circuit => {
            return await this.circuitRepository.update(updateFields)
                .then(async updatedResult => {
                    circuit.company_id = updatedResult.company_id
                    circuit.name = updatedResult.name
                    return circuit
                })
                .catch(error => {
                    throw error
                })
        }).catch(error => {
            throw error
        })
    }

    /**
     * Remove um registro de circuito do banco de dados
     * @param {Object} options - objeto com dados da requisição
     * @param {String} options.id - UUID do circuito que será removido
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {
        let dataInfo = {
            headers: options.headers,
            query: {
                id: options.query.id,
                expandSchedulings: true
            }
        }

        return await this.info(dataInfo)
            .then(async result => {

                return await this.circuitRepository.deleteCircuit(result.id)
                    .then(async resultDeleteCircuit => resultDeleteCircuit)
                    .catch(error => {
                        throw error
                    })
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Remove um registro de scheduling do banco de dados
     * @param {Object} options - objeto com dados da requisição
     * @param {String} options.id - UUID do scheduling que será removido
     * @returns {int} - quantidade de registros removidos
     */
    async deleteScheduling(options) {
        return await this.circuitRepository.deleteScheduling(options.query.scheduling_id)
            .then(async result => result)
            .catch(error => {
                throw error
            })
    }
}
