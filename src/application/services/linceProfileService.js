'use strict'

/**
 * Classe responsável por operar todos os serviços relacionados
 * aos endereços de empresas
 */
module.exports = class LinceProfileService {

    /**
     * Construtor da classe
     * @param {LinceProfileRepository} linceProfileRepository 
     */
    constructor(linceProfileRepository) {
        this.linceProfileRepository = linceProfileRepository
    }

    /**
    * Consulta um registro de endereço na base de dados
    * @param {String} options.descricao - descricao
    * @returns {Object} - dados de um endereço
    */
    async info(id) {
        return await this.linceProfileRepository.info(id)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Insere um novo endereço na base de dados
    * @param {Object} options - objeto com os dados do endereço
    * @param {String} options.descricao - descricao
    * @returns {Object} - dados do endereço que foi inserido na base de dados
    */
    async insert(options) {
        return await this.linceProfileRepository.insert(options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Remove um registro de endereço da base de dados
     * @param {Object} options - objeto com os dados do endereço
     * @param {String} options.descricao - descricao
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {
        return await this.linceProfileRepository.delete(options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire uma lista de endereços
     * @param {Object} options - parâmetros para filtrar a busca
     * @returns {Array} - array de registros de endereços
     */
    async index(options) {
        return await this.linceProfileRepository.index([
            'id', 'descricao'
        ], {})
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Atualiza um endereço de uma empresa na base de dados
    * @param {Object} options - objeto com os dados do endereço
    * @param {String} options.descricao - descricao
    * @returns {Object} - endereço com os dados atualizados
    */
    async update(options) {
        return await this.linceProfileRepository.update(options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }
}
