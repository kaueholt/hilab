'use strict'

var UserAgent = require('../utils/userAgent')

/**
 * Classe responsável por operar todos os serviços relacionados
 * à autenticação dos usuários
 */
module.exports = class AuthenticationService {
    /**
     * Construtor da classe
     * @param {Config} config 
     * @param {SecurityManager} securityManager 
     * @param {UserRepository} userRepository
     * @param {AuthenticationRepository} authenticationRepository
     */
    constructor(config, securityManager, userRepository, authenticationRepository, mail) {
        this.config = config
        this.securityManager = securityManager
        this.userRepository = userRepository
        this.authenticationRepository = authenticationRepository
        this.mail = mail
    }

    /**
     * Efetua o login de um usuário
     * @param {Object} data.headers - objeto com os headers recebidos na requisição
     * @param {Object} data.payload - objeto com o payload recebido na requisição
     * @returns {Object} - dados do usuário logado
     */
    async login(data) {
        let options = { search: data.payload.cpf.replace(/[^\w\s]/gi, '') }
        let informedPassword = data.payload.password
        let fields = ['cpf', 'password']

        return await this.userRepository.info(fields, options)
            .then(async result => {

                let grabCpf = result.cpf.substr(result.cpf.length - 3)
                let originalPassword = this.securityManager.decryptPassword(result.password, grabCpf)

                if (result.status === 0)
                    throw new Error('Usuário bloqueado')

                if (originalPassword !== informedPassword)
                    throw new Error('Senha incorreta')

                result = {
                    id: result.id,
                    name: result.name,
                    cpf: result.cpf,
                    email: result.email,
                    date_birth: result.date_birth,
                    avatar: result.avatar,
                    role: result.role,
                    status: result.status
                }

                let os = UserAgent.getOperationSystem(data.headers['user-agent'])
                let browser = UserAgent.getBrowser(data.headers['user-agent'])

                let session = {
                    user_id: result.id,
                    os_name: os.name,
                    os_version: os.version,
                    device: os.device,
                    browser: browser.name,
                    browser_version: browser.version,
                    browser_engine: browser.engine,
                    browser_engine_version: browser.engine_version
                }

                let sessionResult = await this.authenticationRepository.insert(session)
                    .then(resultInsert => {
                        return this.securityManager.sessionDataEncrypt({ sessionId: resultInsert.id })
                    })
                    .catch((error) => {
                        throw error
                    })

                result.token = this.securityManager.generateToken(sessionResult)
                return result
            })
            .catch((error) => {
                throw error
            })
    }

    /**
     * Autentica o usuário com o Bearer Token recebido na requisição.
     * A função validate de @see Server decripta o JWT e traz o objeto auth
     * no objeto de requisição.
     * @param {Object} request - objeto da requisição
     * @returns {Object} - dados do usuário logado
     */
    async authenticate(request) {
        try {
            let credentials = request.auth.credentials
            let sessionIdObj = this.securityManager.sessionDataDecrypt(credentials.encryptedData)
            sessionIdObj = JSON.parse(sessionIdObj)

            return await this.authenticationRepository.info({ session_id: sessionIdObj.sessionId })
                .then(async session => {

                    let os = UserAgent.getOperationSystem(request.headers['user-agent'])
                    let browser = UserAgent.getBrowser(request.headers['user-agent'])

                    if (!this._checkSessionDevice(os, browser, session))
                        throw new Error('Credenciais Inválidas')

                    let fields = ['id', 'name', 'cpf', 'email', 'avatar', 'date_birth']
                    let options = { search: session.user_id }

                    return await this.userRepository.info(fields, options)
                        .then(result => result)
                        .catch(error => {
                            throw error
                        })
                })
                .catch(error => {
                    throw error
                })
        } catch (error) {
            throw error
        }
    }

    /**
     * Efetua o logout do usuário limpando todas as sessões de todos os
     * dispositivos
     * @param {Object} data - objeto com os dados do usuário
     */
    async logout(data) {
        try {
            return await this.authenticationRepository.delete(data)
                .then(result => result)
                .catch(error => {
                    throw error
                })
        } catch (error) {
            throw error
        }
    }

    /**
     * Compara os objetos de entreda com o session obtido no banco
     * @param {Object} os - dados do SO recebidos na requisição
     * @param {Object} browser - dados do Browser recebidos na requisição
     * @returns {boolean} - valor que indica se é o mesmo device
     */
    _checkSessionDevice(os, browser, session) {
        if (os.name !== session.os_name ||
            os.version !== session.os_version ||
            os.device !== session.device)
            return false

        if (browser.name !== session.browser || browser.version !== session.browser_version)
            return false

        return true
    }

    /**
     * Insere o registro de um novo usuário no banco de dados
     * @param {Object} options - objeto com os dados do usuário
     * @param {String} options.name - nome do usuário
     * @param {String} options.cpf - CPF do usuário
     * @param {String} options.email - email do usuário
     * @param {String} options.date_birth - data de nascimento yyyy-mm-dd
     * @param {Int} options.role - permissões do usuário
     * @param {Int} options.status - status do usuário
     * @returns {Object} - dados do usuário que foi inserido no banco
     */
    async signUp(options) {

        const { payload } = options

        payload.password = Math.random().toString(36).slice(2)
        payload.status = Number(payload.status)
        payload.role = Number(payload.role)

        let fileObjetToUpload = payload.avatar || ''

        let grabCpf = payload.cpf.substr(payload.cpf.length - 3)
        payload.password = this.securityManager.encryptPassword(payload.password, grabCpf)
        const decryptedPassword = this.securityManager.decryptPassword(payload.password, grabCpf)

        let arrExtensions = ['png', 'jpg', 'jpeg']
        if (fileObjetToUpload != '' && fileObjetToUpload.hapi.filename != '') {
            payload.avatar = new Date().getTime() + "_" + grabCpf

            if (!this.uploader.setFileToUpload(fileObjetToUpload, payload.avatar, arrExtensions))
                throw new Error("Extensão inválida. Tipos de arquivos permitidos: " + arrExtensions)

            payload.avatar = this.uploader.getFileFormated()
        } else {
            payload.avatar = 'thumb.png'
        }

        return await this.userRepository.insert(options)
            .then(async result => {

                if (fileObjetToUpload != '' && fileObjetToUpload.hapi.filename != '')
                    await this.uploader.uploadFile()

                let body = `<p>Seu cadastro foi realizado com sucesso. <br>
                    Efetue seu primeiro acesso com a senha abaixo:</p> \n
                    <b><em>${decryptedPassword}</em></b>`

                this.mail.setMailConfig(
                    result.email,
                    'Novo cadastro Lince',
                    body
                )

                this.mail.sendMail()
                    .then(resultSendMail => {
                        if (resultSendMail)
                            return resultSendMail
                        else throw new Error('Cadastro efetuato com sucesso porém ocorreu um erro ao tentar enviar o e-mail de confirmação')
                    })
                    .catch(error => {
                        throw error
                    })

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
  * Insere o registro de um novo usuário no banco de dados
  * @param {Object} options - objeto com os dados do usuário
  * @param {String} options.cpf - CPF do usuário
  * @returns {Object} - dados com informações da requisição
  */
    async forgotPassword(options) {

        let grabCpf = options.search.substr(options.search.length - 3)
        const tokenForgotPassword = this.securityManager.generateToken(grabCpf)
        const hashTokenForgotPassword = this.securityManager.sessionDataEncrypt(tokenForgotPassword)
        //const decryptedgrabEmail = this.securityManager.decrypToken(tokenForgotPassword)

        return await this.userRepository.info('cpf', options)
            .then(async result => {

                options.payload = result
                result.token = hashTokenForgotPassword

                return await this.userRepository.update(options)
                    .then(resultUpdateToken => {

                        let body = `<p>E-mail de recuperação de senha.<br>
                        Se foi você que solicitou a recuperação de senha, clique no link abaixo:</p> \n
                        <b><em><a target="_blank" href="${
                            process.env.APP_URL +
                            '/authentication/reset-password/' +
                            hashTokenForgotPassword +
                            '/' + result.cpf
                            }">REDEFINIR MINHA SENHA</a></em></b>
                        <p>Caso não tenha efetuado a solicitação, por gentileza, desconsidere este e-mail.</p>`

                        this.mail.setMailConfig(
                            resultUpdateToken.email,
                            'Recuperação de senha Circuito Saúde ' + resultUpdateToken.name,
                            body
                        )

                        this.mail.sendMail()
                            .then(resultSendMail => resultSendMail)
                            .catch(error => {
                                throw error
                            })

                        return result
                    })
                    .catch(error => {
                        throw new Error('Erro ao atualizar token do usuário')
                    })
            })
            .catch(error => {
                throw error
            })
    }

    /**
 * Insere o registro de um novo usuário no banco de dados
 * @param {Object} options - objeto com os dados do usuário
 * @param {String} options.cpf - CPF do usuário
 * @returns {Object} - dados com informações de da requisição
 */
    async resetPassword(options) {

        const { params } = options

        let userResult = await this.userRepository.info('cpf', options)
            .then(resultUser => {

                let userResultInfo = {
                    name: resultUser.name,
                    email: resultUser.email,
                    date_birth: resultUser.date_birth,
                    status: resultUser.status,
                    role: resultUser.role,
                    avatar: resultUser.avatar,
                    token: resultUser.token
                }
                return userResultInfo
            })
            .catch(error => {
                throw error
            })

        const decryptedTokenUrl = this.securityManager.sessionDataDecrypt(params.token)
        const decryptedTokenUser = this.securityManager.sessionDataDecrypt(userResult.token)

        if (decryptedTokenUrl !== decryptedTokenUser)
            throw new Error('Token não confere ou token expirado')

        let newPassword = Math.random().toString(36).slice(2)
        let grabCpf = params.cpf.substr(params.cpf.length - 3)
        newPassword = this.securityManager.encryptPassword(newPassword, grabCpf)
        const decryptedPassword = this.securityManager.decryptPassword(newPassword, grabCpf)

        options.payload = {
            name: userResult.name,
            email: userResult.email,
            date_birth: userResult.date_birth,
            status: userResult.status,
            role: userResult.role,
            avatar: userResult.avatar,
            password: newPassword,
            token: ''
        }

        return await this.userRepository.update(options)
            .then(async result => {

                let body = `<p>Sua senha foi redefinida com sucesso. <br>
                    Efetue seu acesso com a nova senha abaixo:</p> \n
                    <b><em>${decryptedPassword}</em></b>`

                this.mail.setMailConfig(
                    result.email,
                    'Redefinição de senha Circuito Saúde',
                    body
                )

                this.mail.sendMail()
                    .then(async resultSendMail => resultSendMail)
                    .catch(error => {
                        throw error
                    })
                return result
            })
            .catch(error => {
                throw error
            })
    }
}