'use strict'

/**
 * Classe responsável por operar todos os serviços relacionados
 * aos endereços de empresas
 */
module.exports = class AddressService {

    /**
     * Construtor da classe
     * @param {AddressRepository} addressRepository 
     */
    constructor(addressRepository) {
        this.addressRepository = addressRepository
    }

    /**
    * Consulta um registro de endereço na base de dados
    * @param {String} company_id - string com o id da empresa para busca do endereço
    * @returns {Object} - dados de um endereço
    */
    async info(company_id) {
        return await this.addressRepository.info(company_id)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Insere um novo endereço na base de dados
    * @param {Object} options - objeto com os dados do endereço
    * @param {String} options.company_id - id da empresa
    * @param {String} options.street - rua da empresa
    * @param {String} options.number - número do endereço da empresa
    * @param {String} options.complement - complemento do endereço da empresa
    * @param {String} options.district - bairro da empresa
    * @param {String} options.city - cidade da empresa
    * @param {String} options.state - UF da empresa
    * @param {String} options.zipcode - código postal da empresa
    * @returns {Object} - dados do endereço que foi inserido na base de dados
    */
    async insert(options) {
        return await this.addressRepository.insert(options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Remove um registro de endereço da base de dados
     * @param {Object} options - objeto com os dados do endereço
     * @param {String} options.id - id do endereço
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {
        return await this.addressRepository.delete(options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire uma lista de endereços
     * @param {Object} options - parâmetros para filtrar a busca
     * @returns {Array} - array de registros de endereços
     */
    async index(options) {
        return await this.addressRepository.index([
            'id', 'name', 'email', 'phone',
            'foundation_date', 'status'
        ], {})
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Atualiza um endereço de uma empresa na base de dados
    * @param {Object} options - objeto com os dados do endereço
    * @param {String} options.street - rua da empresa
    * @param {String} options.number - número do endereço da empresa
    * @param {String} options.complement - complemento do endereço da empresa
    * @param {String} options.district - bairro da empresa
    * @param {String} options.city - cidade da empresa
    * @param {String} options.state - UF da empresa
    * @param {String} options.zipcode - código postal da empresa
    * @returns {Object} - endereço com os dados atualizados
    */
    async update(options) {
        return await this.addressRepository.update(options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }
}
