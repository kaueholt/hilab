'use strict'

/**
 * Classe responsável por operar todos os serviços relacionados
 * às empresas
 */
module.exports = class CompanyService {

    /**
     * Construtor da classe
     * @param {CompanyRepository} companyRepository 
     */
    constructor(companyRepository, addressRepository) {
        this.companyRepository = companyRepository
        this.addressRepository = addressRepository
    }

    /**
     * Adquire uma lista de empresas
     * @param {Object} options - parâmetros para filtrar a busca
     * @returns {Array} - array de registros de empresas
     */
    async index(options) {
        let fields = [
            'id', 'name', 'cnpj', 'email',
            'phone', 'foundation_date', 'status'
        ]

        return await this.companyRepository.index(fields, options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Consulta os dados de uma empresa na base de dados
    * @param {Object} option - dados para efetuar a busca
    * @returns {Object} - dados da empresa
    */
    async info(options) {
        let fields = [
            'id', 'cnpj', 'name', 'email',
            'phone', 'foundation_date', 'status',
        ]

        return await this.companyRepository.info(fields, options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
    * Insere uma empresa na base de dados
    * @param {Object} options - objeto com os dados da empresa
    * @returns {Object} - dados da empresa que foram inseridos no banco
    */
    async insert(options) {

        return await this.companyRepository.insert(options)
            .then(async result => {

                options.payload.address.company_id = result.id

                return await this.addressRepository.insert(options)
                    .then(addressInderted => result)
                    .catch(error => {
                        throw error
                    })
            })
            .catch(error => {
                throw error
            })
    }

    /**
    * Atualiza o registro de uma empresa na base de dados
    * @param {Object} options - objeto com os dados da empresa
    * @returns {Object} - empresa com os dados atualizados
    */
    async update(options) {
        options.query = { addresses: true }

        let companyResult = await this.info(options)
            .then(async result => result)
            .catch(error => {
                throw error
            })

        const { payload } = options
        const { Addresses } = companyResult

        options.payload = {
            name: payload.name ? payload.name : companyResult.name,
            email: payload.email ? payload.email : companyResult.email,
            phone: payload.phone ? payload.phone.replace(/[^\w\s]/gi, '') : companyResult.phone,
            foundation_date: payload.foundation_date ? payload.foundation_date : companyResult.foundation_date,
            status: payload.status == 0 || payload.status == 1 ? payload.status : companyResult.status,
        }

        return await this.companyRepository.update(options)
            .then(async result => {

                if (payload.address) {

                    options.payload = {
                        street: payload.address.street ? payload.address.street : Addresses.street,
                        number: payload.address.number ? payload.address.number.replace(/[^\w\s]/gi, '') : Addresses.number,
                        complement: payload.address.complement != '' || payload.address.complement == '' ?
                            payload.address.complement : Addresses.complement,
                        district: payload.address.district ? payload.address.district : Addresses.district,
                        state: payload.address.state ? payload.address.state : Addresses.state,
                        city: payload.address.city ? payload.address.city : Addresses.city,
                        zipcode: payload.address.zipcode ? payload.address.zipcode.replace(/[^\w\s]/gi, '') : Addresses.zipcode
                    }

                    options.payload.company_id = companyResult.id
                    options.search = companyResult.Addresses.id

                    return await this.addressRepository.update(options)
                        .then(addressUpdated => result)
                        .catch(error => {
                            throw error
                        })
                } else {
                    return result
                }
            })
            .catch(error => {
                throw error
            })

    }

    /**
    * Remove o registro de uma empresa na base de dados
    * @param {Object} options - objeto com os dados da empresa
    * @returns {int} - quantidade de registros removidos
    */
    async delete(options) {

        return await this.companyRepository.delete(options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

}
