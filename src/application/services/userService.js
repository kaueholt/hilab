'use strict'

/**
 * Classe responsável por operar todos os serviços relacionados
 * à usuários
 */
module.exports = class UserService {

    /**
     * Construtor da classe
     * @param {UserRepository} userRepository 
     * @param {SecurityManager} securityManager 
     * @param {Mail} mail 
     * @param {Uploader} uploader 
     */
    constructor(userRepository, securityManager, mail, uploader) {
        this.userRepository = userRepository
        this.securityManager = securityManager
        this.mail = mail
        this.uploader = uploader
    }

    /**
     * Adquire uma lista de usuários no banco de dados
     * @param {Object} options - Objeto com dados da requisição
     * @returns {Array} - lista de usuários encontrados no banco
     */
    async index(options) {
        let fields = [
            'id', 'name', 'cpf', 'email',
            'date_birth', 'avatar',
            'status', 'role'
        ]

        return await this.userRepository.index(fields, options)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Consulta os dados de um usuário
     * @param {Object} options - Objeto com dados da requisição
     * @returns {Object} - dados do usuário encontrado
     */
    async info(options) {
        let fields = [
            'id', 'cpf', 'email', 'role',
            'phone', 'status', 'avatar',
        ]

        return await this.userRepository.info(fields, options)
            .then(result => {
                delete result.password && delete result.token
                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere o registro de um novo usuário no banco de dados
     * @param {Object} options - objeto com os dados do usuário
     * @returns {Object} - dados do usuário que foi inserido no banco
     */
    async insert(options) {

        const { payload } = options

        payload.password = Math.random().toString(36).slice(2)
        payload.status = Number(payload.status)
        payload.role = Number(payload.role)

        let fileObjetToUpload = payload.avatar || ''

        let grabCpf = payload.cpf.substr(payload.cpf.length - 3)
        payload.password = this.securityManager.encryptPassword(payload.password, grabCpf)
        const decryptedPassword = this.securityManager.decryptPassword(payload.password, grabCpf)

        let arrExtensions = ['png', 'jpg', 'jpeg']
        if (fileObjetToUpload != '' && fileObjetToUpload.hapi.filename != '') {
            payload.avatar = new Date().getTime() + "_" + grabCpf

            if (!this.uploader.setFileToUpload(fileObjetToUpload, payload.avatar, arrExtensions))
                throw new Error("Extensão inválida. Tipos de arquivos permitidos: " + arrExtensions)

            payload.avatar = this.uploader.getFileFormated()
        } else {
            payload.avatar = 'thumb.png'
        }

        return await this.userRepository.insert(options)
            .then(async result => {

                if (fileObjetToUpload != '' && fileObjetToUpload.hapi.filename != '')
                    await this.uploader.uploadFile()

                let body = `<p>Seu cadastro foi realizado com sucesso. <br>
                    Efetue seu primeiro acesso com a senha abaixo:</p> \n
                    <b><em>${decryptedPassword}</em></b>`

                this.mail.setMailConfig(
                    result.email,
                    'Novo cadastro Lince',
                    body
                )

                this.mail.sendMail()
                    .then(resultSendMail => {
                        if (resultSendMail)
                            return resultSendMail
                        else throw new Error('Cadastro efetuato com sucesso porém ocorreu um erro ao tentar enviar o e-mail de confirmação')
                    })
                    .catch(error => {
                        throw error
                    })

                return result
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Atualiza os dados de um usuário no banco de dados
     * @param {Object} options - objeto com os dados do usuário
     * @returns {Object} - usuário com os dados atualizados
     */
    async update(options) {

        let userResult = await this.info(options)
            .then(async result => {

                let userResultInfo = {
                    name: result.name,
                    email: result.email,
                    date_birth: result.date_birth,
                    status: result.status,
                    role: result.role,
                    avatar: result.avatar
                }
                return userResultInfo
            })
            .catch(error => {
                throw error
            })

        let fileObjetToUpload = options.payload.avatar || ''

        let grabCpf = options.search.substr(options.search.length - 3)

        let arrExtensions = ['png', 'jpg', 'jpeg']
        if (fileObjetToUpload != '' && fileObjetToUpload.hapi.filename != '') {
            options.payload.avatar = new Date().getTime() + "_" + grabCpf

            if (!this.uploader.setFileToUpload(fileObjetToUpload, options.payload.avatar, arrExtensions))
                throw new Error("Extensão inválida. Tipos de arquivos permitidos: " + arrExtensions)

            options.payload.avatar = this.uploader.getFileFormated()
        } else {
            delete options.payload.avatar
        }

        options.payload = {
            name: options.payload.name ? options.payload.name : userResult.name,
            email: options.payload.email ? options.payload.email : userResult.email,
            date_birth: options.payload.date_birth ? options.payload.date_birth : userResult.date_birth,
            status: options.payload.status ? options.payload.status : userResult.status,
            role: options.payload.role ? options.payload.role : userResult.role,
            avatar: options.payload.avatar ? options.payload.avatar : userResult.avatar
        }

        let userUpdated = await this.userRepository.update(options)
            .then(async result => result)
            .catch(error => {
                throw error
            })

        if (fileObjetToUpload != '' && fileObjetToUpload.hapi.filename != '') {
            if (userResult.avatar != 'thumb.png')
                await this.uploader.deleteFile(userResult.avatar)
            await this.uploader.uploadFile()
        }

        return userUpdated
    }

    /**
     * Remove um registro de usuário do banco de dados
     * @param {Object} options - objeto com dados da requisição
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {

        return this.info(options)
            .then(async result => {
                return await this.userRepository.delete(options)
                    .then(async resultDelete => {

                        result.avatar !== 'thumb.png' && await this.uploader.deleteFile(result.avatar)
                        return resultDelete
                    })
                    .catch(error => {
                        throw error
                    })
            })
            .catch(error => {
                throw error
            })
    }
}
