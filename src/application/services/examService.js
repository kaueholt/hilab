'use strict'

/**
 * Classe responsável por operar todos os serviços relacionados
 * ao exames
 */
module.exports = class ExamService {

    /**
     * Construtor da classe
     * @param {ExamRepository} examRepository
     * @param {FormRepository} formRepository
     */
    constructor(examRepository, formRepository) {
        this.examRepository = examRepository
        this.formRepository = formRepository
    }

    /**
     * Consulta os dados de um exame
     * @param {Object} options - Objeto com dados da requisição
     * @returns {Object} - dados do exame encontrado
     */
    async info(options) {
        let uuid = options.query.id
        let attrs = [
            'id', 'circuit_id', 'worker_id', 'start', 'finish',
            'created_at', 'updated_at'
        ]

        return await this.examRepository.info(uuid, attrs, true)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere o registro de um novo exame no banco de dados
     * @param {Object} data.headers - objeto com os headers recebidos na requisição
     * @param {Object} data.payload - objeto com o payload recebido na requisição
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async insert(data) {
        let exam = {
            start: data.payload.start,
            finish: data.payload.finish,
            circuit_id: data.payload.circuit_id,
            worker_id: data.payload.worker_id
        }

        return await this.examRepository.insert(exam)
            .then(async insertedResult => {
                let options = {
                    query: { id: insertedResult.id },
                    auth: data.auth
                }

                return await this.info(options).then(res => res).catch(error => {
                    throw error
                })
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire uma lista de exames no banco de dados
     * @param {Object} options - Objeto com dados da requisição
     * @param {Object} options.headers - Objeto com os headers
     * @param {Object} options.query - Objeto com os params
     * @returns {Array} - lista de registros encontrados no banco
     */
    async index(options) {
        let attrs = [
            'id', 'circuit_id', 'worker_id', 'start', 'finish',
            'created_at', 'updated_at'
        ]

        let where = {
            circuit_id: options.query.circuit_id,
            worker_id: options.query.worker_id,
            date_start: options.query.date_start,
            date_finish: options.query.date_finish
        }

        return await this.examRepository.index(attrs, where, options.query.expandWorker)
            .then(exams => exams)
            .catch(error => {
                throw error
            })
    }

    /**
     * Remove um registro do banco de dados
     * @param {Object} options - objeto com dados da requisição
     * @param {String} options.id - UUID do exame que será removido
     * @returns {int} - quantidade de registros removidos
     */
    async delete(options) {

        let dataInfo = {
            headers: options.headers,
            query: {
                id: options.query.id
            }
        }

        return await this.info(dataInfo)
            .then(async result => {
                return await this.examRepository.delete(result.id)
                    .then(deleteResult => deleteResult)
                    .catch(error => {
                        throw error
                    })
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere o registro de uma nova classe de exame no banco de dados
     * @param {Object} data.headers - objeto com os headers recebidos na requisição
     * @param {Object} data.payload - objeto com o payload recebido na requisição
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async insertExamClass(data) {
        let examClass = {
            name: data.payload.name
        }

        return await this.examRepository.insertExamClass(examClass)
            .then(insertedResult => insertedResult)
            .catch(error => {
                throw error
            })
    }

    /**
* Consulta os dados de um tipo de exame
* @param {Object} options - Objeto com dados da requisição
* @returns {Object} - dados do exame encontrado
*/
    async infoExamClass(options) {
        let attrs = [
            'id', 'name',
            'created_at', 'updated_at'
        ]

        return await this.examRepository.infoExamClass(options, attrs)
            .then(result => result)
            .catch(error => {
                throw error
            })
    }

    /**
     * Insere o registro de uma nova classe de exame no banco de dados
     * @param {Object} data.headers - objeto com os headers recebidos na requisição
     * @param {Object} data.query - objeto com os parâmetros recebidos na requisição
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async deleteExamClass(data) {
        return await this.examRepository.deleteExamClass(data.query.id)
            .then(deleteResult => deleteResult)
            .catch(error => {
                throw error
            })
    }

    /**
     * Atualiza o registro de uma nova classe de exame no banco de dados
     * @param {Object} data.headers - objeto com os headers recebidos na requisição
     * @param {Object} data.payload - objeto com o payload recebido na requisição
     * @returns {Object} - dados do registro que foi atualizado no banco
     */
    async updateExamClass(data) {
        let updateFields = {
            id: data.payload.id,
            name: data.payload.name
        }

        return await this.examRepository.updateExamClass(updateFields)
            .then(updatedResult => updatedResult)
            .catch(error => {
                throw error
            })
    }

    /**
     * Adquire uma lista de classes de exames no banco de dados
     * @param {Object} options - Objeto com dados da requisição
     * @param {Object} options.headers - Objeto com os headers
     * @param {Object} options.query - Objeto com os params
     * @returns {Array} - lista de registros encontrados no banco
     */
    async indexExamClass(data) {
        let attrs = [
            'id', 'name', 'created_at', 'updated_at'
        ]
        data.attrs = attrs

        return await this.examRepository.indexExamClass(data)
            .then(exams => exams)
            .catch(error => {
                throw error
            })
    }
}
