
'use strict';

/**
 * Model da tabela topic_form
 * @version 1.0.0 - migration: 20200528195254-create-topic-form.js
 * @version 1.0.1 - migration: 20200528195638-add-topic-formulary-association.js
 */
module.exports = (sequelize, DataTypes) => {
  const TopicForms = sequelize.define('TopicForms', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
      autoIncrement: false
    },
    formulary_id: DataTypes.UUID,
    sequence: DataTypes.INTEGER,
    status: DataTypes.INTEGER,
    description: DataTypes.STRING
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  TopicForms.associate = function (models) {
    TopicForms.belongsTo(models.Formularies, { as: 'formularies', foreignKey: 'formulary_id' });
    TopicForms.hasMany(models.Questions, { as: 'questions' });
  };
  return TopicForms;
};
