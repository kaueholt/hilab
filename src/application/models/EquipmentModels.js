'use strict'

/**
 * Utilizada para criar instâncias de EquipmentModels estendida da classe Model da
 * biblioteca Sequelize
 * @version 1.0.0 - migration: 20200409225919-create-equipment-models.js
 */
module.exports = (sequelize, DataTypes) => {
  const EquipmentModels = sequelize.define('EquipmentModels', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
      autoIncrement: false
    },
    name: DataTypes.STRING(30),
    maker: DataTypes.STRING(30),
    description: DataTypes.STRING(200),
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  })
  EquipmentModels.associate = function (models) {
    EquipmentModels.hasMany(models.EquipmentPatrimonies, { as: 'equipment_patrimonies'})
  }
  return EquipmentModels
}