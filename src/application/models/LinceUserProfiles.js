'use strict'

module.exports = (sequelize, DataTypes) => {
    const LinceUserProfiles = sequelize.define('LinceUserProfiles', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        status: {
            type: DataTypes.INTEGER,
            unique: false,
            allowNull: true
        },
        origemCadastro: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        perfil_id: {
            type: DataTypes.UUID,
            unique: false,
            allowNull: true
        },
        lince_usuario_id: {
            type: DataTypes.UUID,
            unique: true,
            allowNull: false
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    LinceUserProfiles.associate = function (models) {
        LinceUserProfiles.belongsTo(models.LinceProfiles, { as: 'perfil', foreignKey: 'perfil_id' });
        LinceUserProfiles.belongsTo(models.LinceUsers, { as: 'user', foreignKey: 'lince_usuario_id' });
    };
    return LinceUserProfiles
}