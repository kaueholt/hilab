'use strict'

module.exports = (sequelize, DataTypes) => {
    const LinceAnswerTypes = sequelize.define('LinceAnswerTypes', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        tipo: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    LinceAnswerTypes.associate = function (models) {
        LinceAnswerTypes.hasMany(models.LinceTipQuestions)
    };
    return LinceAnswerTypes
}