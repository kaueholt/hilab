'use strict'

/**
 * Utilizada para criar instâncias de Addresses estendida da classe Model da
 * biblioteca Sequelize
 * @version 1.0.0 - migration: 20200403045726-create-addresses.js
 * @version 1.0.1 - migration: 20200408151727-create-addresses-association.js
 */
module.exports = (sequelize, DataTypes) => {
    const Addresses = sequelize.define('Addresses', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            unique: true
        },
        company_id: {
            type: DataTypes.UUID,
            allowNull: false,
            unique: false,
        },
        street: DataTypes.STRING,
        number: DataTypes.STRING,
        complement: DataTypes.STRING,
        district: DataTypes.STRING,
        city: DataTypes.STRING,
        state: DataTypes.STRING,
        zipcode: DataTypes.STRING,
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    Addresses.associate = function (models) {
        Addresses.belongsTo(models.Companies)
      }
    return Addresses
}