'use strict';

/**
 * Model da tabela Exams
 * @version 1.0.0 - migration: 20200504165130-create-exams.js
 * @version 1.0.1 - migration: 20200504165556-add-exam-circuits-association.js
 * @version 1.0.2 - migration: 20200504174712-add-exam-workers-association.js
 */
module.exports = (sequelize, DataTypes) => {
  const Exams = sequelize.define('Exams', {
    circuit_id: DataTypes.UUID,
    worker_id: DataTypes.UUID,
    formulary_id: DataTypes.UUID,
    start: DataTypes.DATE,
    finish: DataTypes.DATE
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  Exams.associate = function (models) {
    Exams.belongsTo(models.Circuits, { as: 'circuit', foreignKey: 'circuit_id' });
    Exams.belongsTo(models.Workers, { as: 'worker', foreignKey: 'worker_id' });
    Exams.belongsTo(models.Formularies, { as: 'formulary', foreignKey: 'formulary_id' });
  };
  return Exams;
};