'use strict'

module.exports = (sequelize, DataTypes) => {
    const LinceTips = sequelize.define('LinceTips', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        titulo: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        descricao: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        link: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        status: {
            type: DataTypes.INTEGER,
            unique: false,
            allowNull: true
        },
        perfil_id: {
            type: DataTypes.UUID,
            unique: false,
            allowNull: true
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    LinceTips.associate = function (models) {
        LinceTips.belongsTo(models.LinceProfiles, { as: 'perfil', foreignKey: 'perfil_id' });
    };
    return LinceTips
}