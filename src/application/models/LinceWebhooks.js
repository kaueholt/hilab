'use strict'

module.exports = (sequelize, DataTypes) => {
    const LinceWebhooks = sequelize.define('LinceWebhooks', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        type: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        json: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        CNPJ: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        custom: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        token: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        parceiro_id: {
            type: DataTypes.UUID,
            unique: false,
            allowNull: false
        }
    }, {
        datahora: 'datahora',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    LinceWebhooks.associate = function (models) {
        LinceWebhooks.belongsTo(models.LincePartners, { as: 'parceiro', foreignKey: 'parceiro_id' });
    };
    return LinceWebhooks
}