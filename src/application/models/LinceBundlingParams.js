'use strict'

module.exports = (sequelize, DataTypes) => {
    const LinceBundlingParams = sequelize.define('LinceBundlingParams', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        endpointwebhook: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        username: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        password: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        token_login: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        token_sessao: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        parceiro_id: {
            type: DataTypes.UUID,
            unique: true,
            allowNull: true
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        tokeValidade: 'token_validade',
        timestamps: true,
    })
    LinceBundlingParams.associate = function (models) {
        LinceBundlingParams.belongsTo(models.LincePartners, { as: 'partner', foreignKey: 'parceiro_id' });
    };
    return LinceBundlingParams
}