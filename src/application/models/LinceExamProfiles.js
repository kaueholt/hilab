'use strict'

module.exports = (sequelize, DataTypes) => {
    const LinceExamProfiles = sequelize.define('LinceExamProfiles', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        questao: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: true
        },
        perfil_id: {
            type: DataTypes.UUID,
            unique: false,
            allowNull: true
        },
        sesi_tipoexame_id: {
            type: DataTypes.UUID,
            unique: true,
            allowNull: true
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    LinceExamProfiles.associate = function (models) {
        LinceExamProfiles.belongsTo(models.LinceProfiles, { as: 'perfil', foreignKey: 'perfil_id' });
        LinceExamProfiles.belongsTo(models.ExamClass, { as: 'exame', foreignKey: 'sesi_tipoexame_id' });
    };
    return LinceExamProfiles
}