'use strict'

/**
 * Utilizada para criar instâncias de Session estendida da classe Model da
 * biblioteca Sequelize
 * @version 1.0.0 - migration: 20200325180212-create-sessions.js
 * @version 1.0.1 - migration: 20200408151912-create-sessions-association.js
 */
module.exports = (sequelize, DataTypes) => {
    const Sessions = sequelize.define('Sessions', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        user_id: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        os_name: DataTypes.STRING,
        os_version: DataTypes.STRING,
        device: DataTypes.STRING,
        browser: DataTypes.STRING,
        browser_version: DataTypes.STRING,
        browser_engine: DataTypes.STRING,
        browser_engine_version: DataTypes.STRING
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    Sessions.associate = function (models) {
        Sessions.belongsTo(models.Users)
    }
    return Sessions
}