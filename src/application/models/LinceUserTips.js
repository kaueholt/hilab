'use strict'

module.exports = (sequelize, DataTypes) => {
    const LinceUserTips = sequelize.define('LinceUserTips', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        media: DataTypes.NUMBER,
        sesi_usuario_id: DataTypes.INTEGER,
        dica_id: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        lince_usuario_id: {
            type: DataTypes.UUID,
            allowNull: false,
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        dataEnvio: 'data_envio',
        timestamps: true,
    })
    LinceUserTips.associate = function (models) {
        LinceUserTips.belongsTo(models.LinceTips, { as: 'dica', foreignKey: 'dica_id' });
        LinceUserTips.belongsTo(models.LinceUsers, { as: 'usuario', foreignKey: 'lince_usuario_id' });
    };
    return LinceUserTips
}