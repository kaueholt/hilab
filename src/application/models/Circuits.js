'use strict';

/**
 * Model da tabela Circuits
 * @version 1.0.0 - migration: 20200406142345-create-circuits.js
 * @version 1.0.1 - migration: 20200408180425-add-circuits-companies-association.js
 */
module.exports = (sequelize, DataTypes) => {
  const Circuits = sequelize.define('Circuits', {
    name: DataTypes.STRING,
    company_id: DataTypes.UUID,
    start: DataTypes.DATE,
    finish: DataTypes.DATE
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  Circuits.associate = function (models) {
    Circuits.belongsTo(models.Companies, { as: 'company', foreignKey: 'company_id' });
  };
  return Circuits;
};