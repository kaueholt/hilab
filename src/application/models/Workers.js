'use strict'

/**
 * Utilizada para criar instâncias de Worker estendida da classe Model da
 * biblioteca Sequelize
 * @version 1.0.0 - migration: 20200403045755-create-workers.js
 * @version 1.0.1 - migration: 20200408151247-create-workers-association.js
 */
module.exports = (sequelize, DataTypes) => {
     const Workers = sequelize.define('Workers', {
          id: {
               type: DataTypes.UUID,
               primaryKey: true,
               allowNull: false,
          },
          company_id: {
               type: DataTypes.UUID,
               allowNull: false,
               unique: false
          },
          name: DataTypes.STRING,
          cpf: {
               type: DataTypes.STRING,
               unique: true,
               allowNull: false
          },
          email: DataTypes.STRING,
          avatar: DataTypes.STRING,
          date_birth: DataTypes.DATE,
          status: DataTypes.INTEGER,
          phone: DataTypes.STRING,
          gender: DataTypes.STRING,
          race: DataTypes.STRING,
          pis_nit: {
               type: DataTypes.STRING,
               allowNull: false,
               unique: true
          },
          rfid: DataTypes.STRING,
          role: DataTypes.STRING,
          observation: DataTypes.TEXT
     }, {
          createdAt: 'created_at',
          updatedAt: 'updated_at',
          timestamps: true,
     })
     Workers.associate = function (models) {
          Workers.belongsTo(models.Companies)
     }
     return Workers
}