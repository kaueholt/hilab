'use strict'

module.exports = (sequelize, DataTypes) => {
    const LinceUserTipAnswers = sequelize.define('LinceUserTipAnswers', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        resposta: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        dicapergunta_id: {
            type: DataTypes.UUID,
            unique: false,
            allowNull: false
        },
        usuariodica_id: {
            type: DataTypes.UUID,
            unique: false,
            allowNull: false
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    LinceUserTipAnswers.associate = function (models) {
        LinceUserTipAnswers.belongsTo(models.LinceTipQuestions, { as: 'dicaPergunta', foreignKey: 'dicapergunta_id' });
        LinceUserTipAnswers.belongsTo(models.LinceUserTips, { as: 'usuarioDica', foreignKey: 'usuariodica_id' });
    };
    return LinceUserTipAnswers
}