'use strict'

module.exports = (sequelize, DataTypes) => {
    const LinceUsers = sequelize.define('LinceUsers', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        cpf: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        status: DataTypes.INTEGER,
        sesi_usuario_id: DataTypes.INTEGER
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    LinceUsers.associate = function (models) {
        LinceUsers.hasMany(models.LinceUserTips)
        LinceUsers.hasMany(models.LinceUserProfiles)
        LinceUsers.belongsTo(models.Users, { as: 'user', foreignKey: 'sesi_usuario_id' });
    };
    return LinceUsers
}