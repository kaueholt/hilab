
'use strict';

/**
 * Model da tabela question_item
 * @version 1.0.0 - migration: 20200528195323-create-question-item.js
 * @version 1.0.1 - migration: 20200528200548-add-question-question-item-association.js
 */
module.exports = (sequelize, DataTypes) => {
  const QuestionItems = sequelize.define('QuestionItems', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
      autoIncrement: false
    },
    question_id: DataTypes.UUID,
    topic_form_id: DataTypes.UUID,
    formulary_id: DataTypes.UUID,
    sequence: DataTypes.INTEGER,
    description: DataTypes.STRING,
    value: DataTypes.STRING,
    status: DataTypes.INTEGER,
    complement: DataTypes.STRING
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  QuestionItems.associate = function (models) {
    QuestionItems.belongsTo(models.Formularies, { as: 'formularies', foreignKey: 'formulary_id' });
    QuestionItems.belongsTo(models.Questions, { as: 'questions', foreignKey: 'question_id' });
  };
  return QuestionItems;
};
