'use strict';

/**
 * Model da tabela Formulary
 * @version 1.0.0 - migration: 20200513124702-create-formulary.js
 * @version 1.0.1 - migration: 20200513124852-add-formulary-exam-class-association.js
 */
module.exports = (sequelize, DataTypes) => {
  const Formularies = sequelize.define('Formularies', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
      autoIncrement: false
    },
    exam_class_id: DataTypes.UUID,
    equipment_model_id: DataTypes.UUID,
    status: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  Formularies.associate = function (models) {
    Formularies.hasMany(models.TopicForms, { as: 'topic_forms' });
    Formularies.belongsTo(models.ExamClass, { as: 'exam_class', foreignKey: 'exam_class_id' });
    Formularies.belongsTo(models.EquipmentModels, { as: 'equipment_model', foreignKey: 'equipment_model_id' });
  };
  return Formularies;
};