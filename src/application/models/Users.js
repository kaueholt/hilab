'use strict'

/**
 * Utilizada para criar instâncias de User estendida da classe Model da
 * biblioteca Sequelize
 * @version 1.0.0 - migration: 20200403045532-create-users.js
 */
module.exports = (sequelize, DataTypes) => {
    const Users = sequelize.define('Users', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        name: DataTypes.STRING,
        cpf: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        password: DataTypes.STRING,
        token: DataTypes.STRING,
        email: DataTypes.STRING,
        avatar: DataTypes.STRING,
        date_birth: DataTypes.DATE,
        status: DataTypes.INTEGER,
        role: DataTypes.INTEGER
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    Users.associate = function (models) {
        Users.hasMany(models.Sessions)
    }
    return Users
}