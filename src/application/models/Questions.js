
'use strict';

/**
 * Model da tabela question
 * @version 1.0.0 - migration: 20200528195317-create-question.js
 * @version 1.0.1 - migration: 20200528200314-add-question-topic-association.js
 */
module.exports = (sequelize, DataTypes) => {
  const Questions = sequelize.define('Questions', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
      autoIncrement: false
    },
    formulary_id: DataTypes.UUID,
    topic_form_id: DataTypes.UUID,
    sequence: DataTypes.INTEGER,
    status: DataTypes.INTEGER,
    complement: DataTypes.STRING,
    type: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  Questions.associate = function (models) {
    Questions.belongsTo(models.Formularies, { as: 'formularies', foreignKey: 'formulary_id' });
    Questions.belongsTo(models.TopicForms, { as: 'topic_forms', foreignKey: 'topic_form_id' });
    Questions.hasMany(models.QuestionItems, { as: 'question_items' });
  };
  return Questions;
};
