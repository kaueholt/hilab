'use strict'

/**
 * Utilizada para criar instâncias de Companies estendida da classe Model da
 * biblioteca Sequelize
 * @version 1.0.0 - migration: 20200403045609-create-companies.js
 */
module.exports = (sequelize, DataTypes) => {
    const Companies = sequelize.define('Companies', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            unique: true
        },
        name: DataTypes.STRING,
        cnpj: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        email: DataTypes.STRING,
        phone: DataTypes.STRING,
        foundation_date: DataTypes.DATE,
        status: DataTypes.INTEGER,
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    Companies.associate = function (models) {
        Companies.hasMany(models.Addresses)
        Companies.hasMany(models.Workers)
    }
    return Companies
}

