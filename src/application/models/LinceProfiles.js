'use strict'

module.exports = (sequelize, DataTypes) => {
    const LinceProfiles = sequelize.define('LinceProfiles', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        descricao: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    LinceProfiles.associate = function (models) {
        LinceProfiles.hasMany(models.LinceUserTips)
        LinceProfiles.hasMany(models.LinceUserProfiles)
        LinceProfiles.hasMany(models.LinceExamProfiles)
    };
    return LinceProfiles
}