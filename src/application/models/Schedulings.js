'use strict';

/**
 * Model da tabela Scheduling
 * @version 1.0.0 - migration: 20200406141730-create-scheduling.js
 * @version 1.0.1 - migration: 20200406143330-add-scheduling-circutis-association.js
 */
module.exports = (sequelize, DataTypes) => {
  const Schedulings = sequelize.define('Schedulings', {
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    circuit_id: DataTypes.UUID,
    equipment_id: DataTypes.UUID
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  Schedulings.associate = function (models) {
    Schedulings.belongsTo(models.Circuits, { as: 'circuits' });
    Schedulings.belongsTo(models.EquipmentPatrimonies, { as: 'equipment_patrimony', foreignKey: 'equipment_id' });
  };
  return Schedulings;
};