'use strict';

/**
 * Model da tabela ExamClass
 * @version 1.0.0 - migration: 20200513124633-create-exam-class.js
 */
module.exports = (sequelize, DataTypes) => {
  const ExamClass = sequelize.define('ExamClass', {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        autoIncrement: false
    },
    name: DataTypes.STRING,
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  return ExamClass;
};