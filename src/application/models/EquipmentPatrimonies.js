'use strict'

/**
 * Utilizada para criar instâncias de EquipmentPatrimony estendida da classe Model da
 * biblioteca Sequelize
 * @version 1.0.0 - migration: 20200409225921-create-equipment-patrimonies.js
 */
module.exports = (sequelize, DataTypes) => {
  const EquipmentPatrimonies = sequelize.define('EquipmentPatrimonies', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
      autoIncrement: false
    },
    equipment_model_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    code: DataTypes.STRING(10),
    mac: DataTypes.STRING(32),
    status: DataTypes.INTEGER,
    token: DataTypes.STRING(255),
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  })
  EquipmentPatrimonies.associate = function (models) {
    EquipmentPatrimonies.belongsTo(models.EquipmentModels, { as: 'equipment_model'})
  }
  return EquipmentPatrimonies
}