'use strict'

module.exports = (sequelize, DataTypes) => {
    const LinceTipQuestions = sequelize.define('LinceTipQuestions', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        pergunta: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: false
        },
        peso: {
            type: DataTypes.INTEGER,
            unique: false,
            allowNull: true
        },
        resposta: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: true
        },
        tipo_resposta_id: {
            type: DataTypes.UUID,
            unique: false,
            allowNull: false
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    LinceTipQuestions.associate = function (models) {
        LinceTipQuestions.belongsTo(models.LinceAnswerTypes, { as: 'tipoResposta', foreignKey: 'tipo_resposta_id' });
    };
    return LinceTipQuestions
}