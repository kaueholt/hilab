'use strict'

module.exports = (sequelize, DataTypes) => {
    const LincePartners = sequelize.define('LincePartners', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            autoIncrement: false
        },
        nome: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        CNPJ: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: true
        },
        parceiro_id: {
            type: DataTypes.UUID,
            unique: true,
            allowNull: true
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
    })
    LincePartners.associate = function (models) {
        LincePartners.hasMany(models.LinceWebhooks)
    };
    return LincePartners
}