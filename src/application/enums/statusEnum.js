'use strict'

/**
 * Enumerador de status code
 */
const StatusEnum = () => {
    return {
        badGateway: 502,
        badRequest: 400,
        unprocessableEntity: 422,
        paymentNecessary: 402,
        unauthorized: 401,
        forbidden: 403,
        notFound: 404,
        conflict: 409,
        resourceGone: 410,
        tooManyRequests: 429,
        success: 200,
        internalServerError: 500
    }
}

module.exports = StatusEnum()