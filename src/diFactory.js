'use strict'
const kontainer = require('kontainer-di')

/**
 * Script responsável por Injetar as Dependências das classes dessa API (Dependency Injection Pattern)
 */
const customFactory = () => {
    kontainer.reset()

    //################ LIBS, SETUP e UTILS ################################
    kontainer.register('hapi', [], require('@hapi/hapi'))
    kontainer.register('joi', [], require('@hapi/joi'))
    kontainer.register('crypto', [], require('crypto'))
    kontainer.register('config', [], require('./config/config'))
    kontainer.register('jwt', [], require('jsonwebtoken'))
    kontainer.register('dbConfig', [], require('./database/database'))
    kontainer.register('mail', ['config'], require('./application/utils/mail'))
    kontainer.register('uploader', ['config'], require('./application/utils/uploader'))
    kontainer.register('validationUtils', [], require('./application/utils/validationUtils'))
    kontainer.register('statusEnum', [], require('./application/enums/statusEnum'))
    kontainer.register('replyManager', ['config', 'statusEnum'], require('./application/managers/replyManager'))
    kontainer.register('securityManager', [
        'config',
        'crypto',
        'jwt',
        'replyManager'
    ], require('./application/managers/securityManager'))

    //############### REPOSITORY ################################
    kontainer.register('userRepository', [], require('./application/repository/userRepository'))
    kontainer.register('authenticationRepository', [], require('./application/repository/authenticationRepository'))
    kontainer.register('companyRepository', [], require('./application/repository/companyRepository'))
    kontainer.register('addressRepository', [], require('./application/repository/addressRepository'))
    kontainer.register('workerRepository', [], require('./application/repository/workerRepository'))
    kontainer.register('equipmentRepository', [], require('./application/repository/equipmentRepository'))
    kontainer.register('circuitRepository', [], require('./application/repository/circuitRepository'))
    kontainer.register('examRepository', [], require('./application/repository/examRepository'))
    kontainer.register('formRepository', [], require('./application/repository/formRepository'))
    kontainer.register('linceAnswerTypeRepository', [], require('./application/repository/linceAnswerTypeRepository'))
    kontainer.register('linceExamProfileRepository', [], require('./application/repository/linceExamProfileRepository'))
    kontainer.register('lincePartnerRepository', [], require('./application/repository/lincePartnerRepository'))
    kontainer.register('linceProfileRepository', [], require('./application/repository/linceProfileRepository'))
    kontainer.register('linceTipRepository', [], require('./application/repository/linceTipRepository'))
    kontainer.register('linceUserProfileRepository', [], require('./application/repository/linceUserProfileRepository'))
    kontainer.register('linceUserRepository', [], require('./application/repository/linceUserRepository'))

    //############### SERVICES ################################
    //user
    kontainer.register('userService', [
        'userRepository',
        'securityManager',
        'mail',
        'uploader'
    ], require('./application/services/userService'))
    //company
    kontainer.register('companyService', [
        'companyRepository',
        'addressRepository'
    ], require('./application/services/companyService'))
    //address
    kontainer.register('addressService', [
        'addressRepository'
    ], require('./application/services/addressService'))
    //worker
    kontainer.register('workerService', [
        'workerRepository',
        'uploader'
    ], require('./application/services/workerService'))
    //equipment
    kontainer.register('equipmentService', [
        'equipmentRepository',
    ], require('./application/services/equipmentService'))
    //circuits
    kontainer.register('circuitService', [
        'circuitRepository'
    ], require('./application/services/circuitService'))
    //exams
    kontainer.register('examService', [
        'examRepository',
        'formRepository'
    ], require('./application/services/examService'))
    //forms
    kontainer.register('formService', [
        'formRepository'
    ], require('./application/services/formService'))
    //Lince
    // kontainer.register('linceAnswerTypeService', [
    //     'linceAnswerTypeRepository'
    // ], require('./application/services/linceAnswerTypeService'))
    // kontainer.register('linceExamProfileService', [
    //     'linceExamProfileRepository'
    // ], require('./application/services/linceExamProfileService'))
    // kontainer.register('lincePartnerService', [
    //     'lincePartnerRepository'
    // ], require('./application/services/lincePartnerService'))
    kontainer.register('linceProfileService', [
        'linceProfileRepository'
    ], require('./application/services/linceProfileService'))
    // kontainer.register('linceTipService', [
    //     'linceTipRepository'
    // ], require('./application/services/linceTipService'))
    // kontainer.register('linceUserProfileService', [
    //     'linceUserProfileRepository'
    // ], require('./application/services/linceUserProfileService'))
    // kontainer.register('linceUserService', [
    //     'linceUserRepository'
    // ], require('./application/services/linceUserService'))
    //auth
    kontainer.register('authenticationService', [
        'config',
        'securityManager',
        'userRepository',
        'authenticationRepository',
        'mail'
    ], require('./application/services/authenticationService'))
    //facade
    // 'linceAnswerTypeService',
    // 'linceExamProfileService',
    // 'lincePartnerService',
    // 'linceTipService',
    // 'linceUserProfileService',
    // 'linceUserService',
    kontainer.register('facadeManager', [
        'replyManager',
        'authenticationService',
        'userService',
        'workerService',
        'companyService',
        'addressService',
        'equipmentService',
        'circuitService',
        'examService',
        'formService',
        'linceProfileService',
    ], require('./application/managers/facadeManager'))

    //############### AUTHENTICATION ################################
    kontainer.register('authenticationController', [
        'facadeManager',
        'statusEnum',
        'replyManager',
        'validationUtils'
    ], require('./routes/controllers/authenticationController'))
    kontainer.register('authenticationRoutes', [
        'authenticationController',
        'config'
    ], require('./routes/authenticationRoutes'))

    //############### USER ################################
    kontainer.register('userController', [
        'facadeManager',
        'replyManager',
        'validationUtils'
    ], require('./routes/controllers/userController'))
    kontainer.register('userRoutes', ['userController', 'config'], require('./routes/userRoutes'))

    //############### COMPANY #############################
    kontainer.register('companyController', [
        'facadeManager',
        'replyManager',
        'validationUtils'
    ], require('./routes/controllers/companyController'))
    kontainer.register('companyRoutes', ['companyController', 'config'], require('./routes/companyRoutes'))

    //############### WORKER ################################
    kontainer.register('workerController', [
        'facadeManager',
        'replyManager',
        'validationUtils'
    ], require('./routes/controllers/workerController'))
    kontainer.register('workerRoutes', ['workerController', 'config'], require('./routes/workerRoutes'))

    //############### EQUIPMENTS ################################
    kontainer.register('equipmentController', [
        'facadeManager',
        'replyManager',
    ], require('./routes/controllers/equipmentController'))
    kontainer.register('equipmentRoutes', ['equipmentController', 'config'], require('./routes/equipmentRoutes'))


    //############### CIRCUITS #############################
    kontainer.register('circuitController', [
        'facadeManager',
        'replyManager'
    ], require('./routes/controllers/circuitController'))

    kontainer.register('circuitRoutes', [
        'joi',
        'circuitController',
        'config'
    ], require('./routes/circuitRoutes'))

    //############### EXAMS #############################
    kontainer.register('examController', [
        'facadeManager',
        'replyManager'
    ], require('./routes/controllers/examController'))

    kontainer.register('examRoutes', [
        'joi',
        'examController',
        'config'
    ], require('./routes/examRoutes'))

    kontainer.register('examClassRoutes', [
        'joi',
        'examController',
        'config'
    ], require('./routes/examClassRoutes'))

    //############### FORMULARIES #############################
    kontainer.register('formController', [
        'facadeManager',
        'replyManager'
    ], require('./routes/controllers/formController'))

    kontainer.register('formRoutes', [
        'joi',
        'formController',
        'config'
    ], require('./routes/formRoutes'))

    //############### LINCE #############################
    kontainer.register('linceProfileController', [
        'facadeManager',
        'replyManager'
    ], require('./routes/controllers/linceProfileController'))

    kontainer.register('linceRoutes', [
        'joi',
        'linceProfileController',
        'config'
    ], require('./routes/linceRoutes'))

    //############### LAYER: SERVER ################################
    kontainer.register('routes', [
        'authenticationRoutes',
        'userRoutes',
        'companyRoutes',
        'workerRoutes',
        'equipmentRoutes',
        'circuitRoutes',
        'examRoutes',
        'examClassRoutes',
        'linceRoutes',
        'formRoutes'
    ], require('./routes'))

    kontainer.register('server', ['hapi', 'securityManager', 'config', 'routes'], require('./server'))

    return kontainer
}

module.exports = {
    customFactory
}
