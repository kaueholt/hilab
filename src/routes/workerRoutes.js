'use strict'
const Joi = require('@hapi/joi')

/**
 * Classe responsável por configurar as rotas para serviços de usuários
 */
module.exports = class WorkerRoutes {
    constructor(workerController, config) {
        this.config = config
        this.workerController = workerController
        this.path = "/workers"

        this.index = {
            path: '/{company_id}' + this.path,
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para consultar uma lista de colaboradores",
                notes: "Consome WorkerService",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        company_id: Joi.string().trim().required()
                    }),
                    query: Joi.object({
                        name: Joi.string().default(''),
                        cpf: Joi.string().default(''),
                        rfid: Joi.string().default(''),
                        company: Joi.boolean().default(false)
                    })
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.array().items({
                            id: Joi.string().required(),
                            company_id: Joi.string().required(),
                            name: Joi.string().required(),
                            cpf: Joi.string().required(),
                            email: Joi.string().required(),
                            date_birth: Joi.string().required(),
                            avatar: Joi.string().required(),
                            status: Joi.number().required(),
                            pis_nit: Joi.string().required(),
                            phone: Joi.string().required(),
                            gender: Joi.string().required(),
                            race: Joi.string().required(),
                            observation: Joi.string().allow(''),
                            rfid: Joi.string().allow(''),
                            role: Joi.string().required(),
                            created_at: Joi.date(),
                            updated_at: Joi.date(),
                            CompanyId: Joi.string(),
                            Company: Joi.object({
                                id: Joi.string(),
                                name: Joi.string(),
                                cnpj: Joi.string(),
                                email: Joi.string(),
                                phone: Joi.string(),
                                foundation_date: Joi.string(),
                                status: Joi.number(),
                            })
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await workerController.index(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.info = {
            path: this.path + '/{cpf}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir informações de um colaborador",
                notes: "Consome WorkerServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cpf: Joi.string().trim().required()
                    }),
                    query: Joi.object({
                        name: Joi.string().default(''),
                        company: Joi.boolean().default(false)
                    }),
                },
                response: {
                    schema:
                        Joi.object({
                            message: Joi.string(),
                            data: Joi.object({
                                id: Joi.string().required(),
                                company_id: Joi.string().required(),
                                name: Joi.string().required(),
                                cpf: Joi.string().required(),
                                email: Joi.string().email().required(),
                                avatar: Joi.string().required(),
                                date_birth: Joi.string().required(),
                                status: Joi.number().required(),
                                phone: Joi.string().required(),
                                gender: Joi.string().required(),
                                race: Joi.string().required(),
                                pis_nit: Joi.string().required(),
                                observation: Joi.string().allow(''),
                                role: Joi.string().required(),
                                rfid: Joi.string().allow(''),
                                created_at: Joi.date(),
                                updated_at: Joi.date(),
                                CompanyId: Joi.string().required()
                            })
                        }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await workerController.info(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insert = {
            path: this.path,
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para cadastrar um colaborador",
                notes: "Consome WorkerService",
                payload: {
                    output: 'stream',
                    parse: true,
                    allow: 'multipart/form-data',
                    maxBytes: 1024 * 1024 * 100,
                    multipart: true
                },
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    payload: Joi.object({
                        company_id: Joi.string().required(),
                        name: Joi.string().required(),
                        cpf: Joi.string().trim().required(),
                        email: Joi.string().email().required(),
                        gender: Joi.string().required(),
                        race: Joi.string().required(),
                        pis_nit: Joi.string().required(),
                        date_birth: Joi.string().required(),
                        phone: Joi.string().required(),
                        avatar: Joi.any()
                            .meta({ swaggerType: 'file' })
                            .optional()
                            .allow('')
                            .description('image file'),
                        status: Joi.number().required(),
                        observation: Joi.string().optional().allow(''),
                        rfid: Joi.string().optional().allow(''),
                        role: Joi.string().required(),
                    }),
                },
                response: {
                    schema:
                        Joi.object({
                            message: Joi.string(),
                            data: Joi.object({
                                id: Joi.string().required(),
                                company_id: Joi.string().required(),
                                name: Joi.string().required(),
                                cpf: Joi.string().required(),
                                email: Joi.string().email().required(),
                                gender: Joi.string().required(),
                                race: Joi.string().required(),
                                pis_nit: Joi.string().required(),
                                date_birth: Joi.string().required(),
                                phone: Joi.string().required(),
                                avatar: Joi.string().required(),
                                status: Joi.number().required(),
                                observation: Joi.string().allow(''),
                                rfid: Joi.string().allow(''),
                                role: Joi.string().required(),
                                created_at: Joi.date(),
                                updated_at: Joi.date(),
                                CompanyId: Joi.string()
                            })
                        }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await workerController.insert(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.update = {
            path: this.path + '/{cpf}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para atualizar informações de um colaborador",
                notes: "Consome WorkerServices",
                payload: {
                    output: 'stream',
                    parse: true,
                    allow: 'multipart/form-data',
                    maxBytes: 1024 * 1024 * 100,
                    multipart: true
                },
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cpf: Joi.string().trim().required()
                    }),
                    payload: Joi.object({
                        company_id: Joi.string().allow(''),
                        name: Joi.string().allow(''),
                        email: Joi.string().email().allow(''),
                        gender: Joi.string().allow(''),
                        race: Joi.string().allow(''),
                        date_birth: Joi.string().allow(''),
                        phone: Joi.string().allow(''),
                        avatar: Joi.any()
                            .meta({ swaggerType: 'file' })
                            .optional()
                            .allow('')
                            .description('image file'),
                        status: Joi.string().allow(''),
                        role: Joi.string().allow(''),
                        observation: Joi.string().allow(''),
                        rfid: Joi.string().allow('')
                    }),
                },
                response: {
                    schema:
                        Joi.object({
                            message: Joi.string(),
                            data: Joi.object({
                                id: Joi.string().required(),
                                name: Joi.string().required(),
                                cpf: Joi.string().required(),
                                email: Joi.string().email().required(),
                                avatar: Joi.string().required(),
                                date_birth: Joi.string().required(),
                                status: Joi.number().required(),
                                phone: Joi.string().required(),
                                gender: Joi.string().required(),
                                race: Joi.string().required(),
                                pis_nit: Joi.string().required(),
                                observation: Joi.string().allow(''),
                                role: Joi.string().required(),
                                rfid: Joi.string().allow(''),
                                created_at: Joi.date(),
                                updated_at: Joi.date(),
                                CompanyId: Joi.string().required()
                            })
                        }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await workerController.update(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.delete = {
            path: this.path + '/{cpf}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover um colaborador",
                notes: "Consome WorkerService",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cpf: Joi.string().trim().required()
                    }),
                },
                response: {
                    schema:
                        Joi.object({
                            message: Joi.string(),
                            data: Joi.any()
                        }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await workerController.delete(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.getAvatar = {
            path: this.path + '/picture/{file*}',
            method: 'GET',
            options: {
                auth: false,
                tags: ['api'],
                description: "Rota para visualizar a imagem de perfil de um colaborador",
                notes: "Busca a imagem no diretório de upload",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown()
                }
            },
            handler: {
                directory: {
                    path: config.Data.server.uploadPath
                }
            }
        }
    }

    /**
     * Retorna as rotas aqui configuradas
     */
    getRoutes() {
        return [
            this.index,
            this.insert,
            this.delete,
            this.info,
            this.update,
            this.getAvatar
        ]
    }
}