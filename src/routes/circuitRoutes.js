'use strict'

/**
 * Classe responsável por configurar as rotas para serviços de circuitos
 */
module.exports = class CircuitRoutes {
    constructor(joi, circuitController, config) {
        this.joi = joi
        this.circuitController = circuitController
        this.config = config
        this.path = '/circuit'

        this.index = {
            path: this.path,
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma lista de circuitos",
                notes: "Consome CircuitServices",
                validate: {
                    query: this.joi.object({
                        company_id: this.joi.string().default(''),
                        expandCompany: this.joi.boolean().default(false),
                        expandSchedulings: this.joi.boolean().default(false),
                        date_start: this.joi.date().required(),
                        date_finish: this.joi.date().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown()
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.array().items({
                            id: this.joi.string().required(),
                            name: this.joi.string().required(),
                            company_id: this.joi.string().required(),
                            start: this.joi.date().required(),
                            finish: this.joi.date().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            company: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required(),
                                cnpj: this.joi.string().required()
                            }),
                            schedulings: this.joi.array().items({
                                id: this.joi.string().required(),
                                equipment_id: this.joi.string().required(),
                                mac: this.joi.string().required(),
                                code: this.joi.string().required(),
                                status: this.joi.number().required(),
                                model: this.joi.string().required(),
                                maker: this.joi.string().required(),
                                start: this.joi.date().required(),
                                finish: this.joi.date().required()
                            })
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await circuitController.index(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.info = {
            path: this.path + '/{id}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir um circuito e seus agendamentos",
                notes: "Consome CircuitService",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    query: this.joi.object({
                        expandSchedulings: this.joi.boolean().default(false)
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown()
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            name: this.joi.string().required(),
                            company_id: this.joi.string().required(),
                            start: this.joi.date().required(),
                            finish: this.joi.date().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            company: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required(),
                                cnpj: this.joi.string().required()
                            }),
                            schedulings: this.joi.array().items({
                                id: this.joi.string().required(),
                                equipment_id: this.joi.string().required(),
                                mac: this.joi.string().required(),
                                code: this.joi.string().required(),
                                status: this.joi.number().required(),
                                model: this.joi.string().required(),
                                maker: this.joi.string().required(),
                                start: this.joi.date().required(),
                                finish: this.joi.date().required()
                            })
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await circuitController.info(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insert = {
            path: this.path,
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para inserir um novo circuito e/ou agendamentos de equipamentos",
                notes: "Consome CircuitService",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        id: this.joi.string(),
                        name: this.joi.string(),
                        start: this.joi.date(),
                        finish: this.joi.date(),
                        company_id: this.joi.string(),
                        schedulings: this.joi.array().items({
                            equipment_id: this.joi.string().required(),
                            start: this.joi.string().required(),
                            finish: this.joi.string().required()
                        })
                    }).and('name', 'start', 'finish', 'company_id').xor('id', 'name'),
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            name: this.joi.string().required(),
                            company_id: this.joi.string().required(),
                            start: this.joi.date().required(),
                            finish: this.joi.date().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            company: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required(),
                                cnpj: this.joi.string().required()
                            }),
                            schedulings: this.joi.array().items({
                                id: this.joi.string().required(),
                                equipment_id: this.joi.string().required(),
                                mac: this.joi.string().required(),
                                code: this.joi.string().required(),
                                status: this.joi.number().required(),
                                model: this.joi.string().required(),
                                maker: this.joi.string().required(),
                                start: this.joi.date().required(),
                                finish: this.joi.date().required()
                            })
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await circuitController.insert(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.update = {
            path: this.path + '/{id}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para atualizar um circuito",
                notes: "Consome CircuitService",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        name: this.joi.string(),
                        company_id: this.joi.string()
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            name: this.joi.string().required(),
                            company_id: this.joi.string().required(),
                            start: this.joi.date().required(),
                            finish: this.joi.date().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            company: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required(),
                                cnpj: this.joi.string().required()
                            }),
                            schedulings: this.joi.array().items({
                                id: this.joi.string().required(),
                                equipment_id: this.joi.string().required(),
                                mac: this.joi.string().required(),
                                code: this.joi.string().required(),
                                status: this.joi.number().required(),
                                model: this.joi.string().required(),
                                maker: this.joi.string().required(),
                                start: this.joi.date().required(),
                                finish: this.joi.date().required()
                            })
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await circuitController.update(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.delete = {
            path: this.path + '/{id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover um circuito do banco de dados",
                notes: "Consome CircuitService",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await circuitController.delete(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.deleteScheduling = {
            path: this.path + '/scheduling/{scheduling_id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para desmarcar um agendamento de equipamento para um circuito do banco de dados",
                notes: "Consome CircuitService",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    params: this.joi.object({
                        scheduling_id: this.joi.string().trim().required()
                    }),
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await circuitController.deleteScheduling(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }
    }

    /**
     * Retorna as rotas aqui configuradas
     */
    getRoutes() {
        return [
            this.index, this.info, this.insert, this.update, this.delete, this.deleteScheduling
        ];
    }
}