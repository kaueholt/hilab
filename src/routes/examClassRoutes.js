'use strict'

/**
 * Classe responsável por configurar as rotas de classes de exames
 */
module.exports = class ExamRoutes {
    constructor(joi, examController, config) {
        this.joi = joi
        this.examController = examController
        this.config = config
        this.path = '/exam-class'

        this.index = {
            path: this.path,
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma lista de tipos de exames",
                notes: "Consome ExamServices",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown()
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.array().items({
                            id: this.joi.string().required(),
                            name: this.joi.string().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date()
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await examController.indexExamClass(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.info = {
            path: this.path + '/{id}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir um tipo de exame",
                notes: "Consome ExamServices",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            name: this.joi.string().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await examController.infoExamClass(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insert = {
            path: this.path,
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para inserir um novo tipo de exame",
                notes: "Consome ExamService",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        name: this.joi.string()
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            name: this.joi.string().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date()
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await examController.insertExamClass(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.delete = {
            path: this.path + '/{id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover um tipo de exame do banco de dados",
                notes: "Consome ExamService",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await examController.deleteExamClass(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.update = {
            path: this.path + '/{id}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para atualizar um tipo de exame",
                notes: "Consome ExamService",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        name: this.joi.string()
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            name: this.joi.string().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date()
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await examController.updateExamClass(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }
    }

    /**
     * Retorna as rotas aqui configuradas
     */
    getRoutes() {
        return [
            this.index, this.insert, this.delete, this.update, this.info
        ];
    }
}
