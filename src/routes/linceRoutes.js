'use strict'
const Joi = require('@hapi/joi');

/**
 * Classe responsável por configurar as rotas para serviços de perfís
 */
module.exports = class LinceRoutes {
    constructor(joi, linceProfileController, config) {
        this.joi = joi
        this.linceProfileController = linceProfileController
        this.config = config
        this.path = '/profiles'

        this.index = {
            path: this.path,
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma lista de perfís",
                notes: "Consome LinceProfileServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    query: Joi.object({
                        description: Joi.string().default('')
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.array().items({
                            id: Joi.string().required(),
                            description: Joi.string().required(),
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await linceProfileController.index(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.info = {
            path: this.path + '/{id}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir informações de um perfil",
                notes: "Consome LinceProfileServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        id: Joi.string().trim().required()
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            description: Joi.string().required()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await linceProfileController.info(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insert = {
            path: this.path,
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para inserir um perfil",
                notes: "Consome LinceProfileServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    payload: Joi.object().keys({
                        description: Joi.string().trim().required()
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            description: Joi.string().required(),
                            created_at: Joi.date(),
                            updated_at: Joi.date()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await linceProfileController.insert(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.delete = {
            path: this.path + '/{id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover um perfil do banco de dados",
                notes: "Consome LinceProfileServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        id: Joi.string().trim().required()
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await linceProfileController.delete(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.update = {
            path: this.path + '/{id}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para atualizar informações de um perfil",
                notes: "Consome LinceProfileServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cnpj: Joi.string().trim().required()
                    }),
                    payload: Joi.object().keys({
                        description: Joi.string().trim().allow('')
                    })
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            description: Joi.string().required()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await linceProfileController.update(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }
    }
    /**
     * Retorna as rotas aqui configuradas
     */
    getRoutes() {
        return [
            this.index, this.insert, this.info, this.update, this.delete
        ];
    }
}