'use strict'

/**
 * Classe responsável por configurar as rotas para serviços de exames
 */
module.exports = class ExamRoutes {
    constructor(joi, examController, config) {
        this.joi = joi
        this.examController = examController
        this.config = config
        this.path = '/exam'

        this.index = {
            path: this.path,
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma lista de exames",
                notes: "Consome ExamServices",
                validate: {
                    query: this.joi.object({
                        circuit_id: this.joi.string().default(''),
                        worker_id: this.joi.string().default(''),
                        expandWorker: this.joi.boolean().default(false),
                        date_start: this.joi.date().required(),
                        date_finish: this.joi.date().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown()
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.array().items({
                            id: this.joi.string().required(),
                            circuit_id: this.joi.string().required(),
                            worker_id: this.joi.string().required(),
                            start: this.joi.date().required(),
                            finish: this.joi.date().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            worker: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required(),
                                cpf: this.joi.string().required(),
                                email: this.joi.string().required(),
                                gender: this.joi.string().required(),
                                phone: this.joi.string().required(),
                                pis_nit: this.joi.string().required(),
                                date_birth: this.joi.date()
                            }),
                            circuit: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required(),
                                start: this.joi.date().required(),
                                finish: this.joi.date().required()
                            })
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await examController.index(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.info = {
            path: this.path + '/{id}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir um exame",
                notes: "Consome ExamService",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown()
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            circuit_id: this.joi.string().required(),
                            worker_id: this.joi.string().required(),
                            start: this.joi.date().required(),
                            finish: this.joi.date().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            worker: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required(),
                                cpf: this.joi.string().required(),
                                email: this.joi.string().required(),
                                gender: this.joi.string().required(),
                                phone: this.joi.string().required(),
                                pis_nit: this.joi.string().required(),
                                date_birth: this.joi.date()
                            }),
                            circuit: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required(),
                                start: this.joi.date().required(),
                                finish: this.joi.date().required()
                            })
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await examController.info(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insert = {
            path: this.path,
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para inserir um novo exame",
                notes: "Consome ExamService",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        start: this.joi.date(),
                        finish: this.joi.date(),
                        circuit_id: this.joi.string(),
                        worker_id: this.joi.string()
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            circuit_id: this.joi.string().required(),
                            worker_id: this.joi.string().required(),
                            start: this.joi.date().required(),
                            finish: this.joi.date().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            worker: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required(),
                                cpf: this.joi.string().required(),
                                email: this.joi.string().required(),
                                gender: this.joi.string().required(),
                                phone: this.joi.string().required(),
                                pis_nit: this.joi.string().required(),
                                date_birth: this.joi.date()
                            }),
                            circuit: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required(),
                                start: this.joi.date().required(),
                                finish: this.joi.date().required()
                            })
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await examController.insert(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.delete = {
            path: this.path + '/{id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover um exame do banco de dados",
                notes: "Consome ExamService",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await examController.delete(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }
    }

    /**
     * Retorna as rotas aqui configuradas
     */
    getRoutes() {
        return [
            this.index, this.info, this.insert, this.delete
        ];
    }
}
