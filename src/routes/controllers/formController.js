'use strict'

/**
 * Classe controladora das operações de formulários que as rotas solicitam
 */
module.exports = class FormController {
    /**
     * Construtor da classe
     * @param {FacadeManager} facadeManager 
     * @param {ReplyManager} replyManager 
     */
    constructor(facadeManager, replyManager) {
        this.facadeManager = facadeManager
        this.replyManager = replyManager
    }

    /**
     * Atribui os dados recebidos da requisição à um objeto e
     * através do facadeManager inserir um registro no banco de dados
     * @param {*} req - dados da requisição, como body, params e headers
     * @param {*} res - dados montados para a resposta
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async insert(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            payload: req.payload
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.insertForm, data, true)
            .then(result => this.replyManager.handleSuccess('Formulário inserido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager consultar lista de registros no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async index(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: {
                exam_class_id: req.query.exam_class_id,
                equipment_model_id: req.query.equipment_model_id,
                name: req.query.name,
                status: req.query.status
            }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getForms, data, true)
            .then(result => this.replyManager.handleSuccess('Lista obtida com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager consultar um registro no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async info(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id, topics: req.query.topics || false }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getForm, data, true)
            .then(result => this.replyManager.handleSuccess('Formulário obtido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Consome um service para remover um registro do banco de dados
    * @param {Object} req  - objeto da requisição
    * @param {Object} res - objeto da resposta
    * @returns {Object} - mensagem de sucesso
    */
    async delete(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.deleteForm, data, true)
            .then(result => this.replyManager.handleSuccess(`Formulário excluído com sucesso.`,
                { message: 'Registros removidos: ' + result })
            ).catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
     * Atribui os dados recebidos da requisição à um objeto e
     * através do facadeManager atualizar um registro no banco de dados
     * @param {*} req - dados da requisição, como body, params e headers
     * @param {*} res - dados montados para a resposta
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async update(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id },
            payload: req.payload
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.updateForm, data, true)
            .then(result => this.replyManager.handleSuccess('Formulário atualizado com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
     * Atribui os dados recebidos da requisição à um objeto e
     * através do facadeManager inserir um registro no banco de dados
     * @param {*} req - dados da requisição, como body, params e headers
     * @param {*} res - dados montados para a resposta
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async insertTopic(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            payload: req.payload
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.insertTopic, data, true)
            .then(result => this.replyManager.handleSuccess('Tópico inserido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager consultar um registro no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async infoTopic(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id, questions: req.query.topics || false }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getTopic, data, true)
            .then(result => this.replyManager.handleSuccess('Tópico obtido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager consultar uma lista de registros no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async indexTopics(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: {
                formulary_id: req.query.formulary_id,
                description: req.query.description,
            }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getTopics, data, true)
            .then(result => this.replyManager.handleSuccess('Lista de tópicos obtida com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager deletar um registro no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async deleteTopic(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.deleteTopic, data, true)
            .then(result => {
                return this.replyManager.handleSuccess(`Tópico excluído com sucesso.`,
                    { message: 'Registros removidos: ' + result })
            })
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager atualizar um registro no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async updateTopic(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id },
            payload: req.payload,
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.updateTopic, data, true)
            .then(result => this.replyManager.handleSuccess('Tópico atualizado com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
     * Atribui os dados recebidos da requisição à um objeto e
     * através do facadeManager inserir um registro no banco de dados
     * @param {*} req - dados da requisição, como body, params e headers
     * @param {*} res - dados montados para a resposta
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async insertQuestion(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            payload: req.payload
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.insertQuestion, data, true)
            .then(result => this.replyManager.handleSuccess('Campo inserido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager consultar um registro no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async infoQuestion(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getQuestion, data, true)
            .then(result => this.replyManager.handleSuccess('Campo obtido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager consultar uma lista de registros no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async indexQuestions(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: {
                formulary_id: req.query.equipment_model_id || '',
                topic_form_id: req.query.topic_form_id || '',
            }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getQuestions, data, true)
            .then(result => this.replyManager.handleSuccess('Lista de campos obtida com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager deletar um registro no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async deleteQuestion(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.deleteQuestion, data, true)
            .then(result => {
                return this.replyManager.handleSuccess(`Campo excluído com sucesso.`,
                    { message: 'Registros removidos: ' + result })
            })
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager atualizar um registro no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async updateQuestion(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id },
            payload: req.payload
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.updateQuestion, data, true)
            .then(result => this.replyManager.handleSuccess('Campo atualizado com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
     * Atribui os dados recebidos da requisição à um objeto e
     * através do facadeManager inserir um registro no banco de dados
     * @param {*} req - dados da requisição, como body, params e headers
     * @param {*} res - dados montados para a resposta
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async insertQuestionField(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            payload: req.payload
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.insertQuestionField, data, true)
            .then(result => this.replyManager.handleSuccess('Item inserido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager consultar um registro no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async infoQuestionField(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getQuestionItem, data, true)
            .then(result => this.replyManager.handleSuccess('Item obtido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager consultar uma lista de registros no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async indexQuestionFields(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: {
                formulary_id: req.query.formulary_id,
                topic_form_id: req.query.topic_form_id,
                question_id: req.query.question_id,
            }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getQuestionFields, data, true)
            .then(result => this.replyManager.handleSuccess('Lista de itens obtida com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager deletar um registro no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async deleteQuestionField(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id }
        }

        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.deleteQuestionField, data, true)
            .then(result => {
                return this.replyManager.handleSuccess(`Item excluído com sucesso.`,
                    { message: 'Registros removidos: ' + result })
            })
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * através do facadeManager atualizar um registro no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async updateQuestionField(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id },
            payload: req.payload
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.updateQuestionField, data, true)
            .then(result => this.replyManager.handleSuccess('Item atualizado com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

}
