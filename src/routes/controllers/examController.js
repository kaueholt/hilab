'use strict'

/**
 * Classe controladora das operações de exames que as rotas soliciam
 */
module.exports = class ExamController {
    /**
     * Construtor da classe
     * @param {FacadeManager} facadeManager 
     * @param {ReplyManager} replyManager 
     */
    constructor(facadeManager, replyManager) {
        this.facadeManager = facadeManager
        this.replyManager = replyManager
    }

    /**
     * Atribui os dados recebidos da requisição à um objeto e
     * consome o serviço de colaboradores através do facadeManager para
     * inserir um registro no banco de dados
     * @param {*} req - dados da requisição, como body, params e headers
     * @param {*} res - dados montados para a resposta
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async insert(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            payload: req.payload
        }

        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.insertExam, data, true)
            .then(result => this.replyManager.handleSuccess('Exame inserido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * consome o serviço de exames através do facadeManager para
    * adquirir uma lista de registros
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros de exames
    */
    async index(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: {
                circuit_id: req.query.circuit_id,
                worker_id: req.query.worker_id,
                expandWorker: req.query.expandWorker || false,
                date_start: req.query.date_start || new Date().getTime(),
                date_finish: req.query.date_finish || new Date().getTime(),
            }
        }

        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getExams, data, true)
            .then(result => {
                return this.replyManager.handleSuccess('Lista obtida com sucesso!', result)
            })
            .catch(error => {
                this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error)
            })
    }

    /**
     * Consome o serviço ExamService para remover um registro do banco de dados
     * @param {Object} req  - objeto da requisição
     * @param {Object} res - objeto da resposta
     * @returns {Object} - mensagem de sucesso
     */
    async delete(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: {
                id: req.params.id
            }
        }

        return await this.facadeManager.action(this.facadeManager.ActionEnums.deleteExam, data, true)
            .then(result => {
                return this.replyManager.handleSuccess(`Exame excluído com sucesso.`,
                    { message: 'Registros removidos: ' + result })
            })
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
     * Consome ExamService para adquirir os dados de um registro
     * que possui o UUID informado nos parâmetros
     * @param {Object} req - Objeto da requisição
     * @param {Object} res - Objeto da resposta
     * @returns {Object} - dados do colaborador
     */
    async info(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: {
                id: req.params.id
            }
        }

        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getExam, data, true)
            .then(result => this.replyManager.handleSuccess(`Exame encontrado!`, result))
            .catch(error => {
                this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error)
            })
    }

    /**
     * Atribui os dados recebidos da requisição à um objeto e
     * através do facadeManager inserir um registro no banco de dados
     * @param {*} req - dados da requisição, como body, params e headers
     * @param {*} res - dados montados para a resposta
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async insertExamClass(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            payload: req.payload
        }

        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.insertExamClass, data, true)
            .then(result => this.replyManager.handleSuccess('Tipo de exame inserido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * consome um serviço através do facadeManager
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros
    */
    async infoExamClass(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id }
        }
        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.infoExamClass, data, true)
            .then(result => this.replyManager.handleSuccess('Tipo de exame obtido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
     * Atribui os dados recebidos da requisição à um objeto e
     * através do facadeManager atualiar um registro no banco de dados
     * @param {*} req - dados da requisição, como body, params e headers
     * @param {*} res - dados montados para a resposta
     * @returns {Object} - dados do registro que foi inserido no banco
     */
    async updateExamClass(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: { id: req.params.id },
            payload: req.payload
        }

        data.payload.id = data.query.id

        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.updateExamClass, data, true)
            .then(result => this.replyManager.handleSuccess('Tipo de exame atualizado com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * consome o serviço de exames através do facadeManager para
    * adquirir uma lista de registros
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros de exames
    */
    async indexExamClass(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: req.query
        }

        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getExamClass, data, true)
            .then(result => this.replyManager.handleSuccess('Lista obtida com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
     * Consome o serviço ExamService para remover um registro do banco de dados
     * @param {Object} req  - objeto da requisição
     * @param {Object} res - objeto da resposta
     * @returns {Object} - mensagem de sucesso
     */
    async deleteExamClass(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: {
                id: req.params.id
            }
        }

        return await this.facadeManager.action(this.facadeManager.ActionEnums.deleteExamClass, data, true)
            .then(result => {
                return this.replyManager.handleSuccess(`Tipo de exame excluído com sucesso.`,
                    { message: 'Registros removidos: ' + result })
            })
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }
}
