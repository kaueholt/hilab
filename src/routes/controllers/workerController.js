'use strict'

/**
 * Classe controladora das operações de colaboradores que as rotas soliciam
 */
module.exports = class WorkerController {
  /**
   * Construtor da classe
   * @param {FacadeManager} facadeManager 
   * @param {ReplyManager} replyManager 
   * @param {ValidationUtils} validationUtils 
   */
  constructor(facadeManager, replyManager, validationUtils) {
    this.facadeManager = facadeManager
    this.replyManager = replyManager
    this.validationUtils = validationUtils
  }

  /**
  * Atribui os dados recebidos da requisição à um objeto e
  * consome o serviço de colaboradores através do facadeManager para
  * adquirir uma lista de colaboradores
  * @param {*} req - dados da requisição, como body, params e headers
  * @param {*} res - dados montados para a resposta
  * @returns {Array} - lista de registros de usuários
  */
  async index(req, res) {
    let data = {
      headers: req.headers,
      query: req.query,
      auth: req.auth,
      search: req.params.company_id
    }

    return await this.facadeManager.action(this.facadeManager.ActionEnums.getWorkers, data, true)
      .then(result => this.replyManager.handleSuccess(`Lista encontrada com sucesso!`, result))
      .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
  }

  /**
   * Consome o serviço WorkerService para adquirir os dados do colaborador
   * que possui o CPF informado nos parâmetros
   * @param {Object} req - Objeto da requisição
   * @param {Object} res - Objeto da resposta
   * @returns {Object} - dados do colaborador
   */
  async info(req, res) {
    let data = {
      headers: req.headers,
      search: req.params.cpf,
      auth: req.auth,
      query: req.query
    }

    return await this.facadeManager
      .action(this.facadeManager.ActionEnums.getWorker, data, true)
      .then(result => this.replyManager.handleSuccess('Usuário encontrado!', result))
      .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))

  }

  /**
   * Atribui os dados recebidos da requisição à um objeto e
   * consome o serviço de colaboradores através do facadeManager para
   * inserir um colaborador na banco de dados
   * @param {*} req - dados da requisição, como body, params e headers
   * @param {*} res - dados montados para a resposta
   * @returns {Object} - dados do colaborador que foi inserido no banco
   */
  async insert(req, res) {
    let data = {
      headers: req.headers,
      auth: req.auth,
      payload: {
        name: req.payload.name,
        cpf: req.payload.cpf.replace(/[^\w\s]/gi, ''),
        gender: req.payload.gender,
        race: req.payload.race,
        pis_nit: req.payload.pis_nit.replace(/[^\w\s]/gi, ''),
        email: req.payload.email,
        phone: req.payload.phone.replace(/[^\w\s]/gi, ''),
        date_birth: req.payload.date_birth,
        status: Number(req.payload.status),
        avatar: req.payload.avatar || '',
        company_id: req.payload.company_id,
        rfid: req.payload.rfid,
        observation: req.payload.observation,
        role: req.payload.role
      }
    }

    let isCpf = this.validationUtils.isCPF(data.payload.cpf)
    let isPis = this.validationUtils.isPIS(data.payload.pis_nit)

    !isCpf && this.replyManager.handleError(this.replyManager.statusEnum.badRequest, new Error('CPF inválido!'))
    !isPis && this.replyManager.handleError(this.replyManager.statusEnum.badRequest, new Error('PIS/NIT inválido!'))

    data.payload.avatar !== undefined ? data.payload.avatar : data.payload.avatar = ''

    return await this.facadeManager.action(this.facadeManager.ActionEnums.insertWorker, data, true)
      .then(result => this.replyManager.handleSuccess(`Colaborador cadastrado com sucesso!`, result))
      .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
  }

  /**
   * Consome o serviço WorkerService para atualizar um registro de um 
   * colaborador no banco de dados
   * @param {*} req - objeto da requisição
   * @param {*} res - objeto da resposta
   * @returns {Object} - colaborador com os dados atualizados
   */
  async update(req, res) {
    let data = {
      headers: req.headers,
      search: req.params.cpf,
      auth: req.auth,
      payload: req.payload
    }

    return this.facadeManager.action(this.facadeManager.ActionEnums.updateWorker, data, true)
      .then(result => this.replyManager.handleSuccess(`Colaborador atualizado com sucesso!`, result))
      .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
  }

  /**
   * Consome o serviço WorkerService para remover um colaborador do banco de dados
   * e deletar sua imagem do diretório de uploads
   * @param {Object} req  - objeto da requisição
   * @param {Object} res - objeto da resposta
   * @returns {Object} - mensagem de sucesso
   */
  async delete(req, res) {
    let data = {
      headers: req.headers,
      search: req.params.cpf,
      auth: req.auth
    }

    return await this.facadeManager.action(this.facadeManager.ActionEnums.deleteWorker, data, true)
      .then(result => this.replyManager.handleSuccess(`Colaborador excluído com sucesso.`, result))
      .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
  }
}
