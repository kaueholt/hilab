'use strict'

/**
 * Classe controladora das operações com empresas
 */
module.exports = class CompanyController {
   /**
    * Construtor da classe
    * @param {FacadeManager} facadeManager 
    * @param {ReplyManager} replyManager 
    */
   constructor(facadeManager, replyManager, validationUtils) {
      this.facadeManager = facadeManager
      this.replyManager = replyManager
      this.validationUtils = validationUtils
   }

   /**
    * Atribui os dados recebidos da requisição à um objeto e
    * consome o serviço de empresas para adquirir uma lista de empresas
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Object} - objecto com uma mensagem e a lista de registros de empresas
    */
   async index(req, res) {
      let data = {
         headers: req.headers,
         query: req.query,
         auth: req.auth
      }

      return await this.facadeManager.action(this.facadeManager.ActionEnums.getCompanies, data, true)
         .then(result => this.replyManager.handleSuccess('Lista de empresas encontrada!', result))
         .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
   }

   /**
    * Consome o serviço de empresas para adquirir os dados da empresa
    * que possui o CNPJ infrmado nos parâmetros
    * @param {Object} req - Objeto da requisição
    * @param {Object} res - Objeto da resposta
    * @returns {Object} - mensagen de sucesso e os dados da empresa
    */
   async info(req, res) {
      let data = {
         headers: req.headers,
         search: req.params.cnpj,
         auth: req.auth,
         query: req.query
      }

      return await this.facadeManager.action(this.facadeManager.ActionEnums.getCompany, data, true)
         .then(result => this.replyManager.handleSuccess('Empresa encontrada!', result))
         .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
   }

   /**
    * Atribui os dados recebidos da requisição à um objeto e
    * consome o serviço de empresas para inserir um novo registro no banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Object} - mensagem de sucesso e dados da empresa que foi inserida no banco
    */
   async insert(req, res) {
      let data = {
         headers: req.headers,
         auth: req.auth,
         query: req.query,
         payload: {
            cnpj: req.payload.cnpj.replace(/[^\w\s]/gi, ''),
            name: req.payload.name,
            email: req.payload.email,
            phone: req.payload.phone.replace(/[^\w\s]/gi, ''),
            foundation_date: req.payload.foundation_date,
            status: req.payload.status,
            address: {
               street: req.payload.address.street,
               number: req.payload.address.number,
               complement: req.payload.address.complement,
               district: req.payload.address.district,
               city: req.payload.address.city,
               state: req.payload.address.state,
               zipcode: req.payload.address.zipcode.replace(/[^\w\s]/gi, '')
            }
         }
      }

      let isCnpj = this.validationUtils.isCNPJ(data.payload.cnpj)
      !isCnpj && this.replyManager.handleError(this.replyManager.statusEnum.badRequest, new Error('CNPJ inválido!'))

      return this.facadeManager.action(this.facadeManager.ActionEnums.insertCompany, data, true)
         .then(result => this.replyManager.handleSuccess('Empresa cadastrada com sucesso!', result))
         .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
   }

   /**
    * Consome o serviço de empresas para atualizar um registro de uma
    * empresa no banco de dados
    * @param {*} req - objeto da requisição
    * @param {*} res - objeto da resposta
    * @returns {Object} - mensagem de sucesso e a empresa com os dados atualizados
    */
   async update(req, res) {
      let data = {
         headers: req.headers,
         search: req.params.cnpj,
         auth: req.auth,
         payload: req.payload
      }

      return await this.facadeManager.action(this.facadeManager.ActionEnums.updateCompany, data, true)
         .then(result => this.replyManager.handleSuccess(`Empresa atualizada com sucesso.`, result))
         .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
   }

   /**
    * Consome o serviço de empresa para remover um registro de empresa 
    * do banco de dados
    * @param {Object} req - objeto da requisição
    * @param {Object} res - objeto da resposta
    * @returns {Object} - mensagem de sucesso
    */
   async delete(req, res) {
      let data = {
         headers: req.headers,
         search: req.params.cnpj,
         auth: req.auth
      }

      return await this.facadeManager.action(this.facadeManager.ActionEnums.deleteCompany, data, true)
         .then(result => this.replyManager.handleSuccess(`Empresa excluída com sucesso.`, result))
         .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
   }
}