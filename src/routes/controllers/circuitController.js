'use strict'

/**
 * Classe controladora das operações de circuitos que as rotas soliciam
 */
module.exports = class CircuitController {
    /**
     * Construtor da classe
     * @param {FacadeManager} facadeManager 
     * @param {ReplyManager} replyManager 
     */
    constructor(facadeManager, replyManager) {
        this.facadeManager = facadeManager
        this.replyManager = replyManager
    }

    /**
     * Atribui os dados recebidos da requisição à um objeto e
     * consome o serviço de circuito através do facadeManager para
     * inserir um circuito na banco de dados
     * @param {*} req - dados da requisição, como body, params e headers
     * @param {*} res - dados montados para a resposta
     * @returns {Object} - dados do circuito que foi inserido no banco
     */
    async insert(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            payload: req.payload
        }

        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.insertCircuit, data, true)
            .then(result => this.replyManager.handleSuccess('Circuito inserido com sucesso!', result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * consome o serviço de circuitos através do facadeManager para
    * adquirir uma lista de circuitos
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Array} - lista de registros de circuitos
    */
    async index(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: {
                company_id: req.query.company_id,
                expandCompany: req.query.expandCompany,
                expandSchedulings: req.query.expandSchedulings,
                date_start: req.query.date_start || new Date().getTime(),
                date_finish: req.query.date_finish || new Date().getTime(),
            }
        }

        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getCircuits, data, true)
            .then(result => {
                return this.replyManager.handleSuccess('Lista obtida com sucesso!', result)
            })
            .catch(error => {
                this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error)
            })
    }

    /**
     * Consome o serviço CircuitService para remover um circuito do banco de dados
     * @param {Object} req  - objeto da requisição
     * @param {Object} res - objeto da resposta
     * @returns {Object} - mensagem de sucesso
     */
    async delete(req, res) {
        let data = { 
            query: {
                id: req.params.id
            },
            auth: req.auth
        }

        return await this.facadeManager.action(this.facadeManager.ActionEnums.deleteCircuit, data, true)
            .then(result => {
                return this.replyManager.handleSuccess(`Circuito excluído com sucesso.`,
                    { message: 'Registros removidos: ' + result })
            })
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
     * Consome o serviço CircuitService para remover um agendamento de equipamento
     * do banco de dados
     * @param {Object} req  - objeto da requisição
     * @param {Object} res - objeto da resposta
     * @returns {Object} - mensagem de sucesso
     */
    async deleteScheduling(req, res) {
        let data = { 
            query: {
                scheduling_id: req.params.scheduling_id
            },
            auth: req.auth
        }

        return await this.facadeManager.action(this.facadeManager.ActionEnums.deleteScheduling, data, true)
            .then(result => {
                return this.replyManager.handleSuccess(`Agendamento excluído com sucesso.`,
                    { message: 'Registros removidos: ' + result })
            })
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
     * Consome CircuitService para adquirir os dados de um circuito
     * que possui o UUID informado nos parâmetros
     * @param {Object} req - Objeto da requisição
     * @param {Object} res - Objeto da resposta
     * @returns {Object} - dados do circuito
     */
    async info(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            query: {
                id: req.params.id,
                expandSchedulings: req.query.expandSchedulings
            }
        }

        return await this.facadeManager
            .action(this.facadeManager.ActionEnums.getCircuit, data, true)
            .then(result => this.replyManager.handleSuccess(`Circuito encontrado!`, result))
            .catch(error => {
                this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error)
            })
    }

    /**
     * Consome o serviço CircuitService para atualizar um registro de um 
     * circuito ou agendamento no banco de dados
     * @param {*} req - objeto da requisição
     * @param {*} res - objeto da resposta
     * @returns {Object} - circuito com os dados atualizados
     */
    async update(req, res) {
        let data = {
            headers: req.headers,
            search: req.payload.company_id,
            payload: req.payload,
            query: {
                addresses: false
            },
            auth: req.auth
        }

        return await this.facadeManager.action(this.facadeManager.ActionEnums.getCompany, data, true)
            .then(async resultCompany => {

                let circuitData = {
                    payload: {
                        id: req.params.id,
                        company_id: resultCompany.id,
                        name: data.payload.name
                    },
                    headers: data.headers,
                    auth: req.auth
                }

                return await this.facadeManager.action(
                    this.facadeManager.ActionEnums.updateCircuit, circuitData, true)
                    .then(resultCircuit => {
                        return this.replyManager.handleSuccess("Circuito atualizado com sucesso!", resultCircuit)
                    })
                    .catch(error => {
                        throw error
                    })
            })
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }
}
