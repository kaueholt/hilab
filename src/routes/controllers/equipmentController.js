'use strict'

/**
 * Classe controladora das operações com
 * modelos de equipamentos e patrimônios de equipamentos
 */
module.exports = class EquipmentController {
	/**
	 * Construtor da classe
	 * @param {FacadeManager} facadeManager 
	 * @param {ReplyManager} replyManager 
	 */
	constructor(facadeManager, replyManager) {
		this.facadeManager = facadeManager
		this.replyManager = replyManager
	}

	/**
	 * Atribui os dados recebidos da requisição à um objeto e
	 * consome o serviço de equipmentModels para adquirir uma lista de modelos de equipamentos
	 * @param {*} req - dados da requisição, como body, params e headers
	 * @param {*} res - dados montados para a resposta
	 * @returns {Object} - objeto com uma mensagem e a lista de registros de modelos de equipamentos
	 */
	async indexModels(req, res) {
		let data = {
			headers: req.headers,
			query: {
				patrimonies: req.query.patrimonies
			},
			auth: req.auth
		}

		return await this.facadeManager
			.action(this.facadeManager.ActionEnums.getModels, data, true)
			.then(result => this.replyManager.handleSuccess('Lista de modelos de equipamentos encontrada!', result))
			.catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
	}

	/**
	 * Atribui os dados recebidos da requisição à um objeto e
	 * consome o serviço de equipmentModels para inserir um novo registro no banco de dados
	 * @param {*} req - dados da requisição, como body, params e headers
	 * @param {*} res - dados montados para a resposta
	 * @returns {Object} - mensagem de sucesso e dados da empresa que foi inserida no banco
	 */
	async insertModel(req, res) {
		let data = {
			headers: req.headers,
			auth: req.auth,
			payload: req.payload
		}

		return this.facadeManager.action(this.facadeManager.ActionEnums.insertModel, data, true)
			.then(result => this.replyManager.handleSuccess('Modelo de equipamento cadastrado!', result))
			.catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
	}

	/**
	 * Consome o serviço de equipmentModels para adquirir os dados do modelo
	 * que possui o ID infrmado nos parâmetros
	 * @param {Object} req - Objeto da requisição
	 * @param {Object} res - Objeto da resposta
	 * @returns {Object} - mensagen de sucesso e os dados o modelo
	 */
	async infoModel(req, res) {
		let data = {
			headers: req.headers,
			search: req.params.id,
			auth: req.auth,
			query: req.query
		}

		return await this.facadeManager.action(this.facadeManager.ActionEnums.getModel, data, true)
			.then(result => this.replyManager.handleSuccess('Modelo de equipamento encontrado!', result))
			.catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
	}

	/**
	 * Consome o serviço de equipmentModels para atualizar um registro de um
	 * modelo no banco de dados
	 * @param {*} req - objeto da requisição
	 * @param {*} res - objeto da resposta
	 * @returns {Object} - mensagem de sucesso e o modelo com os dados atualizados
	 */
	async updateModel(req, res) {
		let data = {
			headers: req.headers,
			search: req.params.id,
			auth: req.auth,
			payload: req.payload
		}

		return this.facadeManager.action(this.facadeManager.ActionEnums.updateModel, data, true)
			.then(result => this.replyManager.handleSuccess('Modelo de equipamento atualizado!', result))
			.catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
	}

	/**
	 * Consome o serviço de equipmentModels para remover um registro de
	 * um modelo do banco de dados
	 * @param {Object} req - objeto da requisição
	 * @param {Object} res - objeto da resposta
	 * @returns {Object} - mensagem de sucesso
	 */
	async deleteModel(req, res) {
		let data = {
			headers: req.headers,
			params: req.params,
			auth: req.auth
		}

		return await this.facadeManager.action(this.facadeManager.ActionEnums.deleteModel, data, true)
			.then(result => this.replyManager.handleSuccess(`Model de equipamento excluído com sucesso!`, result))
			.catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
	}

	/**
	* Atribui os dados recebidos da requisição à um objeto e
	* consome o serviço de equipmentPatrimonies para adquirir uma lista de patrimônios
	* @param {*} req - dados da requisição, como body, params e headers
	* @param {*} res - dados montados para a resposta
	* @returns {Object} - objeto com uma mensagem e a lista de registros de patrimônios de equipamentos
	*/
	async indexPatrimony(req, res) {
		let data = {
			headers: req.headers,
			auth: req.auth,
			query: req.query
		}

		return await this.facadeManager
			.action(this.facadeManager.ActionEnums.getPatrimonies, data, true)
			.then(result => this.replyManager.handleSuccess('Lista de patrimônios encontrada!', result))
			.catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
	}

	/**
	 * Atribui os dados recebidos da requisição à um objeto e
	 * consome o serviço de equipmentPatrimonies para inserir um novo
	 * patrimônio no banco de dados
	 * @param {*} req - dados da requisição, como body, params e headers
	 * @param {*} res - dados montados para a resposta
	 * @returns {Object} - mensagem de sucesso e dados do patrimônio que foi inserido no banco
	 */
	async insertPatrimony(req, res) {
		let data = {
			headers: req.headers,
			params: req.params,
			auth: req.auth,
			payload: {
				equipment_model_id: req.payload.equipment_model_id,
				mac: req.payload.mac.replace(/[^\w\s]/gi, ''),
				code: req.payload.code.replace(/[^\w\s]/gi, ''),
				status: Number(req.payload.status),
				token: ''
			}
		}

		return this.facadeManager.action(this.facadeManager.ActionEnums.insertPatrimony, data, true)
			.then(result => this.replyManager.handleSuccess('Patrimônio cadastrado com sucesso!', result))
			.catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
	}

	/**
	 * Consome o serviço de equipmentPatrimonies para adquirir os dados do patrimônio
	 * que possui o ID infrmado nos parâmetros
	 * @param {Object} req - Objeto da requisição
	 * @param {Object} res - Objeto da resposta
	 * @returns {Object} - mensagen de sucesso e os dados do patrimônio
	 */
	async infoPatrimony(req, res) {
		let data = {
			headers: req.headers,
			search: req.params.id,
			auth: req.auth,
			query: req.query
		}

		return await this.facadeManager.action(this.facadeManager.ActionEnums.getPatrimony, data, true)
			.then(result => this.replyManager.handleSuccess('Patrimônio encontrado!', result))
			.catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
	}

	/**
	 * Consome o serviço de equipmentPatrimonies para atualizar um registro de um
	 * patrimônio no banco de dados
	 * @param {*} req - objeto da requisição
	 * @param {*} res - objeto da resposta
	 * @returns {Object} - mensagem de sucesso e o patrimônio com os dados atualizados
	 */
	async updatePatrimony(req, res) {
		let data = {
			headers: req.headers,
			search: req.params.id,
			auth: req.auth,
			payload: req.payload
		}

		return this.facadeManager.action(this.facadeManager.ActionEnums.updatePatrimony, data, true)
			.then(result => this.replyManager.handleSuccess('Patrimônio atualizado!', result))
			.catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
	}

	/**
	 * Consome o serviço de equipmentPatrimonies para remover um registro de patrimônio 
	 * do banco de dados
	 * @param {Object} req - objeto da requisição
	 * @param {Object} res - objeto da resposta
	 * @returns {Object} - mensagem de sucesso
	 */
	async deletePatrimony(req, res) {
		let data = {
			headers: req.headers,
			params: req.params,
			auth: req.auth
		}

		return await this.facadeManager.action(this.facadeManager.ActionEnums.deletePatrimony, data, true)
			.then(result => this.replyManager.handleSuccess(`Patrimônio excluído com sucesso!`, result))
			.catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
	}
}