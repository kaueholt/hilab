'use strict'

/**
 * Classe controladora das operações de usuários que as rotas soliciam
 */
module.exports = class UserController {
  /**
   * Construtor da classe
   * @param {FacadeManager} facadeManager 
   * @param {ReplyManager} replyManager 
   * @param {ValidationUtils} validationUtils 
   */
  constructor(facadeManager, replyManager, validationUtils) {
    this.facadeManager = facadeManager
    this.replyManager = replyManager
    this.validationUtils = validationUtils
  }

  /**
  * Atribui os dados recebidos da requisição à um objeto e
  * consome o serviço de usuários através do facadeManager para
  * adquirir uma lista de usuários
  * @param {*} req - dados da requisição, como body, params e headers
  * @param {*} res - dados montados para a resposta
  * @returns {Array} - lista de registros de usuários
  */
  async index(req, res) {
    let data = {
      headers: req.headers,
      query: req.query,
      auth: req.auth
    }

    return await this.facadeManager.action(this.facadeManager.ActionEnums.getUsers, data, true)
      .then(result => this.replyManager.handleSuccess(`Lista encontrada com sucesso!`, result))
      .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
  }

  /**
   * Consome o serviço UserService para adquirir os dados do usuário
   * que possui o CPF infrmado nos parâmetros
   * @param {Object} req - Objeto da requisição
   * @param {Object} res - Objeto da resposta
   * @returns {Object} - dados do usuário
   */
  async info(req, res) {
    let data = {
      headers: req.headers,
      search: req.params.cpf,
      auth: req.auth,
      query: req.query
    }

    return await this.facadeManager.action(this.facadeManager.ActionEnums.getUser, data, true)
      .then(result => this.replyManager.handleSuccess('Usuário encontrado!', result))
      .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))

  }

  /**
   * Atribui os dados recebidos da requisição à um objeto e
   * consome o serviço de usuários através do facadeManager para
   * inserir um usuário na banco de dados
   * @param {*} req - dados da requisição, como body, params e headers
   * @param {*} res - dados montados para a resposta
   * @returns {Object} - dados do usuário que foi inserido no banco
   */
  async insert(req, res) {
    let data = {
      headers: req.headers,
      auth: req.auth,
      payload: {
        name: req.payload.name,
        cpf: req.payload.cpf.replace(/[^\w\s]/gi, ''),
        email: req.payload.email,
        date_birth: req.payload.date_birth,
        status: req.payload.status,
        role: req.payload.role,
        token: '',
        password: '',
        avatar: req.payload.avatar || ''
      }
    }

    let isCpf = this.validationUtils.isCPF(data.payload.cpf)
    !isCpf && this.replyManager.handleError(this.replyManager.statusEnum.badRequest, new Error('CPF inválido!'))

    data.payload.avatar !== undefined ? data.payload.avatar : data.payload.avatar = ''

    return await this.facadeManager.action(this.facadeManager.ActionEnums.insertUser, data, true)
      .then(result => this.replyManager.handleSuccess(`Usuário cadastrado com sucesso!`, result))
      .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))

  }

  /**
   * Consome o serviço UserService para atualizar um registro de um 
   * usuário no banco de dados
   * @param {*} req - objeto da requisição
   * @param {*} res - objeto da resposta
   * @returns {Object} - usuário com os dados atualizados
   */
  async update(req, res) {
    let data = {
      headers: req.headers,
      search: req.params.cpf,
      auth: req.auth,
      payload: req.payload
    }

    return this.facadeManager.action(this.facadeManager.ActionEnums.updateUser, data, true)
      .then(result => this.replyManager.handleSuccess(`Usuário atualizado com sucesso!`, result))
      .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
  }

  /**
   * Consome o serviço UserService para remover um usuário do banco de dados
   * e deletar sua imagem do diretório de uploads
   * @param {Object} req  - objeto da requisição
   * @param {Object} res - objeto da resposta
   * @returns {Object} - mensagem de sucesso
   */
  async delete(req, res) {
    let data = {
      headers: req.headers,
      search: req.params.cpf,
      auth: req.auth
    }

    return await this.facadeManager.action(this.facadeManager.ActionEnums.deleteUser, data, true)
      .then(result => this.replyManager.handleSuccess(`Usuário excluído com sucesso.`, result))
      .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
  }
}
