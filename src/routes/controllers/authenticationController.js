'use strict'

/**
 * Classe controladora das operações de autenticação
 */
module.exports = class AuthenticationController {
    /**
     * Construtor da classe
     * @param {FacadeManager} facadeManager - instância da classe facade para disparar eventos para os serviços
     * @param {StatusEnum} statusEnum - enumerador com os tipos de erros
     * @param {ReplyManager} replyManager - classe encarregada de tratar erros para o response
     */
    constructor(facadeManager, statusEnum, replyManager, validationUtils) {
        this.facadeManager = facadeManager
        this.statusEnum = statusEnum
        this.replyManager = replyManager
        this.validationUtils = validationUtils
    }

    /**
     * Atribui os dados recebidos da requisição à um objeto e
     * consome o serviço de autenticação através do facadeManager
     * @param {*} req - dados da requisição, como body, params e headers
     * @param {*} res - dados montados para a resposta
     */
    async login(req, res) {
        let data = {
            headers: req.headers,
            payload: {
                cpf: req.payload.cpf,
                password: req.payload.password
            }
        }

        return await this.facadeManager.action(this.facadeManager.ActionEnums.login, data)
            .then(result => this.replyManager.handleSuccess(`Login efetuado com sucesso!`, result))
            .catch(error => this.replyManager.handleError(this.statusEnum.badRequest, error))
    }

    /**
     * Atribui os dados recebidos da requisição à um objeto e
     * consome o serviço de autenticação para efetaur o logout do usuário
     * @param {*} req - dados da requisição
     * @param {*} res - dados da resposta
     */
    async logout(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
        }

        return await this.facadeManager.action(this.facadeManager.ActionEnums.logout, data, true)
            .then(result => this.replyManager.handleSuccess(`Logout efetuado com sucesso!`, result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))
    }

    /**
    * Atribui os dados recebidos da requisição à um objeto e
    * consome o serviço de usuários através do facadeManager para
    * inserir um usuário na banco de dados
    * @param {*} req - dados da requisição, como body, params e headers
    * @param {*} res - dados montados para a resposta
    * @returns {Object} - dados do usuário que foi inserido no banco
    */
    async insert(req, res) {
        let data = {
            headers: req.headers,
            auth: req.auth,
            payload: {
                name: req.payload.name,
                cpf: req.payload.cpf.replace(/[^\w\s]/gi, ''),
                email: req.payload.email,
                date_birth: req.payload.date_birth,
                status: req.payload.status,
                role: req.payload.role,
                token: '',
                password: '',
                avatar: req.payload.avatar || ''
            }
        }

        let isCpf = this.validationUtils.isCPF(data.payload.cpf)
        !isCpf && this.replyManager.handleError(this.replyManager.statusEnum.badRequest, new Error('CPF inválido!'))

        data.payload.avatar !== undefined ? data.payload.avatar : data.payload.avatar = ''

        return await this.facadeManager.action(this.facadeManager.ActionEnums.insertUser, data)
            .then(result => this.replyManager.handleSuccess(`Usuário cadastrado com sucesso!`, result))
            .catch(error => this.replyManager.handleError(this.replyManager.statusEnum.badRequest, error))

    }

    /**
   * Atribui os dados recebidos da requisição à um objeto e
   * consome o serviço de autenticação através do facadeManager
   * @param {*} req - dados da requisição, como body, payload, params e headers
   * @param {*} res - dados montados para a resposta
   */
    async forgotPassword(req, res) {
        let data = {
            headers: req.headers,
            search: req.payload.cpf.replace(/[^\w\s]/gi, ''),
        }
        return await this.facadeManager.action(this.facadeManager.ActionEnums.forgotPassword, data)
            .then(result => this.replyManager.handleSuccess(
                `E-mail de recuperação de senha enviado para ${result.email}`,
                { email: result.email }
            ))
            .catch(error => this.replyManager.handleError(this.statusEnum.badRequest, error))
    }

    /**
* Atribui os dados recebidos da requisição à um objeto e
* consome o serviço de autenticação através do facadeManager
* @param {*} req - dados da requisição, como body, params e headers
* @param {*} res - dados montados para a resposta
*/
    async resetPassword(req, res) {
        let data = {
            headers: req.headers,
            search: req.params.cpf,
            params: req.params
        }
        return await this.facadeManager.action(this.facadeManager.ActionEnums.resetPassword, data)
            .then(result => this.replyManager.handleSuccess(`Senha de login atualizada com sucesso!`, { email: result.email }))
            .catch(error => this.replyManager.handleError(this.statusEnum.badRequest, error))
    }
}