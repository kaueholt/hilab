'use strict'
const Joi = require('@hapi/joi');

/**
 * Classe responsável por configurar as rotas para serviços de
 * modelos de equipamentos e patrimônios de modelos de equipamentos
 */
module.exports = class EquipmentRoutes {
    constructor(equipmentController, config) {
        this.equipmentController = equipmentController
        this.config = config
        this.path = '/equipments'

        this.indexModels = {
            path: this.path,
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma lista de modelos equipamentos",
                notes: "Consome EquipmentServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    query: Joi.object({
                        name: Joi.string().default(''),
                        maker: Joi.string().default(''),
                        patrimonies: Joi.boolean().default(false)
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.array().items({
                            id: Joi.string().required(),
                            name: Joi.string().required(),
                            maker: Joi.string().required(),
                            description: Joi.string().allow(''),
                            equipment_patrimonies: Joi.array().items({
                                id: Joi.string().required(),
                                equipment_model_id: Joi.string().required(),
                                code: Joi.string().allow(''),
                                mac: Joi.string().required(),
                                status: Joi.number().required(),
                                token: Joi.string().allow(''),
                            })
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await equipmentController.indexModels(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.infoModel = {
            path: this.path + '/{id}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir informações de um modelo de equipamento",
                notes: "Consome EquipmentServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        id: Joi.string().trim().required()
                    }),
                    query: Joi.object({
                        name: Joi.string().default(''),
                        maker: Joi.string().default(''),
                        patrimonies: Joi.boolean().default(false)
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            name: Joi.string().required(),
                            maker: Joi.string().required(),
                            description: Joi.string().allow(''),
                            created_at: Joi.date(),
                            updated_at: Joi.date()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await equipmentController.infoModel(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insertModel = {
            path: this.path,
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para inserir um novo modelo de equipamento",
                notes: "Consome EquipmentServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    payload: Joi.object().keys({
                        name: Joi.string().trim().required(),
                        maker: Joi.string().trim().required(),
                        description: Joi.string().trim().allow(''),
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            name: Joi.string().required(),
                            maker: Joi.string().required(),
                            description: Joi.string().allow(''),
                            created_at: Joi.date(),
                            updated_at: Joi.date()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await equipmentController.insertModel(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.updateModel = {
            path: this.path + '/{id}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para atualizar informações de um modelo de equipamento",
                notes: "Consome EquipamentServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        id: Joi.string().trim().required()
                    }),
                    payload: Joi.object().keys({
                        name: Joi.string().trim().allow(''),
                        maker: Joi.string().trim().allow(''),
                        description: Joi.string().allow(''),
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            name: Joi.string().required(),
                            maker: Joi.string().required(),
                            description: Joi.string().allow(''),
                            created_at: Joi.date(),
                            updated_at: Joi.date()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await equipmentController.updateModel(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.deleteModel = {
            path: this.path + '/{id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover um modelo de equipamento do banco de dados",
                notes: "Consome EquipmentServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        id: Joi.string().trim().required()
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await equipmentController.deleteModel(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.indexPatrimonies = {
            path: this.path + '/patrimonies/',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota utilizada para resgatar uma lista de patrimonios",
                notes: "consome EquipmentServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    query: Joi.object({
                        mac: Joi.string().default(''),
                        code: Joi.string().default(''),
                        status: Joi.string().default(''),
                        equipment_model_id: Joi.string().default(''),
                        model: Joi.boolean().default(true)
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.array().items({
                            id: Joi.string().required(),
                            mac: Joi.string().required(),
                            code: Joi.string().allow(''),
                            status: Joi.number().required(),
                            token: Joi.any().allow(''),
                            created_at: Joi.date(),
                            updated_at: Joi.date(),
                            equipment_model_id: Joi.string().required(),
                            equipment_model: Joi.object({
                                id: Joi.string(),
                                name: Joi.string(),
                                maker: Joi.string(),
                                description: Joi.string().allow('')
                            })
                        })
                    }).meta({ className: 'Response' })
                },
            },
            handler: async (req, res) => {
                return await equipmentController.indexPatrimony(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.infoPatrimony = {
            path: this.path + '/patrimonies/{id}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota utilizada para adquirir informação de um patrimônio",
                notes: "Consome EquipmentServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        id: Joi.string().trim().required()
                    }),
                    query: Joi.object({
                        mac: Joi.string().default(''),
                        code: Joi.string().default(''),
                        model: Joi.boolean().default(true),
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            mac: Joi.string().required(),
                            code: Joi.string().allow(''),
                            status: Joi.number().required(),
                            equipment_model_id: Joi.string().required(),
                            equipment_model: Joi.object({
                                id: Joi.string(),
                                name: Joi.string(),
                                maker: Joi.string(),
                                description: Joi.string().allow('')
                            })
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await equipmentController.infoPatrimony(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insertPatrimony = {
            path: this.path + '/patrimonies',
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota utilizada para inserir um patrimônio de equipamento",
                notes: "Consome EquipmentServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    payload: Joi.object({
                        equipment_model_id: Joi.string().required(),
                        mac: Joi.string().trim().required(),
                        code: Joi.string().trim().allow(''),
                        status: Joi.string().trim().required(),
                    })
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            mac: Joi.string().required(),
                            code: Joi.string().allow(''),
                            status: Joi.number().required(),
                            equipment_model_id: Joi.string(),
                            updated_at: Joi.date(),
                            created_at: Joi.date(),
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await equipmentController.insertPatrimony(req, res)
                    .then(result => result)
                    .catch(err => err)
            }
        }

        this.updatePatrimony = {
            path: this.path + '/patrimonies/{id}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota utilizada para atualizar um patrimônio",
                notes: "Consome EquipmentServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        id: Joi.string().trim().required()
                    }),
                    payload: Joi.object({
                        equipment_model_id: Joi.string().allow(''),
                        mac: Joi.string().trim().allow(''),
                        code: Joi.string().trim().allow(''),
                        status: Joi.string().trim().allow(''),
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            mac: Joi.string().required(),
                            code: Joi.string().allow(''),
                            status: Joi.number().required(),
                            updated_at: Joi.date(),
                            created_at: Joi.date(),
                            token: Joi.any(),
                            equipmentModelId: Joi.string().trim(),
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await equipmentController.updatePatrimony(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.deletePatrimony = {
            path: this.path + '/patrimonies/{id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota utilizada para deletar um patrimônio",
                notes: "Consome EquipmentServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        id: Joi.string().trim().required()
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await equipmentController.deletePatrimony(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }
    }

    /**
     * Retorna as rotas aqui configuradas
     */
    getRoutes() {
        return [
            this.indexModels, this.insertModel,
            this.infoModel, this.updateModel,
            this.deleteModel, this.insertPatrimony,
            this.updatePatrimony, this.deletePatrimony,
            this.indexPatrimonies, this.infoPatrimony
        ];
    }
}