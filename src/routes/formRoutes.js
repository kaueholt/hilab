'use strict'

/**
 * Classe responsável por configurar as rotas de classes de formulários
 */
module.exports = class ExamRoutes {
    constructor(joi, formController, config) {
        this.joi = joi
        this.formController = formController
        this.config = config
        this.path = '/forms'

        this.index = {
            path: this.path + "/",
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma lista de tipos de formulários",
                notes: "Consome FormServices",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    query: this.joi.object({
                        exam_class_id: this.joi.string().default(''),
                        equipment_model_id: this.joi.string().default(''),
                        name: this.joi.string().default(''),
                        status: this.joi.string().default('')
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.array().items({
                            id: this.joi.string().required(),
                            name: this.joi.string().required(),
                            description: this.joi.string().allow(''),
                            exam_class_id: this.joi.string().required(),
                            equipment_model_id: this.joi.any(),
                            status: this.joi.number(),
                            equipment_model: this.joi.object({
                                id: this.joi.any(),
                                name: this.joi.any(),
                                maker: this.joi.any(),
                                description: this.joi.any()
                            }),
                            exam_class: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required()
                            }),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.index(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.info = {
            path: this.path + '/{id}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir um formulário com seus tópicos e questões",
                notes: "Consome FormServices",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    query: this.joi.object({
                        topics: this.joi.bool().default(false)
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            name: this.joi.string().required(),
                            description: this.joi.string().allow(''),
                            exam_class_id: this.joi.string().required(),
                            equipment_model_id: this.joi.any(),
                            status: this.joi.number(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            equipment_model: this.joi.object({
                                id: this.joi.any(),
                                name: this.joi.any(),
                                maker: this.joi.any(),
                                description: this.joi.any()
                            }),
                            exam_class: this.joi.object({
                                id: this.joi.string().required(),
                                name: this.joi.string().required()
                            }),
                            topics: this.joi.array().items({
                                id: this.joi.any(),
                                status: this.joi.number(),
                                description: this.joi.any(),
                                sequence: this.joi.any(),
                                formulary_id: this.joi.any()
                            }),
                        }),
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.info(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insert = {
            path: this.path,
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para inserir um novo tipo de formulário",
                notes: "Consome FormServices",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        name: this.joi.string().required(),
                        description: this.joi.string().allow(''),
                        exam_class_id: this.joi.string().required(),
                        equipment_model_id: this.joi.any()
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            exam_class_id: this.joi.string().required(),
                            equipment_model_id: this.joi.any(),
                            status: this.joi.number().required(),
                            name: this.joi.string().required(),
                            description: this.joi.string().allow(''),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await formController.insert(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.delete = {
            path: this.path + '/{id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover um tipo de formulário do banco de dados",
                notes: "Consome FormServices",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.delete(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.update = {
            path: this.path + '/{id}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para atualizar um tipo de formulário",
                notes: "Consome FormServices",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        name: this.joi.string(),
                        description: this.joi.string().allow(''),
                        exam_class_id: this.joi.string(),
                        equipment_model_id: this.joi.string().allow(null),
                        status: this.joi.number(),
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            exam_class_id: this.joi.string().required(),
                            equipment_model_id: this.joi.any(),
                            status: this.joi.number().required(),
                            name: this.joi.string().required(),
                            description: this.joi.string().allow(''),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.update(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insertTopic = {
            path: this.path + '/topics',
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para inserir um tópico de questões em um formulário",
                notes: "Consome FormService",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        formulary_id: this.joi.string().required(),
                        sequence: this.joi.number().required(),
                        description: this.joi.string().required(),
                        status: this.joi.number().required(),
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            formulary_id: this.joi.string().required(),
                            sequence: this.joi.number().required(),
                            description: this.joi.string().required(),
                            status: this.joi.number().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            FormularyId: this.joi.string()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.insertTopic(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.updateTopic = {
            path: this.path + '/topics/{id}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para atualizar um tópico de questões em um formulário",
                notes: "Consome FormService",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        description: this.joi.string(),
                        sequence: this.joi.string(),
                        status: this.joi.number(),
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            FormularyId: this.joi.string().required(),
                            sequence: this.joi.number().required(),
                            description: this.joi.string().required(),
                            status: this.joi.number().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.updateTopic(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.deleteTopic = {
            path: this.path + '/topics/{id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover um tópico de formulário do banco de dados",
                notes: "Consome FormServices",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.deleteTopic(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.infoTopic = {
            path: this.path + '/topics/{id}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir um tópico com suas questões",
                notes: "Consome FormServices",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    query: this.joi.object({
                        questions: this.joi.bool().default(false)
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            description: this.joi.string().required(),
                            sequence: this.joi.number(),
                            status: this.joi.number(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            formulary_id: this.joi.string(),
                            questions: this.joi.array().items({
                                id: this.joi.any(),
                                status: this.joi.any(),
                                description: this.joi.any(),
                                sequence: this.joi.any(),
                                formulary_id: this.joi.any(),
                                topic_form_id: this.joi.any(),
                                complement: this.joi.any(),
                                type: this.joi.any(),
                                created_at: this.joi.any(),
                                updated_at: this.joi.any(),
                                TopicFormId: this.joi.any()
                            })
                        }),
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.infoTopic(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.indexTopics = {
            path: this.path + "/topics/",
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma lista de tópicos/módulos",
                notes: "Consome FormServices",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    query: this.joi.object({
                        formulary_id: this.joi.string().default(''),
                        description: this.joi.string().default('')
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.array().items({
                            id: this.joi.string().required(),
                            sequence: this.joi.number().required(),
                            description: this.joi.string().required(),
                            formulary_id: this.joi.string().required(),
                            status: this.joi.number(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.indexTopics(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insertQuestion = {
            path: this.path + '/questions',
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para inserir uma questão",
                notes: "Consome FormService",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        formulary_id: this.joi.string().required(),
                        topic_form_id: this.joi.string().required(),
                        sequence: this.joi.number().required(),
                        description: this.joi.string().required(),
                        complement: this.joi.string().allow(''),
                        status: this.joi.number().required(),
                        type: this.joi.string().required()
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            formulary_id: this.joi.string().required(),
                            topic_form_id: this.joi.string().required(),
                            sequence: this.joi.number().required(),
                            description: this.joi.string().required(),
                            complement: this.joi.string().allow(''),
                            type: this.joi.string().required(),
                            status: this.joi.number().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            TopicFormId: this.joi.string()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.insertQuestion(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.updateQuestion = {
            path: this.path + '/questions/{id}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para atualizar uma questão",
                notes: "Consome FormService",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        sequence: this.joi.number(),
                        description: this.joi.string(),
                        type: this.joi.string(),
                        complement: this.joi.string().allow(''),
                        status: this.joi.number(),
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            sequence: this.joi.number().required(),
                            description: this.joi.string().required(),
                            status: this.joi.number().required(),
                            type: this.joi.string().required(),
                            complement: this.joi.string().allow(''),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            formulary_id: this.joi.string().required(),
                            TopicFormId: this.joi.string(),
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.updateQuestion(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.deleteQuestion = {
            path: this.path + '/questions/{id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover uma qestão do banco de dados",
                notes: "Consome FormServices",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.deleteQuestion(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.indexQuestion = {
            path: this.path + "/questions/",
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma lista de questões",
                notes: "Consome FormServices",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    query: this.joi.object({
                        topic_form_id: this.joi.string().default(''),
                        formulary_id: this.joi.string().default(''),
                        question_fields: this.joi.bool().default(false),
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.array().items({
                            id: this.joi.string().required(),
                            description: this.joi.string().required(),
                            status: this.joi.number().required(),
                            sequence: this.joi.number().required(),
                            type: this.joi.string().required(),
                            formulary_id: this.joi.string().required(),
                            topic_form_id: this.joi.string().required(),
                            complement: this.joi.string().allow(''),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.indexQuestions(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.infoQuestion = {
            path: this.path + '/topics/questions/{id}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma questão com seus itens",
                notes: "Consome FormServices",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown()
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            description: this.joi.string().required(),
                            sequence: this.joi.number().required(),
                            status: this.joi.number().required(),
                            complement: this.joi.string().allow(''),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            type: this.joi.date(),
                            formulary_id: this.joi.string(),
                            topic_form_id: this.joi.string(),
                            questions_items: this.joi.array().items({
                                id: this.joi.any(),
                                question_id: this.joi.string(),
                                status: this.joi.any(),
                                description: this.joi.any(),
                                sequence: this.joi.any(),
                                complement: this.joi.any(),
                                value: this.joi.any(),
                                created_at: this.joi.any(),
                                updated_at: this.joi.any(),
                            })
                        }),
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.infoQuestion(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insertQuestionField = {
            path: this.path + '/question-fields',
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para inserir um campo de questão",
                notes: "Consome FormService",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        question_id: this.joi.string().required(),
                        topic_form_id: this.joi.string().required(),
                        formulary_id: this.joi.string().required(),
                        sequence: this.joi.number().allow(''),
                        description: this.joi.string().required(),
                        complement: this.joi.string().allow(''),
                        status: this.joi.number().required(),
                        value: this.joi.string().required()
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            question_id: this.joi.string().required(),
                            topic_form_id: this.joi.string().required(),
                            formulary_id: this.joi.string().required(),
                            sequence: this.joi.number().required(),
                            status: this.joi.number().required(),
                            value: this.joi.string().required(),
                            description: this.joi.string().required(),
                            complement: this.joi.string().allow(''),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            QuestionId: this.joi.string(),
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.insertQuestionField(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.updateQuestionField = {
            path: this.path + '/question-fields/{id}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para atualizar um item de um campo",
                notes: "Consome FormService",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    payload: this.joi.object().keys({
                        sequence: this.joi.number(),
                        status: this.joi.number(),
                        value: this.joi.string(),
                        description: this.joi.string(),
                        complement: this.joi.string().allow('')
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string().required(),
                            sequence: this.joi.number().required(),
                            description: this.joi.string().required(),
                            complement: this.joi.string().allow(''),
                            value: this.joi.string().required(),
                            status: this.joi.number().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                            formulary_id: this.joi.string().required(),
                            topic_form_id: this.joi.string().required(),
                            QuestionId: this.joi.string(),
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.updateQuestionField(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.deleteQuestionField = {
            path: this.path + '/question-fields/{id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover um campo de questão",
                notes: "Consome FormServices",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.deleteQuestionField(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.infoQuestionField = {
            path: this.path + '/question-fields/{id}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma questão com seus itens",
                notes: "Consome FormServices",
                validate: {
                    params: this.joi.object({
                        id: this.joi.string().trim().required()
                    }),
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown()
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.object({
                            id: this.joi.string(),
                            question_id: this.joi.string(),
                            status: this.joi.number(),
                            description: this.joi.string(),
                            sequence: this.joi.number(),
                            complement: this.joi.string(),
                            value: this.joi.string(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date(),
                        }),
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.info(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.indexQuestionFields = {
            path: this.path + "/question-fields/",
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma lista de itens de questões",
                notes: "Consome FormServices",
                validate: {
                    headers: this.joi.object({
                        authorization: this.joi.string().required(),
                        origin: this.joi.string().default('swagger'),
                        agent: this.joi.string().default('browser')
                    }).unknown(),
                    query: this.joi.object({
                        topic_form_id: this.joi.string().allow(''),
                        formulary_id: this.joi.string().allow(''),
                        question_id: this.joi.string().allow(''),
                    })
                },
                response: {
                    schema: this.joi.object({
                        message: this.joi.string(),
                        data: this.joi.array().items({
                            id: this.joi.string().required(),
                            sequence: this.joi.number().required(),
                            description: this.joi.string().required(),
                            question_id: this.joi.string().required(),
                            formulary_id: this.joi.string().required(),
                            topic_form_id: this.joi.string().required(),
                            status: this.joi.number().required(),
                            complement: this.joi.string().allow(''),
                            value: this.joi.string().required(),
                            created_at: this.joi.date(),
                            updated_at: this.joi.date()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await formController.indexQuestionFields(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }
    }

    /**
     * Retorna as rotas aqui configuradas
     */
    getRoutes() {
        return [
            this.info, this.insert, this.update, this.delete, this.index,
            this.infoTopic, this.insertTopic, this.updateTopic, this.deleteTopic, this.indexTopics,
            this.infoQuestion, this.insertQuestion, this.updateQuestion, this.deleteQuestion,
            this.indexQuestion, this.infoQuestionField, this.insertQuestionField, this.updateQuestionField,
            this.deleteQuestionField, this.indexQuestionFields
        ];
    }
}
