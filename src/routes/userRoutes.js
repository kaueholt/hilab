'use strict'
const Joi = require('@hapi/joi')

/**
 * Classe responsável por configurar as rotas para serviços de usuários
 */
module.exports = class UserRoutes {
    constructor(userController, config) {
        this.config = config
        this.userController = userController
        this.path = "/users"

        this.index = {
            path: this.path + "/",
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para consultar uma lista de usuários",
                notes: "Consome UserService",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    query: Joi.object({
                        name: Joi.string().default(''),
                        cpf: Joi.string().default(''),
                        status: Joi.string().default('')
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.array().items({
                            id: Joi.string().required(),
                            name: Joi.string().required(),
                            cpf: Joi.string().required(),
                            email: Joi.string().required(),
                            date_birth: Joi.string().required(),
                            avatar: Joi.string().required(),
                            status: Joi.number().required(),
                            role: Joi.number().required()
                        }).meta({ className: 'Response' })
                    })
                },
                handler: async (req, res) => {
                    return await userController.index(req, res)
                        .then(result => result)
                        .catch(error => error)
                }
            }
        }

        this.info = {
            path: this.path + '/{cpf}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir informações de um usuário",
                notes: "Consome UserServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cpf: Joi.string().trim().required()
                    }),
                    query: Joi.object({
                        name: Joi.string().default(''),
                        cpf: Joi.string().default(''),
                        status: Joi.string().default('')
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            name: Joi.string().required(),
                            cpf: Joi.string().required(),
                            email: Joi.string().email().required(),
                            date_birth: Joi.string().required(),
                            avatar: Joi.string().required(),
                            status: Joi.number().required(),
                            role: Joi.number().required(),
                            created_at: Joi.date(),
                            updated_at: Joi.date()
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await userController.info(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insert = {
            path: this.path,
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para cadastrar um usuário",
                notes: "Consome UserService",
                payload: {
                    output: 'stream',
                    parse: true,
                    allow: 'multipart/form-data',
                    maxBytes: 1024 * 1024 * 100,
                    multipart: true
                },
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    payload: Joi.object({
                        name: Joi.string().required(),
                        cpf: Joi.string().trim().required(),
                        email: Joi.string().email().required(),
                        date_birth: Joi.string().required(),
                        avatar: Joi.any()
                            .meta({ swaggerType: 'file' })
                            .optional()
                            .allow('')
                            .description('image file'),
                        status: Joi.string().required(),
                        role: Joi.string().required()
                    }),
                },
                response: {
                    schema:
                        Joi.object({
                            message: Joi.string(),
                            data: Joi.object({
                                id: Joi.string().required(),
                                name: Joi.string().required(),
                                cpf: Joi.string().required(),
                                email: Joi.string().email().required(),
                                date_birth: Joi.string().required(),
                                avatar: Joi.string().required(),
                                status: Joi.number().required(),
                                role: Joi.number().required(),
                                updated_at: Joi.date(),
                                created_at: Joi.date()
                            })
                        }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await userController.insert(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.update = {
            path: this.path + '/{cpf}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para atualizar informações de um usuário",
                notes: "Consome UserServices",
                payload: {
                    output: 'stream',
                    parse: true,
                    allow: 'multipart/form-data',
                    maxBytes: 1024 * 1024 * 100,
                    multipart: true
                },
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cpf: Joi.string().trim().required()
                    }),
                    payload: Joi.object({
                        name: Joi.string().trim().allow(''),
                        email: Joi.string().email().allow(''),
                        date_birth: Joi.string().trim().allow(''),
                        avatar: Joi.any()
                            .meta({ swaggerType: 'file' })
                            .optional()
                            .allow('')
                            .description('image file'),
                        status: Joi.string().allow(''),
                        role: Joi.string().allow('')
                    }),
                },
                response: {
                    schema:
                        Joi.object({
                            message: Joi.string(),
                            data: Joi.object({
                                id: Joi.string().required(),
                                name: Joi.string().required(),
                                cpf: Joi.string().required(),
                                email: Joi.string().email().required(),
                                date_birth: Joi.string().required(),
                                avatar: Joi.string().required(),
                                status: Joi.number().required(),
                                role: Joi.number().required(),
                                created_at: Joi.date().required(),
                                updated_at: Joi.date().required()
                            })
                        }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await userController.update(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.delete = {
            path: this.path + '/{cpf}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover um usuário",
                notes: "Consome UserService",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cpf: Joi.string().trim().required()
                    }),
                },
                response: {
                    schema:
                        Joi.object({
                            message: Joi.string(),
                            data: Joi.any()
                        }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await userController.delete(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.getAvatar = {
            path: this.path + '/picture/{file*}',
            method: 'GET',
            options: {
                auth: false,
                tags: ['api'],
                description: "Rota para visualizar a imagem de perfil de um usuário",
                notes: "Busca a imagem no diretório de upload",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown()
                }
            },
            handler: {
                directory: {
                    path: config.Data.server.uploadPath
                }
            }
        }
    }

    /**
     * Retorna as rotas aqui configuradas
     */
    getRoutes() {
        return [
            this.index,
            this.insert,
            this.delete,
            this.info,
            this.update,
            this.getAvatar
        ]
    }
}