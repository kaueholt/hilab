'use strict'
const Joi = require('@hapi/joi');

/**
 * Classe responsável por configurar as rotas para serviços de empresas
 */
module.exports = class CompanyRoutes {
    constructor(companyController, config) {
        this.companyController = companyController
        this.config = config
        this.path = '/companies'

        this.index = {
            path: this.path,
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir uma lista de empresas",
                notes: "Consome CompanyServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    query: Joi.object({
                        cnpj: Joi.string().default(''),
                        name: Joi.string().default(''),
                        status: Joi.string().default(''),
                        addresses: Joi.bool().default(false)
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.array().items({
                            id: Joi.string().required(),
                            name: Joi.string().required(),
                            cnpj: Joi.string().required(),
                            email: Joi.string().required(),
                            phone: Joi.string().required(),
                            foundation_date: Joi.string().required(),
                            status: Joi.number().required(),
                            created_at: Joi.date(),
                            updated_at: Joi.date(),
                            Addresses: Joi.object({
                                id: Joi.string(),
                                street: Joi.string(),
                                number: Joi.string(),
                                complement: Joi.string().allow('').optional(),
                                district: Joi.string(),
                                state: Joi.string(),
                                city: Joi.string(),
                                zipcode: Joi.string(),
                                company_id: Joi.string(),
                            })
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await companyController.index(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.info = {
            path: this.path + '/{cnpj}',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para adquirir informações de uma empresa",
                notes: "Consome CompanyServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cnpj: Joi.string().trim().required()
                    }),
                    query: Joi.object({
                        cnpj: Joi.string().default(''),
                        name: Joi.string().default(''),
                        addresses: Joi.boolean().default(false)
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            name: Joi.string().required(),
                            cnpj: Joi.string().required(),
                            email: Joi.string().required(),
                            phone: Joi.string().required(),
                            foundation_date: Joi.string().required(),
                            status: Joi.number().required(),
                            created_at: Joi.date(),
                            updated_at: Joi.date(),
                            Addresses: Joi.object({
                                id: Joi.string(),
                                company_id: Joi.string(),
                                street: Joi.string(),
                                number: Joi.string(),
                                complement: Joi.string().allow(''),
                                district: Joi.string(),
                                state: Joi.string(),
                                city: Joi.string(),
                                zipcode: Joi.string(),
                            })
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await companyController.info(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.insert = {
            path: this.path,
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para inserir uma nova empresa",
                notes: "Consome CompanyServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    payload: Joi.object().keys({
                        name: Joi.string().trim().required(),
                        cnpj: Joi.string().trim().required(),
                        email: Joi.string().trim().required(),
                        phone: Joi.string().trim().required(),
                        foundation_date: Joi.string().trim().required(),
                        status: Joi.number().required(),
                        address: Joi.object({
                            street: Joi.string().required(),
                            number: Joi.string().required(),
                            complement: Joi.string().allow('').optional(),
                            district: Joi.string().required(),
                            state: Joi.string().required(),
                            city: Joi.string().required(),
                            zipcode: Joi.string().required()
                        })
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            name: Joi.string().required(),
                            cnpj: Joi.string().required(),
                            email: Joi.string().required(),
                            phone: Joi.string().required(),
                            foundation_date: Joi.string().required(),
                            status: Joi.number().required(),
                            created_at: Joi.date(),
                            updated_at: Joi.date()
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await companyController.insert(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.delete = {
            path: this.path + '/{cnpj}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para remover uma empresa do banco de dados",
                notes: "Consome CompanyServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cnpj: Joi.string().trim().required()
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await companyController.delete(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.update = {
            path: this.path + '/{cnpj}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para atualizar informações de uma empresa",
                notes: "Consome CompanyServices",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cnpj: Joi.string().trim().required()
                    }),
                    payload: Joi.object().keys({
                        name: Joi.string().trim().allow(''),
                        email: Joi.string().trim().allow(''),
                        phone: Joi.string().trim().allow(''),
                        foundation_date: Joi.string().trim().allow(''),
                        status: Joi.string().allow(''),
                        address: Joi.object({
                            street: Joi.string().trim().allow(''),
                            number: Joi.string().trim().allow(''),
                            complement: Joi.string().trim().allow(''),
                            district: Joi.string().trim().allow(''),
                            city: Joi.string().trim().allow(''),
                            state: Joi.string().max(2).trim().allow(''),
                            zipcode: Joi.string().trim().allow('')
                        })
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            name: Joi.string().required(),
                            cnpj: Joi.string().required(),
                            email: Joi.string().required(),
                            phone: Joi.string().required(),
                            foundation_date: Joi.string().required(),
                            status: Joi.number().required(),
                            created_at: Joi.date(),
                            updated_at: Joi.date(),
                            Addresses: Joi.object({
                                id: Joi.string(),
                                street: Joi.string(),
                                number: Joi.string(),
                                complement: Joi.string().allow(''),
                                district: Joi.string(),
                                city: Joi.string(),
                                state: Joi.string(),
                                zipcode: Joi.string(),
                                company_id: Joi.string(),
                            })
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await companyController.update(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.indexAddress = {
            path: this.path + '/{cnpj}/address/',
            method: ['GET'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota utilizada para resgatar uma lista de endereços de uma empresa",
                notes: "consome CompanyServices e AddressService",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cnpj: Joi.string().trim().required()
                    }),
                },
                response: {
                    schema: Joi.object({
                        payload: Joi.array().items({
                            id: Joi.string().trim(),
                            address: Joi.string().trim().required(),
                            number: Joi.string().trim().required(),
                            complement: Joi.string().trim().allow('').optional(),
                            region: Joi.string().trim(),
                            city: Joi.string().trim().required(),
                            state: Joi.string().max(2).trim().required(),
                            zipCode: Joi.string().trim().required()
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await companyController.indexAddress(req, res).then(result => result).catch(err => err)
            }
        }

        this.insertAddress = {
            path: this.path + '/{id}/address',
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota utilizada para inserir um endereço de uma empresa",
                notes: "Consome CompanyServices e AddressService",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        id: Joi.string().trim().required()
                    }),
                    payload: Joi.object({
                        street: Joi.string().trim().required(),
                        number: Joi.string().trim().required(),
                        complement: Joi.string().trim().allow('').optional(),
                        district: Joi.string().trim().required(),
                        city: Joi.string().trim().required(),
                        state: Joi.string().max(2).trim().required(),
                        zipcode: Joi.string().trim().required()
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            id: Joi.string().required(),
                            company_id: Joi.string().required(),
                            street: Joi.string().trim().required(),
                            number: Joi.string().trim().required(),
                            complement: Joi.string().trim().allow('').optional(),
                            district: Joi.string().trim().required(),
                            city: Joi.string().trim().required(),
                            state: Joi.string().max(2).trim().required(),
                            zipcode: Joi.string().trim().required()
                        })
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await companyController.insertAddress(req, res).then(result => result).catch(err => err)
            }
        }

        this.updateAddress = {
            path: this.path + '/{cnpj}/address/{address_id}',
            method: ['PUT'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota utilizada para atualizar um endereço de uma empresa",
                notes: "Consome CompanyServices e AddressService",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cnpj: Joi.string().trim().required(),
                        address_id: Joi.string().trim().required()
                    }),
                    payload: Joi.object({
                        address_id: Joi.string().trim().required(),
                        address: Joi.string().trim(),
                        number: Joi.string().trim(),
                        complement: Joi.string().trim().allow('').optional(),
                        region: Joi.string().trim(),
                        city: Joi.string().trim(),
                        state: Joi.string().max(2).trim(),
                        zipcode: Joi.string().trim()
                    }),
                },
                response: {
                    schema: Joi.object({
                        id: Joi.string().required(),
                        address: Joi.string().trim().required(),
                        number: Joi.string().trim().required(),
                        complement: Joi.string().trim().allow('').optional(),
                        region: Joi.string().trim().required(),
                        city: Joi.string().trim().required(),
                        state: Joi.string().max(2).trim().required(),
                        zipcode: Joi.string().trim().required()
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await companyController.updateAddress(req, res)
                    .then(result => result)
                    .catch(err => err)
            }
        }

        this.deleteAddress = {
            path: this.path + '/{cnpj}/address/{address_id}',
            method: ['DELETE'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota utilizada para deletar um endereço de uma empresa",
                notes: "Consome CompanyServices e AddressService",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown(),
                    params: Joi.object({
                        cnpj: Joi.string().trim().required(),
                        address_id: Joi.string().trim().required()
                    }),
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string().required()
                    }).meta({
                        className: 'Response'
                    })
                }
            },
            handler: async (req, res) => {
                return await companyController.deleteAddress(req, res).then(result => result).catch(err => err)
            }
        }
    }

    /**
     * Retorna as rotas aqui configuradas
     */
    getRoutes() {
        return [
            this.index, this.insert, this.info, this.update,
            this.delete, this.insertAddress,
            this.deleteAddress, this.indexAddress
        ];
    }
}