'use strict'
const Joi = require('@hapi/joi');

/**
 * Classe responsável por configurar as rotas de Autenticação
 */
module.exports = class AuthenticationRoutes {
    constructor(authenticationController, config) {
        this.config = config
        this.path = '/authentication'

        this.login = {
            path: this.path + '/login',
            method: ['POST'],
            options: {
                auth: false,
                tags: ['api'],
                description: "Rota para efetuar o login do usuário",
                notes: "Consome AuthenticationService",
                validate: {
                    payload: Joi.object().keys({
                        cpf: Joi.string().trim().required().messages(
                            {
                                'string.base': 'Tipo inválido, username precisa ser uma string',
                                'string.empty': 'Por favor, digite seu usuário',
                                'any.required': 'Campo "username" é obrigatório'
                            }
                        ),
                        password: Joi.string().required()
                    }),
                    headers: Joi.object({
                        agent: Joi.string().default('browser'),
                    }).unknown()
                },
                response: {
                    schema:
                        Joi.object({
                            message: Joi.string(),
                            data: Joi.object({
                                id: Joi.string().required(),
                                name: Joi.string().required(),
                                cpf: Joi.string().required(),
                                email: Joi.string().email().required(),
                                date_birth: Joi.string().required(),
                                avatar: Joi.string().required(),
                                role: Joi.number().required(),
                                status: Joi.number().required(),
                                token: Joi.string().required()
                            })
                        }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await authenticationController.login(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.logout = {
            path: this.path + '/logout',
            method: ['POST'],
            options: {
                auth: 'token',
                tags: ['api'],
                description: "Rota para efetuar o logout do usuário",
                notes: "Consome AuthenticationService",
                validate: {
                    headers: Joi.object({
                        authorization: Joi.string().required(),
                        agent: Joi.string().default('browser'),
                    }).unknown()
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.any()
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await authenticationController.logout(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.signUp = {
            path: this.path + '/signup',
            method: ['POST'],
            options: {
                auth: false,
                tags: ['api'],
                description: "Rota para cadastrar um usuário",
                notes: "Consome AuthenticationService",
                payload: {
                    output: 'stream',
                    parse: true,
                    allow: 'multipart/form-data',
                    maxBytes: 1024 * 1024 * 100,
                    multipart: true
                },
                validate: {
                    payload: Joi.object({
                        name: Joi.string().required(),
                        cpf: Joi.string().trim().required(),
                        email: Joi.string().email().required(),
                        date_birth: Joi.string().required(),
                        avatar: Joi.any()
                            .meta({ swaggerType: 'file' })
                            .optional()
                            .allow('')
                            .description('image file'),
                        status: Joi.string().required(),
                        role: Joi.string().required()
                    }),
                    headers: Joi.object({
                        origin: Joi.string().default('swagger'),
                        agent: Joi.string().default('browser')
                    }).unknown()
                },
                response: {
                    schema:
                        Joi.object({
                            message: Joi.string(),
                            data: Joi.object({
                                id: Joi.string().required(),
                                name: Joi.string().required(),
                                cpf: Joi.string().required(),
                                email: Joi.string().email().required(),
                                date_birth: Joi.string().required(),
                                avatar: Joi.string().required(),
                                status: Joi.number().required(),
                                role: Joi.number().required(),
                                updated_at: Joi.date(),
                                created_at: Joi.date()
                            })
                        }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await authenticationController.insert(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.forgotPassword = {
            path: this.path + '/forgot-password',
            method: ['POST'],
            options: {
                auth: false,
                tags: ['api'],
                description: "Rota para recuperar a senha de login do usuário",
                notes: "Consome AuthenticationService",
                validate: {
                    payload: Joi.object().keys({
                        cpf: Joi.string().trim().required().messages(
                            {
                                'string.base': 'Tipo inválido, cpf precisa ser uma string',
                                'string.empty': 'Por favor, digite seu cpf',
                                'any.required': 'Campo "cpf" é obrigatório'
                            }
                        ),
                    }),
                    headers: Joi.object({
                        agent: Joi.string().default('browser'),
                    }).unknown()
                },
                response: {
                    schema:
                        Joi.object({
                            message: Joi.string(),
                            data: Joi.object({
                                email: Joi.string().required(),
                            })
                        }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await authenticationController.forgotPassword(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }

        this.resetPassword = {
            path: this.path + '/reset-password/{token}/{cpf}',
            method: ['GET'],
            options: {
                auth: false,
                tags: ['api'],
                description: "Rota para resetar a senha de login do usuário",
                notes: "Consome AuthenticationService",
                validate: {
                    params: Joi.object({
                        cpf: Joi.string().trim().required(),
                        token: Joi.string().trim().required()
                    }),
                    headers: Joi.object({
                        agent: Joi.string().default('browser'),
                    }).unknown()
                },
                response: {
                    schema: Joi.object({
                        message: Joi.string(),
                        data: Joi.object({
                            email: Joi.string().required(),
                        })
                    }).meta({ className: 'Response' })
                }
            },
            handler: async (req, res) => {
                return await authenticationController.resetPassword(req, res)
                    .then(result => result)
                    .catch(error => error)
            }
        }
    }

    /**
     * Retorna as rotas aqui configuradas
     */
    getRoutes() {
        return [
            this.login, this.logout, this.signUp,
            this.forgotPassword, this.resetPassword
        ]
    }
}