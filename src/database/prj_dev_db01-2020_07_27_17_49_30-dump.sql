--
-- PostgreSQL database dump
--

-- Dumped from database version 10.13
-- Dumped by pg_dump version 12.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE public."SequelizeMeta" OWNER TO lince;

--
-- Name: addresses; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.addresses (
    id uuid NOT NULL,
    street character varying(255) NOT NULL,
    number character varying(4) NOT NULL,
    complement character varying(50),
    district character varying(150) NOT NULL,
    city character varying(255) NOT NULL,
    state character varying(2) NOT NULL,
    zipcode character varying(8) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    company_id uuid
);


ALTER TABLE public.addresses OWNER TO lince;

--
-- Name: circuits; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.circuits (
    id uuid NOT NULL,
    name character varying(255),
    start timestamp with time zone,
    finish timestamp with time zone,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    company_id uuid
);


ALTER TABLE public.circuits OWNER TO lince;

--
-- Name: companies; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.companies (
    id uuid NOT NULL,
    name character varying(50) NOT NULL,
    cnpj character varying(14) NOT NULL,
    email character varying(150) NOT NULL,
    phone character varying(11) NOT NULL,
    foundation_date date NOT NULL,
    status integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.companies OWNER TO lince;

--
-- Name: equipment_models; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.equipment_models (
    id uuid NOT NULL,
    name character varying(30) NOT NULL,
    maker character varying(30) NOT NULL,
    description character varying(200),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.equipment_models OWNER TO lince;

--
-- Name: equipment_patrimonies; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.equipment_patrimonies (
    id uuid NOT NULL,
    code character varying(10),
    mac character varying(32) NOT NULL,
    status integer NOT NULL,
    token character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    equipment_model_id uuid
);


ALTER TABLE public.equipment_patrimonies OWNER TO lince;

--
-- Name: exam_classes; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.exam_classes (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.exam_classes OWNER TO lince;

--
-- Name: exams; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.exams (
    id uuid NOT NULL,
    start timestamp with time zone,
    finish timestamp with time zone,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    circuit_id uuid,
    worker_id uuid
);


ALTER TABLE public.exams OWNER TO lince;

--
-- Name: formularies; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.formularies (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    status integer NOT NULL,
    description character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    exam_class_id uuid,
    equipment_model_id uuid
);


ALTER TABLE public.formularies OWNER TO lince;

--
-- Name: question_items; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.question_items (
    id uuid NOT NULL,
    sequence integer NOT NULL,
    description character varying(255) NOT NULL,
    complement character varying(255) NOT NULL,
    value character varying(255) NOT NULL,
    status integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    question_id uuid,
    formulary_id uuid,
    topic_form_id uuid
);


ALTER TABLE public.question_items OWNER TO lince;

--
-- Name: questions; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.questions (
    id uuid NOT NULL,
    sequence integer NOT NULL,
    description character varying(255) NOT NULL,
    status integer NOT NULL,
    type character varying(255) NOT NULL,
    complement character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    topic_form_id uuid,
    formulary_id uuid
);


ALTER TABLE public.questions OWNER TO lince;

--
-- Name: schedulings; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.schedulings (
    id uuid NOT NULL,
    start timestamp with time zone NOT NULL,
    finish timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    circuit_id uuid,
    equipment_id uuid
);


ALTER TABLE public.schedulings OWNER TO lince;

--
-- Name: sessions; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.sessions (
    id uuid NOT NULL,
    os_name character varying(40) NOT NULL,
    os_version character varying(40) NOT NULL,
    device character varying(40) NOT NULL,
    browser character varying(40) NOT NULL,
    browser_version character varying(40) NOT NULL,
    browser_engine character varying(40),
    browser_engine_version character varying(40),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    user_id uuid
);


ALTER TABLE public.sessions OWNER TO lince;

--
-- Name: topic_forms; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.topic_forms (
    id uuid NOT NULL,
    sequence integer NOT NULL,
    description character varying(255) NOT NULL,
    status integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    formulary_id uuid
);


ALTER TABLE public.topic_forms OWNER TO lince;

--
-- Name: users; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.users (
    id uuid NOT NULL,
    avatar character varying(250) NOT NULL,
    name character varying(50) NOT NULL,
    cpf character varying(11) NOT NULL,
    password text NOT NULL,
    token text,
    email character varying(150) NOT NULL,
    date_birth date NOT NULL,
    status integer NOT NULL,
    role integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.users OWNER TO lince;

--
-- Name: workers; Type: TABLE; Schema: public; Owner: lince
--

CREATE TABLE public.workers (
    id uuid NOT NULL,
    avatar character varying(250) NOT NULL,
    name character varying(50) NOT NULL,
    cpf character varying(11) NOT NULL,
    gender character varying(1) NOT NULL,
    race character varying(20) NOT NULL,
    pis_nit character varying(14) NOT NULL,
    phone character varying(12),
    email character varying(255) NOT NULL,
    date_birth date NOT NULL,
    status integer NOT NULL,
    role character varying(20) NOT NULL,
    rfid character varying(255),
    observation text,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    company_id uuid
);


ALTER TABLE public.workers OWNER TO lince;

--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public."SequelizeMeta" (name) FROM stdin;
20200403045532-create-users.js
20200403045609-create-companies.js
20200403045726-create-addresses.js
20200403045754-create-sessions.js
20200403045755-create-workers.js
20200408151247-create-workers-association.js
20200408151727-create-addresses-association.js
20200408151912-create-sessions-association.js
20200409225919-create-equipment-models.js
20200409225921-create-equipment-patrimonies.js
20200409231106-create-equipment-models-association.js
20200406141730-create-scheduling.js
20200406142345-create-circuits.js
20200406143330-add-scheduling-circutis-association.js
20200408180425-add-circuits-companies-association.js
20200421181705-add-scheduling-equipment-associaion.js
20200504165130-create-exams.js
20200504165556-add-exam-circuits-association.js
20200504174712-add-exam-workers-association.js
20200528152940-workers-field-change-phone.js
20200513124633-create-exam-class.js
20200513124702-create-formulary.js
20200513124852-add-formulary-exam-class-association.js
20200513192432-add-formulary-equiment-model-association.js
20200528195254-create-topic-form.js
20200528195317-create-question.js
20200528195323-create-question-item.js
20200528195638-add-topic-formulary-association.js
20200528200314-add-question-topic-association.js
20200528200548-add-question-question-item-association.js
20200605181714-add-questions-formularies-association.js
20200619200031-add-question-items-formulary-association.js
20200628160047-add-question-items-topic-association.js
\.


--
-- Data for Name: addresses; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.addresses (id, street, number, complement, district, city, state, zipcode, created_at, updated_at, company_id) FROM stdin;
a0bac687-db29-4d43-a131-2ddc9a725a9b	R. João Candido	120		Centro	Londrina	PR	86010510	2020-04-23 18:08:31.917-03	2020-04-23 18:08:44.702-03	b85cf2f8-21bf-4438-b36e-0d240b7c1b68
\.


--
-- Data for Name: circuits; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.circuits (id, name, start, finish, created_at, updated_at, company_id) FROM stdin;
\.


--
-- Data for Name: companies; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.companies (id, name, cnpj, email, phone, foundation_date, status, created_at, updated_at) FROM stdin;
b85cf2f8-21bf-4438-b36e-0d240b7c1b68	Robotics	83066261000131	robotics@gmail.com.br	4333332222_	2004-10-02	1	2020-04-23 18:08:31.904-03	2020-04-23 18:09:11.834-03
\.


--
-- Data for Name: equipment_models; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.equipment_models (id, name, maker, description, created_at, updated_at) FROM stdin;
f6200094-f8f5-498d-b780-62bcd654985a	Adam Robot 2.0	ADAM ROBOT SA.	Aparelho para triagem.	2020-07-03 16:26:35.069-03	2020-07-03 16:26:35.069-03
54480d61-22e4-4591-991e-167eda6d320d	E-nfermeira 1.5	SaúdeTech LTDA	Teste	2020-07-03 16:34:37.717-03	2020-07-03 16:34:37.717-03
0f8beca0-581c-4674-9922-d871c3b680a4	HiLab 1.0	Hi Technologies		2020-07-07 17:15:21.801-03	2020-07-07 17:32:30.037-03
\.


--
-- Data for Name: equipment_patrimonies; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.equipment_patrimonies (id, code, mac, status, token, created_at, updated_at, equipment_model_id) FROM stdin;
6e0826bc-4718-4fbf-aa1d-876632bb72b2	123456789	656879432423	1		2020-07-07 17:16:25.389-03	2020-07-07 17:16:25.389-03	0f8beca0-581c-4674-9922-d871c3b680a4
\.


--
-- Data for Name: exam_classes; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.exam_classes (id, name, created_at, updated_at) FROM stdin;
c71af932-68f4-4dc0-95eb-50e31621c7d1	Radiológico	2020-07-03 16:20:18.455-03	2020-07-03 16:20:18.455-03
070fc3ef-273e-459e-8742-abec4e61a163	Oftalmológico	2020-07-03 16:36:27.384-03	2020-07-03 16:36:27.384-03
3508d23b-fe74-41fe-93a5-92add7ff682a	Sanguíneo	2020-07-03 15:29:41.744-03	2020-07-07 17:27:31.094-03
bcabcd46-6a0f-49da-83ec-38f960864063	Perfil Lipídico	2020-07-08 15:56:12.158-03	2020-07-08 15:57:18.509-03
\.


--
-- Data for Name: exams; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.exams (id, start, finish, created_at, updated_at, circuit_id, worker_id) FROM stdin;
\.


--
-- Data for Name: formularies; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.formularies (id, name, status, description, created_at, updated_at, exam_class_id, equipment_model_id) FROM stdin;
9a61b64c-516e-4fa6-8caa-9ec931c46cda	Exame - Radiografia Costela	1	Teste	2020-07-03 16:21:42.062-03	2020-07-03 16:21:42.062-03	c71af932-68f4-4dc0-95eb-50e31621c7d1	\N
a99a272a-5b89-4033-aac9-8dadd11a547f	Exame - Oftalmológico	1	Exame para análise dos olhos.	2020-07-03 16:36:58.229-03	2020-07-03 16:36:58.229-03	070fc3ef-273e-459e-8742-abec4e61a163	f6200094-f8f5-498d-b780-62bcd654985a
fcd97e50-7e5f-41f7-a9e7-110ecc0d25a2	Exame - bHCG	1	Gravidez (Beta hCG)	2020-07-07 17:46:16.71-03	2020-07-07 17:51:31.06-03	3508d23b-fe74-41fe-93a5-92add7ff682a	0f8beca0-581c-4674-9922-d871c3b680a4
efe039e0-cc7e-43a1-b80d-c4c3fba2f032	Exame - Glicemia	1	Medir o nível de glicose (taxa de açúcares) na corrente sanguínea	2020-07-07 17:44:22.427-03	2020-07-07 17:52:49.484-03	3508d23b-fe74-41fe-93a5-92add7ff682a	0f8beca0-581c-4674-9922-d871c3b680a4
7c9c03b2-4fbc-41ce-b670-5be2265706af	Exame - COVID-19	1	COVID-19 é o nome dado a doença respiratória infectocontagiosa causada pelo novo Coronavírus.	2020-07-03 17:51:51.329-03	2020-07-07 17:57:43.468-03	3508d23b-fe74-41fe-93a5-92add7ff682a	0f8beca0-581c-4674-9922-d871c3b680a4
63c16984-b643-4bb5-986f-2fa78e307b1d	Exame - Perfil Lipídico	1	Grupo de exames para avaliar o risco de doença cardíaca coronariana	2020-07-07 17:17:30.31-03	2020-07-08 15:57:33.535-03	bcabcd46-6a0f-49da-83ec-38f960864063	0f8beca0-581c-4674-9922-d871c3b680a4
\.


--
-- Data for Name: question_items; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.question_items (id, sequence, description, complement, value, status, created_at, updated_at, question_id, formulary_id, topic_form_id) FROM stdin;
00a83cd0-fdb3-49a1-8407-0390712b4019	1	Teste de Alteração Item	teste	integer	1	2020-07-03 18:26:50.983-03	2020-07-03 18:28:31.836-03	3707c9b2-f1e6-43c3-aacd-ed95f6b63b18	9a61b64c-516e-4fa6-8caa-9ec931c46cda	7e0ccd24-fd0b-49db-93d5-3d41aa50acaa
\.


--
-- Data for Name: questions; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.questions (id, sequence, description, status, type, complement, created_at, updated_at, topic_form_id, formulary_id) FROM stdin;
813ad50f-d83c-4a87-92f4-a2fbc2b4c837	1	Nome	1	string	Teste	2020-07-03 17:59:51.714-03	2020-07-03 17:59:51.714-03	7e0ccd24-fd0b-49db-93d5-3d41aa50acaa	9a61b64c-516e-4fa6-8caa-9ec931c46cda
3b76ce7d-3a1d-475c-9ed6-1e6f6246b080	2	Endereço Teste alteração	1	string	Teste	2020-07-03 18:00:01.891-03	2020-07-03 18:09:21.067-03	7e0ccd24-fd0b-49db-93d5-3d41aa50acaa	9a61b64c-516e-4fa6-8caa-9ec931c46cda
3707c9b2-f1e6-43c3-aacd-ed95f6b63b18	3	Idade Teste alteração 	1	integer	Teste	2020-07-03 18:00:10.894-03	2020-07-03 18:09:22.793-03	7e0ccd24-fd0b-49db-93d5-3d41aa50acaa	9a61b64c-516e-4fa6-8caa-9ec931c46cda
6cc4c7c1-22ed-48f6-bec7-7da4d96bfc57	1	CHOL	1	float		2020-07-07 17:18:45.296-03	2020-07-07 17:37:39.784-03	7ffc4073-a0d0-4100-ac06-358e2f255025	63c16984-b643-4bb5-986f-2fa78e307b1d
0b2f3d6c-cbbe-4fca-8412-a3d90cd44dbd	3	TG	1	float	Triglicérides	2020-07-07 17:40:23.945-03	2020-07-07 17:40:23.945-03	7ffc4073-a0d0-4100-ac06-358e2f255025	63c16984-b643-4bb5-986f-2fa78e307b1d
ee1ac6d2-1833-431e-8744-7a00c15d749d	4	NHDL	1	float	Não-HDL	2020-07-07 17:41:10.288-03	2020-07-07 17:41:10.288-03	7ffc4073-a0d0-4100-ac06-358e2f255025	63c16984-b643-4bb5-986f-2fa78e307b1d
bf144074-ac59-4d34-9905-356ad14a577a	5	LDL	1	float	Colesterol LDL	2020-07-07 17:41:42.725-03	2020-07-07 17:41:42.725-03	7ffc4073-a0d0-4100-ac06-358e2f255025	63c16984-b643-4bb5-986f-2fa78e307b1d
9b370c2d-0a67-44f0-a84d-28936be13e6e	2	HDL	1	float	Colesterol HDL	2020-07-07 17:39:47.611-03	2020-07-07 17:41:52.309-03	7ffc4073-a0d0-4100-ac06-358e2f255025	63c16984-b643-4bb5-986f-2fa78e307b1d
e40b185e-79ee-49fb-9c2f-995d9294b257	6	VLDL	1	float	Colesterol VLDL	2020-07-07 17:42:13.527-03	2020-07-07 17:42:13.527-03	7ffc4073-a0d0-4100-ac06-358e2f255025	63c16984-b643-4bb5-986f-2fa78e307b1d
485885ec-8e1e-4328-a77b-96a10fade11e	1	bHCG	1	float		2020-07-07 17:48:45.394-03	2020-07-07 17:48:45.394-03	6764a386-cffa-423e-87bb-79a8d3edd10e	fcd97e50-7e5f-41f7-a9e7-110ecc0d25a2
e2ed4a14-6356-4c19-8455-6de41b58f953	1	GLICOSE	1	float	Resultado em (mg/dL)	2020-07-07 17:52:17.012-03	2020-07-07 17:52:17.012-03	e7f59dd2-bb23-46d2-a16a-dc628fbe8a53	efe039e0-cc7e-43a1-b80d-c4c3fba2f032
378e4aaa-1676-4033-bc5c-af0c6ec66b5f	1	IgG	1	float		2020-07-07 17:57:28.7-03	2020-07-07 17:57:28.7-03	f9834842-01bc-4092-b3fa-29b496b42224	7c9c03b2-4fbc-41ce-b670-5be2265706af
4d5d2d72-37f0-4274-88c1-6d68e5e27213	2	IgM	1	float		2020-07-07 17:57:39.875-03	2020-07-07 17:57:39.875-03	f9834842-01bc-4092-b3fa-29b496b42224	7c9c03b2-4fbc-41ce-b670-5be2265706af
\.


--
-- Data for Name: schedulings; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.schedulings (id, start, finish, created_at, updated_at, circuit_id, equipment_id) FROM stdin;
\.


--
-- Data for Name: sessions; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.sessions (id, os_name, os_version, device, browser, browser_version, browser_engine, browser_engine_version, created_at, updated_at, user_id) FROM stdin;
00bbf626-2dc6-4701-87e2-d94d9db65818	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:28:34.046-03	2020-07-22 14:28:34.046-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
132757cc-917f-44d5-af05-4bec8aa325da	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:35:09.63-03	2020-07-22 14:35:09.63-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
737b51ab-e362-4805-9d73-63604bc905f5	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:38:04.272-03	2020-07-22 14:38:04.272-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
3720e6fc-760a-4235-a1c5-75a841290620	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:43:31.844-03	2020-07-22 14:43:31.844-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
dc857724-26e4-49ec-a4d2-bb3b46f3a0e3	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 15:13:25.99-03	2020-07-22 15:13:25.99-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
a7d0bb82-8122-4a6c-aea4-f80d5847aa3d	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 15:20:11.1-03	2020-07-22 15:20:11.1-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
1031256a-02dc-4249-be20-a687d4827b95	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 16:50:56.351-03	2020-07-22 16:50:56.351-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
183aa294-77ba-4e44-a298-4e1cd217355c	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:04:05.541-03	2020-07-22 17:04:05.541-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
5058bbc7-88e0-4619-9162-fbba8ba1aed6	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:19:18.833-03	2020-07-22 17:19:18.833-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
3b2d4bbd-6117-4b10-b42e-2517b47f00cd	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:27:41.958-03	2020-07-22 17:27:41.958-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
fd229bf7-eea6-451a-b88c-eb2bcdc3d407	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 18:03:40.894-03	2020-07-22 18:03:40.894-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
3e244409-fd2d-4783-90f7-4a61777ddf41	Windows NT 10.0	Win64	 x64	Firefox	75.0	Gecko	20100101	2020-04-23 18:16:59.501-03	2020-04-23 18:16:59.501-03	16ba50c0-12e8-4225-9890-e0272fbe3d4a
362733e4-81a6-464c-9590-e35085bb490b	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:29:38.996-03	2020-07-22 14:29:38.996-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
e5742572-2667-4759-a1dd-7114d027b16c	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:36:07.953-03	2020-07-22 14:36:07.953-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
0d605867-794d-4793-bc35-92c9dcf02152	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:38:26.102-03	2020-07-22 14:38:26.102-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
319ed81e-47d5-499b-b81b-dd0d6c8f5487	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:45:18.025-03	2020-07-22 14:45:18.025-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
44f56f48-697b-47b4-a687-d0c3efe98002	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 15:13:52.239-03	2020-07-22 15:13:52.239-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
243adad6-99f4-4529-86aa-b9110842e1f2	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 15:37:28.637-03	2020-07-22 15:37:28.637-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
7be03781-9f88-4627-8841-5389f0e9f760	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 16:54:18.613-03	2020-07-22 16:54:18.613-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
0ebe3936-70a7-4787-bd6a-8700e6c5bf61	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:10:21.681-03	2020-07-22 17:10:21.681-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
a3261a57-2483-49de-be81-ceb2b6151739	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:22:39.813-03	2020-07-22 17:22:39.813-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
e5650cfd-c1c6-43b0-8409-91f2f0b2ed4e	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:31:27.231-03	2020-07-22 17:31:27.231-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
f8832350-3fb8-4c79-be50-0a89dec13ab8	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 18:08:45.113-03	2020-07-22 18:08:45.113-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
0b3bdc96-5608-4691-98c4-a38e5517f78b	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:30:14.615-03	2020-07-22 14:30:14.615-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
082d022c-3869-4d64-90f9-0aa1bb67eb3d	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:36:20.502-03	2020-07-22 14:36:20.502-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
5ffe56c3-347a-43e4-a9b0-e3c490ba0239	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:39:31.387-03	2020-07-22 14:39:31.387-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
d1083638-fc64-4e7e-baf3-c00981b48ec2	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:58:45.368-03	2020-07-22 14:58:45.368-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
0eefd189-6167-4f07-9f0d-ee77f50fc393	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 15:14:35.23-03	2020-07-22 15:14:35.23-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
aae8ccd8-b3f4-4910-8494-f9785c735b74	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 15:38:35.39-03	2020-07-22 15:38:35.39-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
4e1bd3e2-3013-42ee-bbbf-d602e15c47ab	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 16:59:57.031-03	2020-07-22 16:59:57.031-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
6558d2a3-f8db-4866-a31b-ffb3b9e0ee59	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:13:33.523-03	2020-07-22 17:13:33.523-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
9fb7999d-72a2-4b10-9b47-5f361c383c8b	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:23:43.175-03	2020-07-22 17:23:43.175-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
e4aa83e4-339f-4275-aa89-ec899fc76945	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:40:02.545-03	2020-07-22 17:40:02.545-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
07d592f0-05e0-4013-bf9f-2ae00a1427bb	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:31:07.738-03	2020-07-22 14:31:07.738-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
5bcbb3e2-d5b8-4eb0-acce-cecee90a1006	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:36:58.086-03	2020-07-22 14:36:58.086-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
8abd7b44-61ff-4ecd-9829-37cb1e14026e	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:40:05.407-03	2020-07-22 14:40:05.407-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
9ef30d73-c2a8-4987-a1bf-91e28d6bf7b7	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 15:02:22.977-03	2020-07-22 15:02:22.977-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
6d7f6ac9-b9af-4b1e-85a5-2c838608557e	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 15:15:09.625-03	2020-07-22 15:15:09.625-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
b2f67344-6fa7-45fc-9c86-2b7b9b9aa0d3	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 15:45:09.226-03	2020-07-22 15:45:09.226-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
8538bf71-7a20-4226-9613-84c3c332af67	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:01:53.747-03	2020-07-22 17:01:53.747-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
3c97ecae-fbf7-494a-be6d-b4481634bfa0	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:13:58.084-03	2020-07-22 17:13:58.084-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
82974d60-98f7-41aa-b0b6-aeda52e97fb0	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:24:20.591-03	2020-07-22 17:24:20.591-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
74829ea6-d722-4bde-9648-20353b04bb38	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:52:54.996-03	2020-07-22 17:52:54.996-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
00519ef6-87c0-4237-8252-e44a81e36224	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:31:37.027-03	2020-07-22 14:31:37.027-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
a2cae9d9-9432-4445-882f-c42315d784a5	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:37:24.689-03	2020-07-22 14:37:24.689-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
35d4530e-af48-4c48-bcb3-91acf75e5717	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 14:40:56.322-03	2020-07-22 14:40:56.322-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
ed7851d4-9d89-4a15-b741-0c9c8c6f729d	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 15:07:49.781-03	2020-07-22 15:07:49.781-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
660b9c96-ec24-46d3-bb74-a06a9187f5b7	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 15:18:58.263-03	2020-07-22 15:18:58.263-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
a034d7e7-10ea-457f-b157-f8a0f5d95f69	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 16:26:19.134-03	2020-07-22 16:26:19.134-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
47fd828e-cf3b-4226-b06a-0b84459ce939	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:02:44.116-03	2020-07-22 17:02:44.116-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
f4be2816-ecaa-4c26-8f41-b7dff2ed3fcd	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:17:55.881-03	2020-07-22 17:17:55.881-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
bb4f45c4-48f2-4186-b92a-8080f1ce8d4b	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 17:25:08.162-03	2020-07-22 17:25:08.162-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
834e11ae-8923-4bc1-bf5b-567fec4a5194	iPhone	CPU iPhone OS 13_3 like Mac OS X		AppleWebKit	605.1.15	AppleWebKit	605.1.15	2020-07-22 18:00:31.642-03	2020-07-22 18:00:31.642-03	faa3b49c-b053-4f0b-94de-4b0c787f31e7
c98ccabc-c70e-48f0-bf25-7f39193c5638	Windows NT 10.0	Win64	 x64	Firefox	78.0	Gecko	20100101	2020-07-07 08:42:36.664-03	2020-07-07 08:42:36.664-03	16ba50c0-12e8-4225-9890-e0272fbe3d4a
c92ceb5d-a607-493c-b19d-13b7dfba1169	Windows NT 10.0	Win64	 x64	Firefox	78.0	Gecko	20100101	2020-07-09 08:46:53.218-03	2020-07-09 08:46:53.218-03	5ec09a2c-1312-47c0-a86e-534dcc062228
3ac21042-0266-46e7-9e08-c7db841a62da	Macintosh	Intel Mac OS X 10_12_6		Chrome	83.0.4103.116	AppleWebKit	537.36	2020-07-09 16:52:48.366-03	2020-07-09 16:52:48.366-03	5ec09a2c-1312-47c0-a86e-534dcc062228
f66ecac5-02e6-43e0-b323-e8eb35e5fed9	Macintosh	Intel Mac OS X 10_14_5		Chrome	83.0.4103.116	AppleWebKit	537.36	2020-07-09 16:53:16.885-03	2020-07-09 16:53:16.885-03	5ec09a2c-1312-47c0-a86e-534dcc062228
84c719a1-2edd-46e8-850f-5130fff35086	Windows NT 10.0	Win64	 x64	Firefox	78.0	Gecko	20100101	2020-07-10 09:03:29.331-03	2020-07-10 09:03:29.331-03	5ec09a2c-1312-47c0-a86e-534dcc062228
\.


--
-- Data for Name: topic_forms; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.topic_forms (id, sequence, description, status, created_at, updated_at, formulary_id) FROM stdin;
7e0ccd24-fd0b-49db-93d5-3d41aa50acaa	1	Informações pessoais	1	2020-07-03 17:58:41.513-03	2020-07-03 18:09:25.342-03	9a61b64c-516e-4fa6-8caa-9ec931c46cda
7ffc4073-a0d0-4100-ac06-358e2f255025	1	Geral	1	2020-07-07 17:18:09.798-03	2020-07-07 17:18:09.798-03	63c16984-b643-4bb5-986f-2fa78e307b1d
6764a386-cffa-423e-87bb-79a8d3edd10e	1	Geral	1	2020-07-07 17:48:20.421-03	2020-07-07 17:48:29.56-03	fcd97e50-7e5f-41f7-a9e7-110ecc0d25a2
e7f59dd2-bb23-46d2-a16a-dc628fbe8a53	1	Geral	1	2020-07-07 17:51:40.005-03	2020-07-07 17:51:40.005-03	efe039e0-cc7e-43a1-b80d-c4c3fba2f032
f9834842-01bc-4092-b3fa-29b496b42224	1	Geral	1	2020-07-07 17:57:06.294-03	2020-07-07 17:57:06.294-03	7c9c03b2-4fbc-41ce-b670-5be2265706af
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.users (id, avatar, name, cpf, password, token, email, date_birth, status, role, created_at, updated_at) FROM stdin;
5ec09a2c-1312-47c0-a86e-534dcc062228	thumb.png	Webmaster	57244446071	24adebc5729c1dbefa5294f2c47a5a56		web@master.com	1990-11-01	1	1	2020-04-22 15:48:03.881-03	2020-04-22 16:46:04.598-03
465f7be0-0c4d-4aac-bb16-7c643b67df4d	1587670261157_007.jpg	Nathalia Pessoal	10727766007	049a1c27b32d52e7e4db6dcedc95d439		nathssilva@gmail.com	1988-04-29	0	4	2020-04-23 16:31:01.158-03	2020-04-23 18:17:13.78-03
faa3b49c-b053-4f0b-94de-4b0c787f31e7	thumb.png	Márcio Sérgio da Silva	64176835915	b0e72d7fa72488ae9bc4e487c2dd2cb9		marcio.sergio@admservice.com.br	2000-06-16	1	4	2020-06-18 16:49:54.618-03	2020-06-18 16:49:54.618-03
16ba50c0-12e8-4225-9890-e0272fbe3d4a	1587676583156_048.jpg	Nathalia Nolepa	43731536048	96190dadfbb1780b09940ae6111fef73		nathalia.nolepa@sistemafiep.org.br	1988-04-29	1	1	2020-04-23 18:16:23.156-03	2020-07-07 08:41:53.115-03
\.


--
-- Data for Name: workers; Type: TABLE DATA; Schema: public; Owner: lince
--

COPY public.workers (id, avatar, name, cpf, gender, race, pis_nit, phone, email, date_birth, status, role, rfid, observation, created_at, updated_at, company_id) FROM stdin;
dcc0f015-f60f-4de6-84b5-aa1c6fdadb49	thumb.png	Márcio	64176835915	M	branca	20938385733	44999999999	marcio.sergio@admservice.com.br	1963-03-03	0	Adm		teste obs	2020-06-23 17:26:12.545-03	2020-06-23 17:26:12.545-03	b85cf2f8-21bf-4438-b36e-0d240b7c1b68
\.


--
-- Name: SequelizeMeta SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- Name: addresses addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: circuits circuits_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.circuits
    ADD CONSTRAINT circuits_pkey PRIMARY KEY (id);


--
-- Name: companies companies_cnpj_key; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_cnpj_key UNIQUE (cnpj);


--
-- Name: companies companies_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- Name: equipment_models equipment_models_name_key; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.equipment_models
    ADD CONSTRAINT equipment_models_name_key UNIQUE (name);


--
-- Name: equipment_models equipment_models_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.equipment_models
    ADD CONSTRAINT equipment_models_pkey PRIMARY KEY (id);


--
-- Name: equipment_patrimonies equipment_patrimonies_code_key; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.equipment_patrimonies
    ADD CONSTRAINT equipment_patrimonies_code_key UNIQUE (code);


--
-- Name: equipment_patrimonies equipment_patrimonies_mac_key; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.equipment_patrimonies
    ADD CONSTRAINT equipment_patrimonies_mac_key UNIQUE (mac);


--
-- Name: equipment_patrimonies equipment_patrimonies_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.equipment_patrimonies
    ADD CONSTRAINT equipment_patrimonies_pkey PRIMARY KEY (id);


--
-- Name: exam_classes exam_classes_name_key; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.exam_classes
    ADD CONSTRAINT exam_classes_name_key UNIQUE (name);


--
-- Name: exam_classes exam_classes_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.exam_classes
    ADD CONSTRAINT exam_classes_pkey PRIMARY KEY (id);


--
-- Name: exams exams_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.exams
    ADD CONSTRAINT exams_pkey PRIMARY KEY (id);


--
-- Name: formularies formularies_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.formularies
    ADD CONSTRAINT formularies_pkey PRIMARY KEY (id);


--
-- Name: question_items question_items_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.question_items
    ADD CONSTRAINT question_items_pkey PRIMARY KEY (id);


--
-- Name: questions questions_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (id);


--
-- Name: schedulings schedulings_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.schedulings
    ADD CONSTRAINT schedulings_pkey PRIMARY KEY (id);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- Name: topic_forms topic_forms_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.topic_forms
    ADD CONSTRAINT topic_forms_pkey PRIMARY KEY (id);


--
-- Name: users users_cpf_key; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_cpf_key UNIQUE (cpf);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: workers workers_cpf_key; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_cpf_key UNIQUE (cpf);


--
-- Name: workers workers_pis_nit_key; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_pis_nit_key UNIQUE (pis_nit);


--
-- Name: workers workers_pkey; Type: CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_pkey PRIMARY KEY (id);


--
-- Name: addresses addresses_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: circuits circuits_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.circuits
    ADD CONSTRAINT circuits_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: equipment_patrimonies equipment_patrimonies_equipment_model_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.equipment_patrimonies
    ADD CONSTRAINT equipment_patrimonies_equipment_model_id_fkey FOREIGN KEY (equipment_model_id) REFERENCES public.equipment_models(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: exams exams_circuit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.exams
    ADD CONSTRAINT exams_circuit_id_fkey FOREIGN KEY (circuit_id) REFERENCES public.circuits(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: exams exams_worker_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.exams
    ADD CONSTRAINT exams_worker_id_fkey FOREIGN KEY (worker_id) REFERENCES public.workers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: formularies formularies_equipment_model_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.formularies
    ADD CONSTRAINT formularies_equipment_model_id_fkey FOREIGN KEY (equipment_model_id) REFERENCES public.equipment_models(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: formularies formularies_exam_class_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.formularies
    ADD CONSTRAINT formularies_exam_class_id_fkey FOREIGN KEY (exam_class_id) REFERENCES public.exam_classes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: question_items question_items_formulary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.question_items
    ADD CONSTRAINT question_items_formulary_id_fkey FOREIGN KEY (formulary_id) REFERENCES public.formularies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: question_items question_items_question_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.question_items
    ADD CONSTRAINT question_items_question_id_fkey FOREIGN KEY (question_id) REFERENCES public.questions(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: question_items question_items_topic_form_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.question_items
    ADD CONSTRAINT question_items_topic_form_id_fkey FOREIGN KEY (topic_form_id) REFERENCES public.topic_forms(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: questions questions_formulary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.questions
    ADD CONSTRAINT questions_formulary_id_fkey FOREIGN KEY (formulary_id) REFERENCES public.formularies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: questions questions_topic_form_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.questions
    ADD CONSTRAINT questions_topic_form_id_fkey FOREIGN KEY (topic_form_id) REFERENCES public.topic_forms(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: schedulings schedulings_circuit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.schedulings
    ADD CONSTRAINT schedulings_circuit_id_fkey FOREIGN KEY (circuit_id) REFERENCES public.circuits(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: schedulings schedulings_equipment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.schedulings
    ADD CONSTRAINT schedulings_equipment_id_fkey FOREIGN KEY (equipment_id) REFERENCES public.equipment_patrimonies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sessions sessions_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: topic_forms topic_forms_formulary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.topic_forms
    ADD CONSTRAINT topic_forms_formulary_id_fkey FOREIGN KEY (formulary_id) REFERENCES public.formularies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: workers workers_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lince
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

