'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'schedulings', // name of Source model
      'circuit_id', // name of the key we're adding 
      {
        type: Sequelize.UUID,
        references: {
          model: 'circuits', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'schedulings', // name of Source model
      'circuit_id' // key we want to remove
    );
  }
};
