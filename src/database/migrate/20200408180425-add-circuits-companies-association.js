'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'circuits', // name of Source model
      'company_id', // name of the key we're adding 
      {
        type: Sequelize.UUID,
        references: {
          model: 'companies', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'circuits', // name of Source model
      'company_id' // key we want to remove
    );
  }
};
