'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('workers',
    {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        unique: true
      },
      avatar: {
        type: Sequelize.STRING(250),
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      cpf: {
        type: Sequelize.STRING(11),
        allowNull: false,
        unique: true,
      },
      gender: {
        type: Sequelize.STRING(1),
        allowNull: false
      },
      race: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      pis_nit: {
        type: Sequelize.STRING(14),
        allowNull: false,
        unique: true
      },
      phone: {
        type: Sequelize.STRING(11),
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING(255),
        allowNull: false,
      },
      date_birth: {
        type: Sequelize.DATEONLY,
        allowNull: false,
      },
      status: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      role: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      rfid: Sequelize.STRING(255),
      observation: Sequelize.TEXT,
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      }
    },
    { freezeTableName: true }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('workers')
}