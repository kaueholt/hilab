'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'formularies', // name of Source model
      'equipment_model_id', // name of the key we're adding 
      {
        type: Sequelize.UUID,
        references: {
          model: 'equipment_models', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: "CASCADE",
        allowNull: true
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'formularies', // name of Source model
      'equipment_model_id' // key we want to remove
    );
  }
};