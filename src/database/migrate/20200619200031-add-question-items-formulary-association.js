'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'question_items', // name of Source model
      'formulary_id', // name of the key we're adding 
      {
        type: Sequelize.UUID,
        references: {
          model: 'formularies', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: "CASCADE"
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'question_items', // name of Source model
      'formulary_id' // key we want to remove
    );
  }
};
