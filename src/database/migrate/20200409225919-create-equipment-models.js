'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('equipment_models',
    {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        unique: true
      },
      name: {
        type: Sequelize.STRING(30),
        allowNull: false,
        unique: true
      },
      maker: {
        type: Sequelize.STRING(30),
        allowNull: false
      },
      description: Sequelize.STRING(200),
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      }
    },
    { freezeTableName: true }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('equipment_models')
}