'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('equipment_patrimonies',
    {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        unique: true
      },
      code: {
        type: Sequelize.STRING(10),
        allowNull: true,
        unique: true
      },
      mac: {
        type: Sequelize.STRING(32),
        unique: true,
        allowNull: false
      },
      status: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      token: Sequelize.STRING(255),
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      }
    },
    { freezeTableName: true }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('equipment_patrimonies')
}