'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('workers', 'phone', {
      type: Sequelize.STRING(12),
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('workers', 'phone', {
      type: Sequelize.STRING(11),
    })
  }
};
