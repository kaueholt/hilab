'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'exams', // name of Source model
      'worker_id', // name of the key we're adding 
      {
        type: Sequelize.UUID,
        references: {
          model: 'workers', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: "CASCADE"
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'exams', // name of Source model
      'worker_id' // key we want to remove
    );
  }
};