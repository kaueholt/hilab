'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('addresses',
    {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        unique: true
      },
      street: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      number: {
        type: Sequelize.STRING(4),
        allowNull: false,
      },
      complement: {
        type: Sequelize.STRING(50)
      },
      district: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      state: {
        type: Sequelize.STRING(2),
        allowNull: false,
      },
      zipcode: {
        type: Sequelize.STRING(8),
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      }
    },
    { freezeTableName: true }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('addresses')
}
