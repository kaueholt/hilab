'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'formularies', // name of Source model
      'exam_class_id', // name of the key we're adding 
      {
        type: Sequelize.UUID,
        references: {
          model: 'exam_classes', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: "CASCADE"
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'formularies', // name of Source model
      'exam_class_id' // key we want to remove
    );
  }
};