'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('sessions',
    {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        unique: true
      },
      os_name: {
        type: Sequelize.STRING(40),
        allowNull: false,
      },
      os_version: {
        type: Sequelize.STRING(40),
        allowNull: false,
      },
      device: {
        type: Sequelize.STRING(40),
        allowNull: false,
      },
      browser: {
        type: Sequelize.STRING(40),
        allowNull: false,
      },
      browser_version: {
        type: Sequelize.STRING(40),
        allowNull: false,
      },
      browser_engine: {
        type: Sequelize.STRING(40),
        allowNull: true,
      },
      browser_engine_version: {
        type: Sequelize.STRING(40),
        allowNull: true,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      }
    },
    { freezeTableName: true }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('sessions')
}
