'use strict'

const dotenv = require('dotenv')
dotenv.config()

// TODO: classe config
module.exports = {
  development: {
    username: process.env.DB_USER || 'lince',
    password: process.env.DB_PASSWORD || 'lince',
    database: process.env.DB_NAME || 'db_lince',
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || '5432',
    dialect: 'postgres',
    operatorsAliases: 0,
    define: {
      timestamp: true,
      underscored: true
    }
  },
  homolog: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'postgres',
    operatorsAliases: 0,
    define: {
      timestamp: true,
      underscored: true
    }
  },
  production: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'postgres',
    operatorsAliases: 0,
    define: {
      timestamp: true,
      underscored: true
    }
  }
}
