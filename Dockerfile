FROM registry.git.senailondrina.com/reuso/docker/node-13-8-0-alpine:1.0

WORKDIR /app

EXPOSE 5000

ENTRYPOINT [ "npm","run","prod"]

# pegando variavel por parâmetro
ARG APP_URL=
ARG DB_HOST=
ARG DB_PASSWORD=
ARG DB_USER=
ARG DB_PORT=
ARG DB_NAME=

# gravando variaveis de ambiente
ENV NODE_ENV=production \
    PORT=5000 \
    APP_URL=${APP_URL} \
    DB_HOST=${DB_HOST} \
    DB_PASSWORD=${DB_PASSWORD} \
    DB_USER=${DB_USER} \
    DB_PORT=${DB_PORT} \
    DB_NAME=${DB_NAME}

# instalando dependencias do projeto
COPY package.json package-lock.json ./

RUN npm install --production

COPY . .
